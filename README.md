# Fantasia Game Engine

## Project Goals

Fantasia is a 2D game engine designed to create immersive and dynamic game worlds.

The engine allows players to:

* gather and mine resources to modify the game world,
* create new things through crafting and construction,
* engage in farming and hunting for food production,
* interact with mechanic and hydraulic systems
* control NPCs and engage in quests.

Fantasia uses a hexagonal density grid for world storage, providing a smooth, non-blocky visual experience.

Plant grow is simulated by the engine, allowing a seamless grow animation and access to branches, leaves and fruits.

The engine enables deep interactions between mobile entities (mobs) and their environment.

The world generator supports everything from medieval settings with changing seasons to tropical worlds or oceans with coral reefs, each world presents its own challenges and opportunities for players to discover and conquer.

Fantasia provides a C++ core for performance-sensitive components like water, fire, and physics simulations, which can be accessed through a Python layer containing high-level game logic.

