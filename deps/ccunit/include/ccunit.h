/*
 * MIT License
 * Copyright (c) 2024
 */

#pragma once

#include <iostream>
#include <memory>
#include <string>
#include <vector>

// clang-format off

#define EXPECT_TRUE(value) \
  { if (ccunit::check((value), "EXPECT_TRUE(" #value ")", __FILE__, __LINE__, 0)) {} }

#define ASSERT_TRUE(value) \
  { if (ccunit::check((value), "ASSERT_TRUE(" #value ")", __FILE__, __LINE__, 1)) return; }

#define EXPECT_FALSE(value) \
  { if (ccunit::check(!(value), "EXPECT_FALSE(" #value ")", __FILE__, __LINE__, 0)) {} }

#define ASSERT_FALSE(value) \
  { if (ccunit::check(!(value), "ASSERT_FALSE(" #value ")", __FILE__, __LINE__, 1)) return; }

#define EXPECT_EQ(actual, expected) \
  { if (ccunit::check(((actual) == (expected)), #actual " == " #expected, __FILE__, __LINE__, 0)) {} }

#define ASSERT_EQ(actual, expected) \
  { if (ccunit::check(((actual) == (expected)), #actual " == " #expected, __FILE__, __LINE__, 1)) return; }

#define EXPECT_NE(actual, expected) \
  { if (ccunit::check(((actual) != (expected)), #actual " != " #expected, __FILE__, __LINE__, 0)) {} }

#define ASSERT_NE(actual, expected) \
  { if (ccunit::check(((actual) != (expected)), #actual " != " #expected, __FILE__, __LINE__, 1)) return; }

#define EXPECT_LT(actual, expected) \
  { if (ccunit::check(((actual) < (expected)), #actual " < " #expected, __FILE__, __LINE__, 0)) {} }

#define ASSERT_LT(actual, expected) \
  { if (ccunit::check(((actual) < (expected)), #actual " < " #expected, __FILE__, __LINE__, 1)) return; }

#define EXPECT_LE(actual, expected) \
  { if (ccunit::check(((actual) <= (expected)), #actual " <= " #expected, __FILE__, __LINE__, 0)) {} }

#define ASSERT_LE(actual, expected) \
  { if (ccunit::check(((actual) <= (expected)), #actual " <= " #expected, __FILE__, __LINE__, 1)) return; }

#define EXPECT_GT(actual, expected) \
  { if (ccunit::check(((actual) > (expected)), #actual " > " #expected, __FILE__, __LINE__, 0)) {} }

#define ASSERT_GT(actual, expected) \
  { if (ccunit::check(((actual) > (expected)), #actual " > " #expected, __FILE__, __LINE__, 1)) return; }

#define EXPECT_GE(actual, expected) \
  { if (ccunit::check(((actual) >= (expected)), #actual " >= " #expected, __FILE__, __LINE__, 0)) {} }

#define ASSERT_GE(actual, expected) \
  { if (ccunit::check(((actual) >= (expected)), #actual " >= " #expected, __FILE__, __LINE__, 1)) return; }

#define EXPECT_FLOAT_EQ(actual, expected) \
  { if (ccunit::check(ccunit::float_eq((actual), (expected)), #actual " = " #expected, __FILE__, __LINE__, 0)) {} }

#define ASSERT_FLOAT_EQ(actual, expected) \
  { if (ccunit::check(ccunit::float_eq((actual), (expected)), #actual " = " #expected, __FILE__, __LINE__, 1)) return; }

#define EXPECT_FLOAT_NEAR(actual, expected) \
  { if (ccunit::check(ccunit::float_near((actual), (expected)), #actual " ~ " #expected, __FILE__, __LINE__, 0)) {} }

#define ASSERT_FLOAT_NEAR(actual, expected) \
  { if (ccunit::check(ccunit::float_near((actual), (expected)), #actual " ~ " #expected, __FILE__, __LINE__, 1)) return; }

// clang-format on

namespace ccunit {

// ----------------------------------------------------------------------------
typedef void (*FnTest)();

// ----------------------------------------------------------------------------
class Test
{
public:
  Test(const std::string& name, FnTest fnTest);

  const std::string name() const;
  void run() const;

private:
  const std::string _name;
  FnTest _fnTest;
};

using Tests = std::vector<std::unique_ptr<Test>>;

// ----------------------------------------------------------------------------
typedef enum
{
  printNothing,
  printFailuresOnly,
  printEverything,
} VerboseMode;

size_t runTests(const Tests& tests, VerboseMode mode = printFailuresOnly, std::ostream& output = std::cout);

int check(
  int value,
  const std::string& condition,
  const std::string& file,
  unsigned line,
  int fatal);

bool float_eq(const float& v0, const float& v1);
bool float_near(const float& v0, const float& v1);

} // namespace ccunit
