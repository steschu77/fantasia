/*
 * MIT License
 * Copyright (c) 2024
 */

#include "ccunit.h"
#include "ccunit_testrun.h"

// ----------------------------------------------------------------------------
static ccunit::TestRun* testrun = nullptr;

// ----------------------------------------------------------------------------
int ccunit::check(int value, const std::string& condition,
  const std::string& file, unsigned line, int fatal)
{
  return testrun->check(value, condition, file, line, fatal);
}

// ----------------------------------------------------------------------------
size_t ccunit::runTests(const Tests& tests, VerboseMode mode, std::ostream& output)
{
  TestRun run { mode, output };
  testrun = &run;

  return run.runTests(tests);
}
