/*
 * MIT License
 * Copyright (c) 2024
 */

#include "ccunit_failure.h"

// ----------------------------------------------------------------------------
ccunit::Failure::Failure(
  const std::string& file,
  unsigned line,
  const std::string& condition)
: _file(file)
, _line(line)
, _condition(condition)
{
}

// ----------------------------------------------------------------------------
void ccunit::Failure::print(std::ostream& output) const
{
  output << "\n   " << _file << ":" << _line << " - " << _condition;
}

// ----------------------------------------------------------------------------
void ccunit::printFailures(const Failures& failures, std::ostream& output)
{
  for (const auto& failure : failures) {
    failure->print(output);
  }
}
