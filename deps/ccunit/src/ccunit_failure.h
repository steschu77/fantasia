/*
 * MIT License
 * Copyright (c) 2024
 */

#pragma once

#include <iostream>
#include <memory>
#include <string>
#include <vector>

namespace ccunit {

// ----------------------------------------------------------------------------
class Failure
{
public:
  Failure(
    const std::string& file,
    unsigned line,
    const std::string& condition);

  void print(std::ostream& output) const;

private:
  std::string _file;
  unsigned _line;
  std::string _condition;
};

using Failures = std::vector<std::unique_ptr<Failure>>;

void printFailures(const Failures& failures, std::ostream& output);

} // namespace ccunit
