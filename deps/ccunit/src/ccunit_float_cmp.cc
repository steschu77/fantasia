/*
 * MIT License
 * Copyright (c) 2024
 */

#include "ccunit.h"

#include <cmath>
#include <cstdint>

// ----------------------------------------------------------------------------
constexpr int k_maxULP = 4;
constexpr float k_epsilon = 1e-6;

// ----------------------------------------------------------------------------
struct float_complement_t
{
  explicit float_complement_t(const float& x)
  {
    _u.value = x;
    if (_u.raw < 0) {
      _u.raw = 0x80000000 - _u.raw;
    }
  }

  union float_union_t
  {
    float value;
    int32_t raw;
  } _u;

  int32_t raw() const { return _u.raw; };
};

// ----------------------------------------------------------------------------
bool ccunit::float_eq(const float& v0, const float& v1)
{
  // treat representation as two's complement
  float_complement_t f0 { v0 };
  float_complement_t f1 { v1 };
  return abs(f0.raw() - f1.raw()) <= k_maxULP;
}

// ----------------------------------------------------------------------------
bool ccunit::float_near(const float& v0, const float& v1)
{
  if (fabs(v0 - v1) <= k_epsilon) {
    return true;
  }
  return float_eq(v0, v1);
}
