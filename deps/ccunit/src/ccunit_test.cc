/*
 * MIT License
 * Copyright (c) 2024
 */

#include "ccunit.h"

// ----------------------------------------------------------------------------
ccunit::Test::Test(const std::string& name, FnTest fnTest)
: _name(name)
, _fnTest(fnTest)
{
}

// ----------------------------------------------------------------------------
const std::string ccunit::Test::name() const
{
  return _name;
}

// ----------------------------------------------------------------------------
void ccunit::Test::run() const
{
  (*_fnTest)();
}
