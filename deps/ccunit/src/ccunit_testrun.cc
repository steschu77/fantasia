/*
 * MIT License
 * Copyright (c) 2024
 */

#include <memory>
#include <string>
#include <vector>

#include <iomanip>
#include <iostream>

#include "ccunit_colors.h"
#include "ccunit_testrun.h"

// ----------------------------------------------------------------------------
template <typename T>
void emplace_back(std::vector<T>& v2, std::vector<T>& v1)
{
  v2.insert(
    v2.end(),
    std::make_move_iterator(v1.begin()),
    std::make_move_iterator(v1.end()));
  v1.clear();
}

// ----------------------------------------------------------------------------
int ccunit::TestRun::check(int value, const std::string& condition,
  const std::string& file, unsigned line, int fatal)
{
  ++_numChecks[fatal];

  if (!value) {
    ++_numChecksFailed[fatal];

    _currentFailures.push_back(std::make_unique<ccunit::Failure>(file, line, condition));
  }

  return fatal != 0 && !value;
}

// ----------------------------------------------------------------------------
ccunit::TestRun::TestRun(VerboseMode mode, std::ostream& output)
: _mode(mode)
, _output(output)
{
}

// ----------------------------------------------------------------------------
void ccunit::TestRun::onTestStart(const Test& test)
{
  if (_mode == printEverything) {
    _output << "\nTest: '" << test.name() << "' ";
  }
}

// ----------------------------------------------------------------------------
void ccunit::TestRun::onTestComplete(
  const Test& test, const Failures& failures)
{
  switch (_mode) {
  case printEverything:
    _output << (failures.empty() ? ANSI_BRIGHTGREEN "passed" ANSI_RESET : ANSI_BRIGHTRED "FAILED" ANSI_RESET);
    if (!failures.empty()) {
      printFailures(failures, _output);
    }
    break;
  case printFailuresOnly:
    if (!failures.empty()) {
      _output << "\nTest " ANSI_YELLOW << test.name() << ANSI_RESET " had failures:";
      printFailures(failures, _output);
    }
    break;
  case printNothing:
    break;
  }
}

// ----------------------------------------------------------------------------
void ccunit::TestRun::onTestRunStart()
{
  if (_mode != printNothing) {
    _output << ANSI_CYAN "ccunit" ANSI_RESET " - Simple Unit Testing for C++\n";
  }
}

// ----------------------------------------------------------------------------
void ccunit::TestRun::onTestRunComplete(const Failures& failures)
{
  if (_mode != printNothing) {
    printTestResult(_output);
  }
}

// ----------------------------------------------------------------------------
void ccunit::TestRun::runSingleTest(const Test& test)
{
  onTestStart(test);

  test.run();
  ++_numTestsRun;

  onTestComplete(test, _currentFailures);

  if (!_currentFailures.empty()) {
    ++_numTestsFailed;
    emplace_back(_allFailures, _currentFailures);
  }
}

// ----------------------------------------------------------------------------
size_t ccunit::TestRun::runTests(const std::vector<std::unique_ptr<Test>>& tests)
{
  onTestRunStart();

  for (const auto& test : tests) {
    runSingleTest(*test);
  }

  onTestRunComplete(_allFailures);

  return getTestResult();
}

// ----------------------------------------------------------------------------
void ccunit::TestRun::printTestResult(std::ostream& output) const
{
  const int width[5] = { 4, 8, 8, 8, 8 };

  // clang-format off
  output << "\n\n" ANSI_BOLD "Run Summary:\n" ANSI_RESET
    << std::setw(width[0]) << " "
    << std::setw(width[1]) << " "
    << std::setw(width[2]) << "Total"
    << std::setw(width[3]) << "Passed"
    << std::setw(width[4]) << "Failed" << "\n"
    << std::setw(width[0]) << " "
    << std::setw(width[1]) << "tests"
    << std::setw(width[2]) << _numTestsRun
    << std::setw(width[3]) << _numTestsRun - _numTestsFailed
    << std::setw(width[4]) << _numTestsFailed << "\n"
    << std::setw(width[0]) << " "
    << std::setw(width[1]) << "checks"
    << std::setw(width[2]) << _numChecks[0]
    << std::setw(width[3]) << _numChecks[0] - _numChecksFailed[0]
    << std::setw(width[4]) << _numChecksFailed[0] << "\n"
    << std::setw(width[0]) << " "
    << std::setw(width[1]) << "asserts"
    << std::setw(width[2]) << _numChecks[1]
    << std::setw(width[3]) << _numChecks[1] - _numChecksFailed[1]
    << std::setw(width[4]) << _numChecksFailed[1]
    << "\n";
  // clang-format on
}

// ----------------------------------------------------------------------------
size_t ccunit::TestRun::getTestResult() const
{
  return _allFailures.size();
}
