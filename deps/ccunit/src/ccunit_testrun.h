/*
 * MIT License
 * Copyright (c) 2024
 */

#pragma once

#include "ccunit.h"
#include "ccunit_failure.h"

namespace ccunit {

// ----------------------------------------------------------------------------
class TestRun
{
public:
  TestRun(VerboseMode mode, std::ostream& output);

  size_t runTests(const Tests& tests);
  void printTestResult(std::ostream& output) const;

  int check(int value, const std::string& condition,
    const std::string& file, unsigned line, int fatal);

private:
  void runSingleTest(const Test& test);

  void onTestStart(const Test& test);
  void onTestComplete(const Test& test, const Failures& failures);
  void onTestRunStart();
  void onTestRunComplete(const Failures& failures);

  const std::string getTestResultDesc() const;
  size_t getTestResult() const;

  unsigned _numTestsRun = 0;
  unsigned _numTestsFailed = 0;

  // fatal(1) and non-fatal(0)
  unsigned _numChecks[2] = { 0, 0 };
  unsigned _numChecksFailed[2] = { 0, 0 };

  Failures _allFailures;
  Failures _currentFailures;

  VerboseMode _mode;
  std::ostream& _output;
};

} // namespace ccunit
