#include <cfloat>
#include <iostream>

#include "ccunit.h"

// ----------------------------------------------------------------------------
const ccunit::Tests test0()
{
  ccunit::Tests tests;

  tests.push_back(std::make_unique<ccunit::Test>("Test 0", []() {
  }));

  return tests;
}

// ----------------------------------------------------------------------------
const ccunit::Tests test1()
{
  ccunit::Tests tests;

  tests.push_back(std::make_unique<ccunit::Test>("Test 1", []() {
    EXPECT_TRUE(1 == 1);
    ASSERT_TRUE(1 == 1);
  }));

  tests.push_back(std::make_unique<ccunit::Test>("Test 2", []() {
    ASSERT_TRUE(1 == 2); // expected to fail
    EXPECT_TRUE(1 == 2); // not executed due to the previous assertion
  }));

  tests.push_back(std::make_unique<ccunit::Test>("Test 3", []() {
    EXPECT_TRUE(1 == 3); // expected to fail
    ASSERT_TRUE(1 == 3); // expected to fail
  }));

  tests.push_back(std::make_unique<ccunit::Test>("Test 4", []() {
    // expected to succeed
    EXPECT_FLOAT_EQ(1.0f, 1.0f);
    EXPECT_FLOAT_EQ(FLT_MAX, FLT_MAX);
    EXPECT_FLOAT_EQ(FLT_MIN, FLT_MIN);
    EXPECT_FLOAT_EQ(1.0f + 4 * FLT_EPSILON, 1.0f); // 4 ULPs difference is OK
    EXPECT_FLOAT_NEAR(1.0f + 5 * FLT_EPSILON, 1.0f); // 5 ULPs difference is OK for near

    // expected to fail
    EXPECT_FLOAT_EQ(1.0f + 5 * FLT_EPSILON, 1.0f); // 5 ULPs difference is not OK
    EXPECT_FLOAT_EQ(-1.0f, 1.0f);
    EXPECT_FLOAT_NEAR(1.0f + 1e-5, 1.0f); // 1e-5 difference is not OK for near
  }));

  return tests;
}

// ----------------------------------------------------------------------------
int main(int argc, char* argv[])
{
  const size_t exp0 = ccunit::runTests(test0(), ccunit::VerboseMode::printEverything);
  const int res0 = exp0 != 0;

  const size_t exp1 = ccunit::runTests(test1(), ccunit::VerboseMode::printFailuresOnly);
  const int res1 = exp1 != 6;

  return res0 | res1;
}
