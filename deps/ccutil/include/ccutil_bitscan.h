#pragma once

#include <stdint.h>

#if defined(_MSC_VER)

#include <intrin.h>

// ----------------------------------------------------------------------------
inline uint32_t rotr32(uint32_t value, unsigned shift)
{
  return _lrotr(value, shift);
}

// ----------------------------------------------------------------------------
inline uint64_t rotr64(uint64_t value, unsigned shift)
{
  return _rotr64(value, shift);
}

// ----------------------------------------------------------------------------
inline uint32_t rotl32(uint32_t value, unsigned shift)
{
  return _lrotl(value, shift);
}

// ----------------------------------------------------------------------------
inline uint64_t rotl64(uint64_t value, unsigned shift)
{
  return _lrotl(value, shift);
}

// ----------------------------------------------------------------------------
#pragma intrinsic(_BitScanForward)
inline unsigned bsf32(uint32_t bits)
{
  unsigned long index = 0;
  _BitScanForward(&index, bits);
  return index;
}

// ----------------------------------------------------------------------------
#pragma intrinsic(_BitScanForward64)
inline unsigned bsf64(uint64_t bits)
{
  unsigned long index = 0;
  _BitScanForward64(&index, bits);
  return index;
}

// ----------------------------------------------------------------------------
#pragma intrinsic(_BitScanReverse)
inline unsigned bsr32(uint32_t bits)
{
  unsigned long index = 0;
  _BitScanReverse(&index, bits);
  return index;
}

#pragma intrinsic(_BitScanReverse64)
inline unsigned bsr64(uint64_t bits)
{
  unsigned long index = 0;
  _BitScanReverse64(&index, bits);
  return index;
}

#else // defined(_MSC_VER)

// ----------------------------------------------------------------------------
inline unsigned bsf32(uint32_t bits)
{
  return __builtin_ffs(bits);
}

// ----------------------------------------------------------------------------
inline unsigned bsf64(uint64_t bits)
{
  return __builtin_ffsll(bits);
}

// ----------------------------------------------------------------------------
inline unsigned bsr32(uint32_t bits)
{
  return 31 - __builtin_clz(bits);
}

// ----------------------------------------------------------------------------
inline unsigned bsr64(uint64_t bits)
{
  return 63 - __builtin_clzll(bits);
}

#endif // defined(_MSC_VER)
