#pragma once

#include <string>

// include platform specific impl for File class
#if defined(_WIN32)
# include "ccutil_file_win32.h"
#else
# include "ccutil_file_posix.h"
#endif

namespace ccutil {

File createFile(const std::string& path);

} // namespace ccutil

