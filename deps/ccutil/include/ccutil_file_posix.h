#pragma once

#include <string>

#include "ccutil_retcode.h"
#include "ccutil_filesystem.h"

namespace ccutil {

// ----------------------------------------------------------------------------
enum FileMoveMode
{
  moveBegin = SEEK_SET,
  moveCurrent = SEEK_CUR,
  moveEnd = SEEK_END,
};

// ----------------------------------------------------------------------------
class File
{
public:
  File(const std::string& path, FileCreateMode mode);
  ~File();

  size_t read(void* data, size_t length);
  size_t write(const void* data, size_t length);
  size_t seek(size_t length, FileMoveMode mode);

  retcode validate() const;
  uint64_t pointer() const;
  uint64_t size() const;

  const std::string path() const;

private:
  int _handle = -1;
  retcode _rv = rcSuccess;
  std::string _path;
};

} // namespace ccutil
