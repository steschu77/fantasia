#pragma once

// do not include rare used Win32 Headers
#define WIN32_LEAN_AND_MEAN
#define _WIN32_WINNT 0x0501
#include <windows.h>

#include <string>

#include "ccutil_retcode.h"
#include "ccutil_filesystem.h"

namespace ccutil {

// ----------------------------------------------------------------------------
enum FileMoveMode
{
  moveBegin = FILE_BEGIN,
  moveCurrent = FILE_CURRENT,
  moveEnd = FILE_END,
};

class File
{
public:
  File(const std::string& path, FileCreateMode mode);
  ~File();

  size_t read(void* data, size_t length);
  size_t write(const void* data, size_t length);
  size_t seek(size_t length, FileMoveMode mode);

  retcode validate() const;
  uint64_t pointer() const;
  uint64_t size() const;

  const std::string path() const;

private:
  HANDLE _handle = nullptr;
  retcode _rv = rcSuccess;
  std::string _path;
};

} // namespace ccutil
