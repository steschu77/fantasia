#pragma once

#include <string>
#include <vector>

#include "ccutil_retcode.h"

namespace ccutil {

// ----------------------------------------------------------------------------
class Path
{
public:
  Path(const std::string& path, const std::string& dir, const std::string& name, const std::string& ext);

  std::string path() const;
  std::string directory() const;
  std::string filename() const;
  std::string extension() const;

private:
  std::string _path;
  std::string _directory;
  std::string _filename;
  std::string _extension;
};

Path createPath(const std::string& path);

std::vector<std::string> splitPath(const std::string& path);

} // namespace ccutil

// ----------------------------------------------------------------------------
inline ccutil::Path::Path(const std::string& path, const std::string& dir, const std::string& name, const std::string& ext)
: _path(path)
, _directory(dir)
, _filename(name)
, _extension(ext)
{
}

// ----------------------------------------------------------------------------
inline std::string ccutil::Path::path() const
{
  return _path;
}

// ----------------------------------------------------------------------------
inline std::string ccutil::Path::directory() const
{
  return _directory;
}

// ----------------------------------------------------------------------------
inline std::string ccutil::Path::filename() const
{
  return _filename;
}

// ----------------------------------------------------------------------------
inline std::string ccutil::Path::extension() const
{
  return _extension;
}

