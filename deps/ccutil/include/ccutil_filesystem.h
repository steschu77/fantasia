#pragma once

#include <string>
#include <functional>

#include "ccutil_retcode.h"

namespace ccutil {

// ----------------------------------------------------------------------------
enum FileCreateMode
{
  f_existing = 0,
  f_write = 1,
  f_overwrite = 2
};

bool fileExists(const std::string& path);

// ----------------------------------------------------------------------------
enum DirectoryListingFlags
{
  f_recursive   = (1 << 0),
  f_files       = (1 << 1),
  f_directories = (1 << 2),
};

typedef std::function<int(const std::string&, unsigned attrib)> FnFileCb;
retcode listDirectory(const std::string& path, unsigned flags, FnFileCb cb);

retcode makeDirectory(const std::string& path);

}
