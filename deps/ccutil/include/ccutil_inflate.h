#pragma once

#include <stddef.h>
#include <stdint.h>

// ----------------------------------------------------------------------------
size_t inflate(uint8_t* dst, size_t cdst, const uint8_t* src, size_t csrc);
size_t zlib_decompress(uint8_t* dst, size_t cdst, const uint8_t* src, size_t csrc);
