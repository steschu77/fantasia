#pragma once

#include <string>

#include "ccutil_retcode.h"

// ----------------------------------------------------------------------------
#define rcsetXmlErrorCodes (-0x40010000)

// clang-format off
enum XmlReturnCode
{
  rcXmlParsing            = rcsetXmlErrorCodes | 0x0000,
  rcXmlIncompleteFile     = rcsetXmlErrorCodes | 0x0001,
  rcXmlStringExpected     = rcsetXmlErrorCodes | 0x0002,
  rcXmlEqualSignExpected  = rcsetXmlErrorCodes | 0x0003,
  rcXmlCloseTagExpected   = rcsetXmlErrorCodes | 0x0004,
  rcXmlPINameExpected     = rcsetXmlErrorCodes | 0x0005,
  rcXmlPIValueExpected    = rcsetXmlErrorCodes | 0x0006,
  rcXmlUnexpectedToken    = rcsetXmlErrorCodes | 0x0007,
};
// clang-format on

namespace ccutil {

// ----------------------------------------------------------------------------
class IReader
{
public:
  virtual int readChar() = 0;
};

// ----------------------------------------------------------------------------
class IXmlEvents
{
public:
  virtual ~IXmlEvents() = default;

  virtual retcode onElementBegin(const std::string&) { return rcSuccess; }
  virtual retcode onElementEnd(const std::string&) { return rcSuccess; }
  virtual retcode onAttribute(const std::string&, const std::string&) { return rcSuccess; }
  virtual retcode onCData(const std::string&) { return rcSuccess; }
  virtual retcode onProcessingInstruction(const std::string&) { return rcSuccess; }
};

// ----------------------------------------------------------------------------
retcode parseXml(IReader* read, IXmlEvents* events);

} // namespace ccutil
