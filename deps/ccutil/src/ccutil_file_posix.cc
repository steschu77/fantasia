#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <wordexp.h>

#include "ccutil_file_posix.h"

#define PATH_MAX 4096

// ----------------------------------------------------------------------------
static retcode mapPosixErrorCode();
static int isValidHandle(int h);

// ----------------------------------------------------------------------------
static const unsigned gMode[3] =
{
  O_RDONLY,
  O_WRONLY | O_NONBLOCK | O_CREAT | O_EXCL,
  O_WRONLY | O_NONBLOCK | O_CREAT | O_TRUNC,
};

// ----------------------------------------------------------------------------
static const unsigned gPermissions[3] =
{
  0,
  S_IRUSR | S_IWUSR,
  S_IRUSR | S_IWUSR,
};

// ----------------------------------------------------------------------------
ccutil::File::File(const std::string& path, FileCreateMode mode)
: _path(path)
{
  char resolved_name[PATH_MAX + 1];
  char* absolute;

  wordexp_t p;
  if (wordexp(path.c_str(), &p, 0) == 0) {
    absolute = realpath(p.we_wordv[0], resolved_name);
    wordfree(&p);
  } else {
    absolute = realpath(path.c_str(), resolved_name);
  }

  if (absolute || errno == 0x2) {
    _handle = open(resolved_name, gMode[mode], gPermissions[mode]);
  }

  _rv = mapPosixErrorCode();
}

// ----------------------------------------------------------------------------
ccutil::File::~File()
{
  if (isValidHandle(_handle)) {
    close(_handle);
  }
}

// ----------------------------------------------------------------------------
size_t ccutil::File::read(void* data, size_t length)
{
  return ::read(_handle, data, length);
}

// ----------------------------------------------------------------------------
size_t ccutil::File::write(const void* data, size_t length)
{
  return ::write(_handle, data, length);
}

// ----------------------------------------------------------------------------
size_t ccutil::File::seek(size_t length, FileMoveMode mode)
{
  return (size_t)lseek(_handle, length, mode);
}

// ----------------------------------------------------------------------------
uint64_t ccutil::File::size() const
{
  struct stat fileStat;
  if (fstat(_handle, &fileStat) != 0) {
    return 0;
  }

  return fileStat.st_size;
}

// ----------------------------------------------------------------------------
retcode mapPosixErrorCode()
{
  switch (errno) {
  case 0:
    return rcSuccess;
  case ENOENT:
    return rcFileNotFound;
  case EPERM:
  case EACCES:
    return rcAccessDenied;
  case EMFILE:
    return rcTooManyOpenFiles;
  default:
    return rcFailed;
  }
}

// ----------------------------------------------------------------------------
int isValidHandle(int handle)
{
  return handle >= 0;
}
