#include "ccutil_file_win32.h"

// ----------------------------------------------------------------------------
static retcode mapWin32ErrorCode(DWORD error);
static int isValidHandle(HANDLE h);

// ----------------------------------------------------------------------------
static const unsigned gSecurity[3] =
{
  FILE_GENERIC_READ,
  FILE_GENERIC_WRITE,
  FILE_GENERIC_WRITE,
};

// ----------------------------------------------------------------------------
static const unsigned gShare[3] =
{
  FILE_SHARE_READ,
  0,
  0,
};

// ----------------------------------------------------------------------------
static const unsigned gCreation[3] =
{
  OPEN_EXISTING,
  CREATE_NEW,
  CREATE_ALWAYS,
};

// ----------------------------------------------------------------------------
ccutil::File::File(const std::string& path, FileCreateMode mode)
: _path(path)
{
  _handle = CreateFileA(path.c_str(), gSecurity[mode], gShare[mode], nullptr,
    gCreation[mode], FILE_ATTRIBUTE_NORMAL, nullptr);
  _rv = mapWin32ErrorCode(GetLastError());
}

// ----------------------------------------------------------------------------
ccutil::File::~File()
{
  if (isValidHandle(_handle)) {
    CloseHandle(_handle);
  }
}

// ----------------------------------------------------------------------------
size_t ccutil::File::read(void* data, size_t length)
{
  if (_rv != rcSuccess) {
    return 0;
  }

  DWORD dwLength = (DWORD)length;
  if (dwLength != length) {
    _rv = rcInvalidParam;
    return 0;
  }

  DWORD dwBytes = 0;
  if (!ReadFile(_handle, data, dwLength, &dwBytes, nullptr)) {
    _rv = rcReadingFile;
    return 0;
  }

  return dwBytes;
}

// ----------------------------------------------------------------------------
size_t ccutil::File::write(const void* data, size_t length)
{
  if (_rv != rcSuccess) {
    return 0;
  }

  DWORD dwLength = (DWORD)length;
  if (dwLength != length) {
    _rv = rcInvalidParam;
    return 0;
  }

  DWORD dwBytes = 0;
  if (!WriteFile(_handle, data, dwLength, &dwBytes, nullptr)) {
    _rv = rcWritingFile;
    return 0;
  }

  return dwBytes;
}

// ----------------------------------------------------------------------------
size_t ccutil::File::seek(size_t length, FileMoveMode mode)
{
  LARGE_INTEGER distance = { { 0, 0 } }, pos = { { 0, 0 } };
  distance.QuadPart = (LONGLONG)length;
  SetFilePointerEx(_handle, distance, &pos, mode);
  return static_cast<size_t>(pos.QuadPart);
}

// ----------------------------------------------------------------------------
retcode ccutil::File::validate() const
{
  return _rv;
}

// ----------------------------------------------------------------------------
uint64_t ccutil::File::pointer() const
{
  LARGE_INTEGER distance = { { 0, 0 } }, pos = { { 0, 0 } };
  SetFilePointerEx(_handle, distance, &pos, FILE_CURRENT);
  return static_cast<size_t>(pos.QuadPart);
}

// ----------------------------------------------------------------------------
uint64_t ccutil::File::size() const
{
  LARGE_INTEGER size;
  if (!GetFileSizeEx(_handle, &size)) {
    return 0;
  }

  return size.QuadPart;
}

// ----------------------------------------------------------------------------
const std::string ccutil::File::path() const
{
  return _path;
}

// ----------------------------------------------------------------------------
retcode mapWin32ErrorCode(DWORD error)
{
  switch (error) {
  case NO_ERROR:
    return rcSuccess;
  case ERROR_FILE_NOT_FOUND:
    return rcFileNotFound;
  case ERROR_PATH_NOT_FOUND:
    return rcPathNotFound;
  case ERROR_TOO_MANY_OPEN_FILES:
    return rcTooManyOpenFiles;
  case ERROR_ACCESS_DENIED:
    return rcAccessDenied;
  case ERROR_ALREADY_EXISTS:
    return rcAlreadyExists;
  default:
    return rcFailed;
  }
}

// ----------------------------------------------------------------------------
static int isValidHandle(HANDLE h)
{
  return h != 0 && h != INVALID_HANDLE_VALUE;
}
