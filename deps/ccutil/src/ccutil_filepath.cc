#include "ccutil_filepath.h"

#include <string.h>

// ----------------------------------------------------------------------------
ccutil::Path ccutil::createPath(const std::string& path)
{
  static const char* strEmpty = "";
  const char* strPath = path.c_str();
  const char* strLastFSlash = strrchr(strPath, '/');
  const char* strLastBSlash = strrchr(strPath, '\\');
  const char* strLastSlash = std::max(strLastFSlash, strLastBSlash);
  const char* strLastDot = strrchr(strPath, '.');

  const char* strDir = strPath;
  const char* strName = (strLastSlash == nullptr) ? strPath : strLastSlash + 1;
  const char* strExt = (strLastDot == nullptr || strLastDot < strLastSlash) ? strEmpty : strLastDot;

  size_t lenExt = strlen(strExt);
  size_t lenName = strExt - strName;
  size_t lenDir = strName - strPath;

  std::string dir(strDir, lenDir);
  std::string name(strName, lenName);
  std::string ext(strExt, lenExt);

  ccutil::Path obj { path, dir, name, ext };
  return obj;
}

// ----------------------------------------------------------------------------
std::vector<std::string> ccutil::splitPath(const std::string& str)
{
  std::vector<std::string> splits;
  size_t start = 0;

  do {
    size_t end = std::min(str.find('/', start), str.find('\\', start));
    if (end == std::string::npos) {
      splits.push_back(str.substr(start));
      break;
    } else if (start != end) {
      splits.push_back(str.substr(start, end - start));
    }
    start = end + 1;
  } while (start < str.length());

  return splits;
}
