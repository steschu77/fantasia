#include "ccutil_filesystem.h"
#include "ccutil_filepath.h"

#include <windows.h>

// ----------------------------------------------------------------------------
bool ccutil::fileExists(const std::string& path)
{
  HANDLE h = CreateFileA(path.c_str(), GENERIC_READ, FILE_SHARE_READ,
    nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

  if (h == INVALID_HANDLE_VALUE) {
    const unsigned code = GetLastError();
    return code != ERROR_FILE_NOT_FOUND && code != ERROR_PATH_NOT_FOUND;
  }

  CloseHandle(h);
  return true;
}

// ----------------------------------------------------------------------------
retcode ccutil::listDirectory(const std::string& path, unsigned flags, FnFileCb fnCb)
{
  const std::string pattern = path + "\\*.*";
  WIN32_FIND_DATAA fData;
  HANDLE h = FindFirstFileA(pattern.c_str(), &fData);
  if (h == INVALID_HANDLE_VALUE) {
    return rcFailed;
  }

  unsigned match = 0;
  if ((flags & f_files) != 0) {
    match |= FILE_ATTRIBUTE_DIRECTORY;
  }
  if ((flags & f_directories) != 0) {
    match |= FILE_ATTRIBUTE_DIRECTORY;
  }

  retcode rv = rcSuccess;
  do
  {
    const std::string filename = fData.cFileName;
    const std::string filepath = path + filename;

    if (filename != "." && filename != ".."
      && (fData.dwFileAttributes & match) != 0) {
      rv = fnCb(filepath, fData.dwFileAttributes);
    }

    if (fData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
      rv = listDirectory(filepath, flags, fnCb);
    }


  } while (rv == rcSuccess && FindNextFileA(h, &fData));

  FindClose(h);
  return rv;
}

// ----------------------------------------------------------------------------
retcode ccutil::makeDirectory(const std::string& path)
{
  const std::vector<std::string> directories = ccutil::splitPath(path);
  
  for (const auto& directory : directories) {

    if (!CreateDirectoryA(directory.c_str(), NULL)) {
      DWORD errorCode = GetLastError();
      if (errorCode != ERROR_ALREADY_EXISTS) {
        return rcFailed;
      }
    }
  }

  return rcSuccess;
}
