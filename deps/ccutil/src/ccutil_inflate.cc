#include "ccutil_inflate.h"
#include "ccutil_retcode.h"

#include <string.h>

// ----------------------------------------------------------------------------
unsigned getbit(size_t* pos, const uint8_t* ptr)
{
  const size_t bytepos = *pos >> 3;
  const size_t shift = *pos & 7;

  (*pos)++;

  return (ptr[bytepos] >> shift) & 1;
}

// ----------------------------------------------------------------------------
#if 0
// the following would be a more efficient version of getbits, but needs
// access to up to 3 bytes after the end of the input buffer
inline unsigned getbits(size_t* pos, const uint8_t* ptr, unsigned count)
{
  const size_t bytepos = *pos >> 3;
  const size_t shift = *pos & 7;
  const unsigned mask = (1u << count) - 1u;

  *pos += count;

  return (((const unsigned*)(ptr + bytepos))[0] >> shift) & mask;
}
#else
unsigned getbits(size_t* pos, const uint8_t* ptr, unsigned count)
{
  unsigned bits = 0;
  for (unsigned i = 0; i < count; i++) {
    bits |= getbit(pos, ptr) << i;
  }
  return bits;
}
#endif

// ----------------------------------------------------------------------------
unsigned showbits9(size_t* pos, const uint8_t* ptr)
{
  const size_t bytepos = *pos >> 3;
  const size_t shift = *pos & 7;
  return (((const uint16_t*)(ptr + bytepos))[0] >> shift) & 0x1ff;
}

// ----------------------------------------------------------------------------
#define FIRST_LENGTH_CODE_INDEX 257
#define LAST_LENGTH_CODE_INDEX 285
#define NUM_DEFLATE_CODE_SYMBOLS 288
#define NUM_DISTANCE_SYMBOLS 32
#define NUM_CODE_LENGTH_CODES 19

// ----------------------------------------------------------------------------
struct info_s {
  uint8_t len;
  uint16_t base;
};

typedef struct info_s info_t;

// ----------------------------------------------------------------------------
// clang-format off
static const info_t gDistInfo[30] =
{
  { 0,    1}, { 0,    2}, { 0,    3}, { 0,    4},
  { 1,    5}, { 1,    7}, { 2,    9}, { 2,   13},
  { 3,   17}, { 3,   25}, { 4,   33}, { 4,   49},
  { 5,   65}, { 5,   97}, { 6,  129}, { 6,  193},
  { 7,  257}, { 7,  385}, { 8,  513}, { 8,  769},
  { 9, 1025}, { 9, 1537}, {10, 2049}, {10, 3073},
  {11, 4097}, {11, 6145}, {12, 8193}, {12,12289},
  {13,16385}, {13,24577},
};

// ----------------------------------------------------------------------------
static const info_t gCodeInfo[29] =
{
  { 0,    3}, { 0,    4}, { 0,    5}, { 0,    6},
  { 0,    7}, { 0,    8}, { 0,    9}, { 0,   10},
  { 1,   11}, { 1,   13}, { 1,   15}, { 1,   17},
  { 2,   19}, { 2,   23}, { 2,   27}, { 2,   31},
  { 3,   35}, { 3,   43}, { 3,   51}, { 3,   59},
  { 4,   67}, { 4,   83}, { 4,   99}, { 4,  115},
  { 5,  131}, { 5,  163}, { 5,  195}, { 5,  227},
  { 0,  258},
};
// clang-format on

// ----------------------------------------------------------------------------
static const unsigned gCodeLenPerm[NUM_CODE_LENGTH_CODES]
  = { 16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15 };

// ----------------------------------------------------------------------------
enum CompressMethod {
  methodDeflated = 8,
};

// ----------------------------------------------------------------------------
enum CompressFlags {
  flagDictionary = 0x20,
};

// ----------------------------------------------------------------------------
struct vlc_s {
  uint16_t ct;
  uint16_t len;
};

typedef struct vlc_s vlc_t;

// ----------------------------------------------------------------------------
struct HuffmanTree_s {
  uint16_t codes[320];
  uint16_t lengths[320];
  unsigned maxbitlen;
  unsigned numcodes;

  vlc_t LookupTable[512 + 512];
};

typedef struct HuffmanTree_s HuffmanTree;

// ----------------------------------------------------------------------------
void initHuffmanTree(HuffmanTree* tree, unsigned numcodes, unsigned maxbitlen)
{
  tree->maxbitlen = maxbitlen;
  tree->numcodes = numcodes;
}

#define FIRSTBITS 9u
#define INVALIDSYMBOL 65535u

static unsigned reverseBits9(unsigned bits)
{
  static const uint8_t reverse4[16] = {
    0x0, 0x8, 0x4, 0xc, 0x2, 0xa, 0x6, 0xe,
    0x1, 0x9, 0x5, 0xd, 0x3, 0xb, 0x7, 0xf
  };

  return (reverse4[bits & 0xf] << 5) | (bits & 0x10) | (reverse4[bits >> 5]);
}

static unsigned reverseBits(unsigned bits, unsigned num)
{
  /*TODO: implement faster lookup table based version when needed*/
  unsigned result = 0;
  for (unsigned i = 0; i < num; i++) {
    result |= ((bits >> (num - i - 1u)) & 1u) << i;
  }
  return result;
}

/* make table for huffman decoding */
static unsigned HuffmanTree_makeTable(HuffmanTree* tree)
{
  static const unsigned headsize = 1u << FIRSTBITS; /*size of the first table*/
  static const unsigned mask = (1u << FIRSTBITS) /*headsize*/ - 1u;
  unsigned maxlens[1u << FIRSTBITS] = { 0 };

  /* compute maxlens: max total bit length of symbols sharing prefix in the first table*/
  for (unsigned i = 0; i < tree->numcodes; i++) {
    const unsigned l = tree->lengths[i];
    if (l <= FIRSTBITS) {
      continue; /*symbols that fit in first table don't increase secondary table size*/
    }

    /*get the FIRSTBITS MSBs, the MSBs of the symbol are encoded first. See later comment about the reversing*/
    const unsigned symbol = tree->codes[i];
    const unsigned index = reverseBits9(symbol >> (l - FIRSTBITS));
    maxlens[index] = maxlens[index] > l ? maxlens[index] : l;
  }

  /* compute total table size: size of first table plus all secondary tables for symbols longer than FIRSTBITS */
  unsigned size = headsize;
  for (unsigned i = 0; i < headsize; ++i) {
    const unsigned l = maxlens[i];
    if (l > FIRSTBITS) {
      size += (1u << (l - FIRSTBITS));
    }
  }

  /*initialize with an invalid length to indicate unused entries*/
  for (unsigned i = 0; i < size; ++i) {
    tree->LookupTable[i].len = 16;
  }

  /*fill in the first table for long symbols: max prefix size and pointer to secondary tables*/
  unsigned pointer = headsize;
  for (unsigned i = 0; i < headsize; ++i) {
    const unsigned l = maxlens[i];
    if (l <= FIRSTBITS) {
      continue;
    }
    tree->LookupTable[i].len = l;
    tree->LookupTable[i].ct = pointer;
    pointer += (1u << (l - FIRSTBITS));
  }

  /*fill in the first table for short symbols, or secondary table for long symbols*/
  unsigned numpresent = 0;
  for (unsigned i = 0; i < tree->numcodes; ++i) {
    const unsigned l = tree->lengths[i];
    if (l == 0) {
      continue;
    }

    // the huffman bit pattern. i itself is the value. reverse bits,
    // because the huffman bits are given in MSB first order but the bit reader reads LSB first
    const unsigned reverse = reverseBits(tree->codes[i], l);
    numpresent++;

    if (l <= FIRSTBITS) {
      // short symbol, fully in first table, replicated num times if l < FIRSTBITS
      const unsigned num = 1u << (FIRSTBITS - l);
      for (unsigned j = 0; j < num; ++j) {
        /*bit reader will read the l bits of symbol first, the remaining FIRSTBITS - l bits go to the MSB's*/
        const unsigned index = reverse | (j << l);
        if (tree->LookupTable[index].len != 16) {
          return rcInvalidFormat; /*invalid tree: long symbol shares prefix with short symbol*/
        }
        tree->LookupTable[index].len = l;
        tree->LookupTable[index].ct = i;
      }
    } else {
      /*long symbol, shares prefix with other long symbols in first lookup table, needs second lookup*/
      /*the FIRSTBITS MSBs of the symbol are the first table index*/
      unsigned index = reverse & mask;
      unsigned maxlen = tree->LookupTable[index].len;
      /*log2 of secondary table length, should be >= l - FIRSTBITS*/
      unsigned tablelen = maxlen - FIRSTBITS;
      unsigned start = tree->LookupTable[index].ct; /*starting index in secondary table*/
      unsigned num = 1u << (tablelen - (l - FIRSTBITS)); /*amount of entries of this symbol in secondary table*/
      unsigned j;
      if (maxlen < l)
        return 55; /*invalid tree: long symbol shares prefix with short symbol*/
      for (j = 0; j < num; ++j) {
        unsigned reverse2 = reverse >> FIRSTBITS; /* l - FIRSTBITS bits */
        unsigned index2 = start + (reverse2 | (j << (l - FIRSTBITS)));
        tree->LookupTable[index2].len = l;
        tree->LookupTable[index2].ct = i;
      }
    }
  }

  if (numpresent < 2) {
    /* In case of exactly 1 symbol, in theory the huffman symbol needs 0 bits,
    but deflate uses 1 bit instead. In case of 0 symbols, no symbols can
    appear at all, but such huffman tree could still exist (e.g. if distance
    codes are never used). In both cases, not all symbols of the table will be
    filled in. Fill them in with an invalid symbol value so returning them from
    huffmanDecodeSymbol will cause error. */
    for (unsigned i = 0; i < size; ++i) {
      if (tree->LookupTable[i].len == 16) {
        /* As length, use a value smaller than FIRSTBITS for the head table,
        and a value larger than FIRSTBITS for the secondary table, to ensure
        valid behavior for advanceBits when reading this symbol. */
        tree->LookupTable[i].len = (i < headsize) ? 1 : (FIRSTBITS + 1);
        tree->LookupTable[i].ct = INVALIDSYMBOL;
      }
    }
  } else {
    /* A good huffman tree has N * 2 - 1 nodes, of which N - 1 are internal nodes.
    If that is not the case (due to too long length codes), the table will not
    have been fully used, and this is an error (not all bit combinations can be
    decoded): an oversubscribed huffman tree, indicated by error 55. */
    for (unsigned i = 0; i < size; ++i) {
      if (tree->LookupTable[i].len == 16)
        return 55;
    }
  }

  return 0;
}

// ----------------------------------------------------------------------------
static retcode HuffmanTree_makeFromLengths2(HuffmanTree* tree)
{
  // count number of instances of each code length
  unsigned blcount[16] = { 0 };
  for (unsigned bits = 0; bits < tree->numcodes; bits++) {
    blcount[tree->lengths[bits]]++;
  }

  unsigned nextcode[16] = { 0 };
  for (unsigned bits = 1; bits <= tree->maxbitlen; bits++) {
    nextcode[bits] = (nextcode[bits - 1] + blcount[bits - 1]) << 1;
  }

  for (unsigned n = 0; n < tree->numcodes; n++) {
    if (tree->lengths[n] != 0) {
      tree->codes[n] = nextcode[tree->lengths[n]]++;
      tree->codes[n] &= ((1u << tree->lengths[n]) - 1u);
    }
  }

  return HuffmanTree_makeTable(tree);
}

// ----------------------------------------------------------------------------
static retcode HuffmanTree_makeFromLengths(HuffmanTree* tree,
  const unsigned* bitlen, size_t valid, size_t numcodes, unsigned maxbitlen)
{
  for (size_t i = 0; i < numcodes; i++) {
    tree->lengths[i] = i < valid ? bitlen[i] : 0;
  }
  tree->numcodes = (unsigned)numcodes;
  tree->maxbitlen = maxbitlen;
  return HuffmanTree_makeFromLengths2(tree);
}

// ----------------------------------------------------------------------------
static retcode generateFixedLitLenTree(HuffmanTree* tree)
{
  for (int i = 0; i <= 143; i++) {
    tree->lengths[i] = 8;
  }
  for (int i = 144; i <= 255; i++) {
    tree->lengths[i] = 9;
  }
  for (int i = 256; i <= 279; i++) {
    tree->lengths[i] = 7;
  }
  for (int i = 280; i <= 287; i++) {
    tree->lengths[i] = 8;
  }
  tree->numcodes = NUM_DEFLATE_CODE_SYMBOLS;
  tree->maxbitlen = 15;

  return HuffmanTree_makeFromLengths2(tree);
}

// ----------------------------------------------------------------------------
static retcode generateFixedDistanceTree(HuffmanTree* tree)
{
  for (int i = 0; i < NUM_DISTANCE_SYMBOLS; i++) {
    tree->lengths[i] = 5;
  }

  tree->numcodes = NUM_DISTANCE_SYMBOLS;
  tree->maxbitlen = 15;

  return HuffmanTree_makeFromLengths2(tree);
}

// ----------------------------------------------------------------------------
static retcode huffmanDecodeSymbol(
  const unsigned char* in, size_t* bp, size_t inlength, const HuffmanTree* codetree, unsigned* code)
{
  const size_t inbitlength = inlength * 8;
  const unsigned bits9 = showbits9(bp, in);
  const unsigned ct0 = codetree->LookupTable[bits9].ct;
  const unsigned len0 = codetree->LookupTable[bits9].len;

  if (len0 <= FIRSTBITS) {
    if (inbitlength - *bp < len0) {
      return rcBufferUnderrun;
    }
    (*bp) += len0;
    *code = ct0;
    return rcSuccess;
  }

  (*bp) += FIRSTBITS;
  const unsigned count = len0 - FIRSTBITS;
  const unsigned bits = showbits9(bp, in) & ((1u << count) - 1);
  const unsigned idx = ct0 + bits;

  const unsigned ct = codetree->LookupTable[idx].ct;
  const unsigned len = codetree->LookupTable[idx].len;

  (*bp) += len - FIRSTBITS;
  *code = ct;
  return rcSuccess;
}

// ----------------------------------------------------------------------------
static retcode getTreeInflateDynamic(HuffmanTree* tree_ll, HuffmanTree* tree_d,
  const unsigned char* in, size_t* bp, size_t inlength)
{
  retcode rv;

  size_t inbitlength = inlength * 8;

  if (inbitlength - *bp < 14) {
    return rcBufferUnderrun;
  }

  unsigned HLIT = getbits(bp, in, 5) + 257;
  unsigned HDIST = getbits(bp, in, 5) + 1;
  unsigned HCLEN = getbits(bp, in, 4) + 4;

  if (inbitlength - *bp < HCLEN * 3) {
    return rcBufferUnderrun;
  }

  HuffmanTree tree_cl;
  initHuffmanTree(&tree_cl, NUM_CODE_LENGTH_CODES, 7);

  unsigned n = 0;
  for (; n < HCLEN; n++) {
    tree_cl.lengths[gCodeLenPerm[n]] = getbits(bp, in, 3);
  }
  for (; n < NUM_CODE_LENGTH_CODES; n++) {
    tree_cl.lengths[gCodeLenPerm[n]] = 0;
  }

  if (failed(rv = HuffmanTree_makeFromLengths2(&tree_cl))) {
    return rv;
  }

  const unsigned count = HLIT + HDIST;
  unsigned bitlen[NUM_DEFLATE_CODE_SYMBOLS + NUM_DISTANCE_SYMBOLS];

  for (unsigned i = 0; i < count;) {
    unsigned code = 0;
    if ((rv = huffmanDecodeSymbol(in, bp, inlength, &tree_cl, &code)) != rcSuccess) {
      return rv;
    }

    if (code <= 15) {
      bitlen[i++] = code;
    } else if (code == 16) {
      if (i == 0) {
        return rcInvalidFormat;
      }

      if (*bp >= inbitlength) {
        return rcBufferUnderrun;
      }

      unsigned replength = 3 + getbits(bp, in, 2);
      if (i + replength >= count) {
        return rcBufferOverflow;
      }

      const unsigned value = bitlen[i - 1];
      for (unsigned n = 0; n < replength; n++) {
        bitlen[i++] = value;
      }

    } else { // if (code == 17 || code == 18)

      if (*bp >= inbitlength) {
        return rcBufferUnderrun;
      }

      unsigned len = (code == 17) ? (3 + getbits(bp, in, 3)) : (11 + getbits(bp, in, 7));

      if (i + len >= count) {
        return rcBufferOverflow;
      }

      for (unsigned n = 0; n < len; n++) {
        bitlen[i++] = 0;
      }
    }
  }

  if (bitlen[256] == 0) {
    return rcInvalidFormat;
  }

  if (failed(rv = HuffmanTree_makeFromLengths(tree_ll, bitlen, HLIT, NUM_DEFLATE_CODE_SYMBOLS, 15))
    || failed(rv = HuffmanTree_makeFromLengths(tree_d, bitlen + HLIT, HDIST, NUM_DISTANCE_SYMBOLS, 15))) {
    return rv;
  }

  return rcSuccess;
}

// ----------------------------------------------------------------------------
static retcode inflateHuffmanBlock(unsigned char* out, size_t outsize,
  const unsigned char* in, size_t* bp, size_t* pos, size_t inlength, unsigned btype)
{
  retcode rv;

  HuffmanTree tree_ll;
  HuffmanTree tree_d;

  size_t inbitlength = inlength * 8;

  if (btype == 1) {
    generateFixedLitLenTree(&tree_ll);
    generateFixedDistanceTree(&tree_d);
  } else { // if (btype == 2)
    if ((rv = getTreeInflateDynamic(&tree_ll, &tree_d, in, bp, inlength)) != rcSuccess) {
      return rv;
    }
  }

  for (;;) {

    unsigned code_ll = 0;
    if ((rv = huffmanDecodeSymbol(in, bp, inlength, &tree_ll, &code_ll)) != rcSuccess) {
      return rv;
    }

    if (code_ll <= 255) {
      if ((*pos) >= outsize) {
        return rcBufferOverflow;
      }

      out[(*pos)] = (unsigned char)(code_ll);
      (*pos)++;
    } else if (code_ll >= FIRST_LENGTH_CODE_INDEX
      && code_ll <= LAST_LENGTH_CODE_INDEX) {
      size_t length = gCodeInfo[code_ll - FIRST_LENGTH_CODE_INDEX].base;

      unsigned numextrabits_l = gCodeInfo[code_ll - FIRST_LENGTH_CODE_INDEX].len;

      length += getbits(bp, in, numextrabits_l);

      unsigned code_d = 0;
      if ((rv = huffmanDecodeSymbol(in, bp, inlength, &tree_d, &code_d)) != rcSuccess) {
        return rv;
      }

      if (code_d > 29) {
        return rcInvalidFormat;
      }

      unsigned distance = gDistInfo[code_d].base;

      unsigned numextrabits_d = gDistInfo[code_d].len;
      if (*bp >= inbitlength) {
        return rcBufferUnderrun;
      }

      distance += getbits(bp, in, numextrabits_d);

      size_t start = (*pos);
      if (distance > start) {
        return rcInvalidFormat;
      }

      if ((*pos) + length > outsize) {
        return rcBufferOverflow;
      }

      if (distance == 1) {
        memset(out + *pos, out[start - 1], length);
        *pos += length;
      } else {
        size_t Loops = length / distance;
        size_t Remain = length % distance;

        for (size_t Loop = 0; Loop < Loops; ++Loop) {
          memcpy(out + *pos, out + start - distance, distance);
          *pos += distance;
        }

        memcpy(out + *pos, out + start - distance, Remain);
        *pos += Remain;
      }
    } else if (code_ll == 256) {
      return rcSuccess;
    } else {
      return rcInternal;
    }
  }
}

// ----------------------------------------------------------------------------
static retcode inflateNoCompression(unsigned char* out, size_t outsize,
  const unsigned char* in, size_t* bp, size_t* pos, size_t inlength)
{
  // align on byte boundary
  (*bp) = ((*bp) + 7) & (~7);

  const size_t p = (*bp) / 8; /*byte position*/

  if (p + 4 > inlength) {
    return rcBufferUnderrun; // error, bit pointer will jump past memory
  }

  const unsigned LEN = in[p] + (in[p + 1] << 8);
  const unsigned NLEN = in[p + 2] + (in[p + 3] << 8);

  if (LEN + NLEN != 65535) {
    return rcInvalidFormat; // error: NLEN is not one's complement of LEN
  }

  if ((*pos) + LEN > outsize) {
    return rcBufferOverflow;
  }

  if (p + 4 + LEN > inlength) {
    return rcBufferUnderrun; // error, bit pointer will jump past memory
  }

  // read the literal data: LEN bytes are now stored in the out buffer
  memcpy(&out[*pos], &in[p + 4], LEN);

  (*pos) += LEN;
  (*bp) += (4 + LEN) * 8;

  return rcSuccess;
}

// ----------------------------------------------------------------------------
size_t inflate(uint8_t* out, size_t outsize, const uint8_t* in, size_t insize)
{
  size_t bp = 0;
  unsigned BFINAL = 0;
  size_t pos = 0;
  unsigned error = 0;

  while (!BFINAL) {
    if (bp + 2 >= insize * 8) {
      return 0;
    }

    BFINAL = getbit(&bp, in);

    unsigned BTYPE = getbits(&bp, in, 2);

    if (BTYPE == 3) {
      return 0;
    } else if (BTYPE == 0) {
      error = inflateNoCompression(out, outsize, in, &bp, &pos, insize);
    } else {
      error = inflateHuffmanBlock(out, outsize, in, &bp, &pos, insize, BTYPE);
    }

    if (error) {
      return 0;
    }
  }

  return pos;
}

// ----------------------------------------------------------------------------
size_t zlib_decompress(uint8_t* dst, size_t cdst, const uint8_t* src, size_t csrc)
{
  if (src == nullptr || dst == nullptr || csrc < 6) {
    return 0;
  }

  unsigned method = src[0];
  unsigned flg = src[1];
  unsigned fcheck = ((method << 8) + flg) % 31;
  if ((method & 0x0f) != methodDeflated || (method >> 4) > 8 || fcheck != 0) {
    return 0;
  }

  if (flg & flagDictionary) {
    // Preset Dictionary not implemented
    return 0;
  }

  return inflate(dst, cdst, src + 2, csrc - 6);
}
