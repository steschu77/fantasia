#include "ccutil_xmlread.h"
#include "ccutil_retcode.h"

#include <vector>
#include <string>

#include <inttypes.h>

// ----------------------------------------------------------------------------
enum xmlstate_e
{
  Z0 = 0,
  Z1 = 1,
  Z2 = 2,
  Z3 = 3,
  Z4 = 4,
  Z5 = 5,
  Z6 = 6,
  Z7 = 7,
  Z8 = 8,
  Z9 = 9,
  Z10 = 10,
  Z11 = 11,
  Z12 = 12,
  Z13 = 13,
  Z14 = 14,
  Z15 = 15,
};

typedef enum xmlstate_e xmlstate_t;

// ----------------------------------------------------------------------------
union xmlres_u
{
  xmlres_u(xmlstate_t state);
  xmlres_u(retcode code);

  xmlstate_t state;
  retcode err;
};

// ----------------------------------------------------------------------------
xmlres_u::xmlres_u(xmlstate_t state)
{
  this->state = state;
}

// ----------------------------------------------------------------------------
xmlres_u::xmlres_u(retcode code)
{
  this->err = code;
}

typedef union xmlres_u xmlres_t;

// ----------------------------------------------------------------------------
inline int failed(const xmlres_t& res)
{
  return failed(res.err);
}

// ----------------------------------------------------------------------------
enum xmltoken_type_e {
  tCD = 1, // character data
  tBT, // begin tag    <id
  tET, // end tag      </id
  tCL, // close         >
  tEC, // empty close  />
  tID, // identifier   id
  tEQ, // equal        =
  tST, // string       "xx"
  tCO, // comment      <!-- xxx -->
  tDC, // declaration  <!ELEMENT
  tDV, // decl. value  value!>
  tPI, // processing instruction <?name
  tPV, // processing value       value?>
  tDA, // data
  tEr, // error
  tMax,
};

typedef enum xmltoken_type_e xmltoken_type_t;

// ----------------------------------------------------------------------------
struct XmlToken
{
  xmltoken_type_t type;
  std::string text;
};

// clang-format off
// ----------------------------------------------------------------------------
enum xmlchar_classes_e {
  ER = 0,  // error
  SP = 1,  // space
  CH = 2,  // character
  LE = 3,  // a-zA-Z letter
  N  = 4,  // 0-9 number (digit)
  EQ = 5,  // = equal sign
  MI = 6,  // - minus
  QU = 7,  // " quote
  SQ = 8,  // ' single quote
  BG = 9,  // < begin
  EN = 10, // > end
  SL = 11, // / slash
  EX = 12, // ! exclamation mark
  QM = 13, // ? question mark
  BO = 14, // [ Bracket Open
  BC = 15, // ] Bracket Close
  DT = 16, // . dot
  CLASSES = 17
};

typedef enum xmlchar_classes_e xmlchar_classes_t;

// ----------------------------------------------------------------------------
const uint8_t gCharClass[0x80] = {
  //00, 01, 02, 03, 04, 05, 06, 07, 08,TAB, LF, 0B, 0C, CR, 0E, 0F
    ER, ER, ER, ER, ER, ER, ER, ER, ER, SP, SP, ER, ER, SP, ER, ER,
  //10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 1A, 1B, 1C, 1D, 1E, 1F
    ER, ER, ER, ER, ER, ER, ER, ER, ER, ER, ER, ER, ER, ER, ER, ER,
  //  ,  !,  ",  #,  $,  %,  &,  ',  (,  ),  *,  +,  ,,  -,  .,  /
    SP, EX, QU, CH, CH, CH, CH, SQ, CH, CH, CH, CH, CH, MI, DT, SL,
  // 0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  :,  ;,  <,  =,  >,  ?
     N,  N,  N,  N,  N,  N,  N,  N,  N,  N, LE, CH, BG, EQ, EN, QM,
  // @,  A,  B,  C,  D,  E,  F,  G,  H,  I,  J,  K,  L,  M,  N,  O
    CH, LE, LE, LE, LE, LE, LE, LE, LE, LE, LE, LE, LE, LE, LE, LE,
  // P,  Q,  R,  S,  T,  U,  V,  W,  X,  Y,  Z,  [,  \,  ],  ^,  _
    LE, LE, LE, LE, LE, LE, LE, LE, LE, LE, LE, BO, CH, BC, CH, LE,
  // ',  a,  b,  c,  d,  e,  f,  g,  h,  i,  j,  k,  l,  m,  n,  o
    CH, LE, LE, LE, LE, LE, LE, LE, LE, LE, LE, LE, LE, LE, LE, LE,
  // p,  q,  r,  s,  t,  u,  v,  w,  x,  y,  z,  {,  |,  },  ~
    LE, LE, LE, LE, LE, LE, LE, LE, LE, LE, LE, CH, CH, CH, CH, CH
};
// clang-format on

// ----------------------------------------------------------------------------
inline uint8_t getCharClass(int chr)
{
  unsigned uchr = (unsigned)chr;
  return (uchr >= 0x80u) ? (uint8_t)LE : gCharClass[uchr];
}

// ----------------------------------------------------------------------------
inline int isempty(const std::string& str)
{
  for (const char& c : str)   {
    if (getCharClass(c) != SP) {
      return 0;
    }
  }

  return 1;
}

// ----------------------------------------------------------------------------
struct IXmlTokenCb
{
  virtual ~IXmlTokenCb() = default;
  virtual retcode onToken(const XmlToken&) = 0;
};

// ----------------------------------------------------------------------------
class XmlTokenizer
{
public:
  XmlTokenizer(IXmlTokenCb* cb);

  retcode tokenize(ccutil::IReader* read);

private:
  IXmlTokenCb* _cb;

  xmltoken_type_t _Token = tEr;
  std::string _aux;

  void y(char chr);
  void x(xmltoken_type_t token);
  retcode s();

  xmlres_t _Z0(int chr);
  xmlres_t _Z1(int chr);
  xmlres_t _Z2(int chr);
  xmlres_t _Z3(int chr);
  xmlres_t _Z4(int chr);
  xmlres_t _Z5(int chr);
  xmlres_t _Z6(int chr);
  xmlres_t _Z7(int chr);
  xmlres_t _Z8(int chr);
  xmlres_t _Z9(int chr);
  xmlres_t _Z10(int chr);
  xmlres_t _Z11(int chr);
  xmlres_t _Z12(int chr);
  xmlres_t _Z13(int chr);
  xmlres_t _Z14(int chr);
  xmlres_t _Z15(int chr);
};

// ----------------------------------------------------------------------------
typedef xmlres_t (XmlTokenizer::*xmlFnStateToken)(int);

// ----------------------------------------------------------------------------
XmlTokenizer::XmlTokenizer(IXmlTokenCb* cb)
: _cb(cb)
{
}

// ----------------------------------------------------------------------------
void XmlTokenizer::y(char chr)
{
  _aux.push_back(chr);
}

// ----------------------------------------------------------------------------
void XmlTokenizer::x(xmltoken_type_t token)
{
  _Token = token;
}

// ----------------------------------------------------------------------------
retcode XmlTokenizer::s()
{
  retcode res = rcSuccess;

  if (_Token != tCD || !isempty(_aux)) {
    XmlToken token;
    token.type = _Token;
    token.text.swap(_aux);
    res = _cb->onToken(token);
  }

  _aux.clear();
  return res;
}

// ----------------------------------------------------------------------------
xmlres_t XmlTokenizer::_Z0(int chr)
{
  switch (getCharClass(chr)) {
  case SP:
    return Z0;
  case LE:
    x(tID);
    y(chr);
    return Z5;
  case EQ: {
    x(tEQ);
    retcode rv = s();
    return success(rv) ? Z0 : rv;
  }
  case EN: {
    x(tCL);
    retcode rv = s();
    return success(rv) ? Z6 : rv;
  }
  case QU:
    x(tST);
    return Z7;
  case SQ:
    x(tST);
    return Z8;
  case SL:
    x(tEC);
    return Z4;
  case EX:
    x(tDC);
    return Z4;
  case QM:
    x(tPI);
    return Z4;
  default:
    return rcXmlParsing;
  }
}

// ----------------------------------------------------------------------------
xmlres_t XmlTokenizer::_Z1(int chr)
{
  // Precondition: '<'
  switch (getCharClass(chr)) {
  case LE:
    x(tBT);
    y(chr);
    return Z5;
  case SL:
    x(tET);
    return Z2;
  case EX:
    x(tDC);
    return Z3;
  case QM:
    x(tPI);
    return Z13;
  default:
    return rcXmlParsing;
  }
}

// ----------------------------------------------------------------------------
xmlres_t XmlTokenizer::_Z2(int chr)
{
  switch (getCharClass(chr)) {
  case LE:
    y(chr);
    return Z5;
  default:
    return rcXmlParsing;
  }
}

// ----------------------------------------------------------------------------
xmlres_t XmlTokenizer::_Z3(int chr)
{
  switch (getCharClass(chr)) {
  case LE:
    y(chr);
    return Z5;
  case MI:
    x(tCO);
    return Z9;
  case BO:
    x(tDA);
    return rcNotImplemented; // not impl. <[ -> Z16++
  default:
    return rcXmlParsing;
  }
}

// ----------------------------------------------------------------------------
xmlres_t XmlTokenizer::_Z4(int chr)
{
  switch (getCharClass(chr)) {
  case EN: {
    retcode rv = s();
    return success(rv) ? Z6 : rv;
  }
  default:
    return rcXmlParsing;
  }
}

// ----------------------------------------------------------------------------
xmlres_t XmlTokenizer::_Z5(int chr)
{
  // Precondition: LE
  switch (getCharClass(chr)) {
  case LE:
  case N:
  case MI:
  case DT:
    y(chr);
    return Z5;
  case SP: {
    retcode rv = s();
    return success(rv) ? Z0 : rv;
  }
  case EQ: {
    retcode rv = s();
    if (!success(rv))
      return rv;
    x(tEQ);
    return success(rv = s()) ? Z0 : rv;
  }
  case EN: {
    retcode rv = s();
    if (!success(rv))
      return rv;
    x(tCL);
    return success(rv = s()) ? Z6 : rv;
  }
  case SL: {
    retcode rv = s();
    if (!success(rv))
      return rv;
    x(tEC);
    return Z4;
  }
  default:
    return rcXmlParsing;
  }
}

// ----------------------------------------------------------------------------
xmlres_t XmlTokenizer::_Z6(int chr)
{
  switch (getCharClass(chr)) {
  case BG: {
    x(tCD);
    retcode rv = s();
    return success(rv) ? Z1 : rv;
  }
  default:
    y(chr);
    return Z6;
  }
}

// ----------------------------------------------------------------------------
xmlres_t XmlTokenizer::_Z7(int chr)
{
  switch (chr) {
  case '"': {
    retcode rv = s();
    return success(rv) ? Z0 : rv;
  }
  default:
    y(chr);
    return Z7;
  }
}

// ----------------------------------------------------------------------------
xmlres_t XmlTokenizer::_Z8(int chr)
{
  switch (chr) {
  case '\'': {
    retcode rv = s();
    return success(rv) ? Z0 : rv;
  }
  default:
    y(chr);
    return Z8;
  }
}

// ----------------------------------------------------------------------------
xmlres_t XmlTokenizer::_Z9(int chr)
{
  // Precondition: '<!-'
  switch (chr) {
  case '-':
    return Z10;
  default:
    return rcXmlParsing;
  }
}

// ----------------------------------------------------------------------------
xmlres_t XmlTokenizer::_Z10(int chr)
{
  // Precondition: '<!--'
  switch (chr) {
  case '-':
    return Z11;
  default:
    y(chr);
    return Z10;
  }
}

// ----------------------------------------------------------------------------
xmlres_t XmlTokenizer::_Z11(int chr)
{
  // Precondition: '<!--..-'
  switch (chr) {
  case '-':
    return Z12;
  default:
    y('-');
    y(chr);
    return Z10;
  }
}

// ----------------------------------------------------------------------------
xmlres_t XmlTokenizer::_Z12(int chr)
{
  // Precondition: '<!--..--'
  switch (chr) {
  case '-':
    return Z12;
  case '>': {
    retcode rv = s();
    return success(rv) ? Z6 : rv;
  }
  default:
    y('-');
    y('-');
    y(chr);
    return Z10;
  }
}

// ----------------------------------------------------------------------------
xmlres_t XmlTokenizer::_Z13(int chr)
{
  switch (getCharClass(chr)) {
  case LE:
  case N:
  case MI:
    y(chr);
    return Z13;
  case SP: {
    retcode rv = s();
    if (!success(rv))
      return rv;
    x(tPV);
    return Z14;
  }
  case QM: {
    retcode rv = s();
    if (!success(rv))
      return rv;
    x(tPV);
    return Z15;
  }
  default:
    return rcXmlParsing;
  }
}

// ----------------------------------------------------------------------------
xmlres_t XmlTokenizer::_Z14(int chr)
{
  switch (chr) {
  case '?':
    return Z15;
  default:
    y(chr);
    return Z14;
  }
}

// ----------------------------------------------------------------------------
xmlres_t XmlTokenizer::_Z15(int chr)
{
  switch (chr) {
  case '>': {
    retcode rv = s();
    return success(rv) ? Z6 : rv;
  }
  default:
    y('?');
    y(chr);
    return Z14;
  }
}

// ----------------------------------------------------------------------------
retcode XmlTokenizer::tokenize(ccutil::IReader* read)
{
  const static xmlFnStateToken fnState[] = {
    &XmlTokenizer::_Z0,
    &XmlTokenizer::_Z1,
    &XmlTokenizer::_Z2,
    &XmlTokenizer::_Z3,
    &XmlTokenizer::_Z4,
    &XmlTokenizer::_Z5,
    &XmlTokenizer::_Z6,
    &XmlTokenizer::_Z7,
    &XmlTokenizer::_Z8,
    &XmlTokenizer::_Z9,
    &XmlTokenizer::_Z10,
    &XmlTokenizer::_Z11,
    &XmlTokenizer::_Z12,
    &XmlTokenizer::_Z13,
    &XmlTokenizer::_Z14,
    &XmlTokenizer::_Z15
  };

  xmlres_t s { Z6 };

  int chr;
  while ((chr = read->readChar()) != '\0') {
    if (failed(s = (this->*fnState[s.state])(chr))) {
      return s.err;
    }
  }

  return s.state == Z6 ?
    static_cast<retcode>(rcSuccess) :
    static_cast<retcode>(rcXmlIncompleteFile);
}

// ----------------------------------------------------------------------------
struct XmlParser : public IXmlTokenCb
{
public:
  XmlParser(ccutil::IXmlEvents* events);

  retcode onToken(const XmlToken& token) override;

private:
  ccutil::IXmlEvents* _events;

  xmlres_t _state = Z0;
  std::string _attrName;
  std::string _elementName;

  xmlres_t _Z0(const XmlToken& token);
  xmlres_t _Z1(const XmlToken& token);
  xmlres_t _Z2(const XmlToken& token);
  xmlres_t _Z3(const XmlToken& token);
  xmlres_t _Z4(const XmlToken& token);
  xmlres_t _Z5(const XmlToken& token);
};

// ----------------------------------------------------------------------------
typedef xmlres_t (XmlParser::*xmlFnStateParse)(const XmlToken&);

// ----------------------------------------------------------------------------
XmlParser::XmlParser(ccutil::IXmlEvents* events)
: _events(events)
{}

// ----------------------------------------------------------------------------
xmlres_t XmlParser::_Z0(const XmlToken& token)
{
  // [39] STag content
  switch (token.type) {
  case tPI:
    // [01] prolog
    if (failed(_events->onProcessingInstruction(token.text))) {
      return rcFailed;
    }
    return Z1;

  case tBT:
    _elementName = token.text;
    if (failed(_events->onElementBegin(token.text))) {
      return rcFailed;
    }
    return Z2;

  case tCO:
    // ignore comments
    return Z0;

  case tCD:
    if (failed(_events->onCData(token.text))) {
      return rcFailed;
    }
    return Z0;

  case tET:
    if (failed(_events->onElementEnd(token.text))) {
      return rcFailed;
    }
    return Z3; // expect close tag;

  default:
    return rcXmlUnexpectedToken;
  }
}

// ----------------------------------------------------------------------------
xmlres_t XmlParser::_Z1(const XmlToken& token)
{
  // [16] PI  ::= '<?' PITarget (S (Char* - (Char* '?>' Char*)))? '?>'
  if (token.type != tPV) {
    return rcXmlPIValueExpected;
  }

  return Z0;
}

// ----------------------------------------------------------------------------
xmlres_t XmlParser::_Z2(const XmlToken& token)
{
  // [40]/[44] '<' Name (S Attribute)* S?
  // [41] Attribute ::= Name Eq AttValue
  if (token.type == tID) {
    _attrName = token.text;
    return Z4;
  }

  // [40]/[44] '<' Name (S Attribute)* S? ('>' | '/>')
  if (token.type == tCL) {
    return Z0;
  }

  if (token.type == tEC) {
    // [44] EmptyElemTag ::= '<' Name (S Attribute)* S? '/>'
    // ready with: [39] EmptyElemTag
    if (failed(_events->onElementEnd(_elementName))) {
      return rcFailed;
    }
    return Z0;
  }
  return rcXmlCloseTagExpected;
}

// ----------------------------------------------------------------------------
xmlres_t XmlParser::_Z3(const XmlToken& token)
{
  // [40] '<' Name (S Attribute)* S? '>'
  // [39] STag content ETag
  // [42] ETag ::= '</' Name S? '>'
  if (token.type != tCL) {
    return rcXmlCloseTagExpected;
  }

  return Z0;
}

// ----------------------------------------------------------------------------
xmlres_t XmlParser::_Z4(const XmlToken& token)
{
  // [41] Name Eq
  if (token.type != tEQ) {
    return rcXmlEqualSignExpected;
  }

  return Z5;
}

// ----------------------------------------------------------------------------
xmlres_t XmlParser::_Z5(const XmlToken& token)
{
  // [41] Name Eq AttValue
  if (token.type != tST) {
    return rcXmlStringExpected;
  }

  if (failed(_events->onAttribute(_attrName, token.text))) {
    return rcFailed;
  }
  return Z2;
}

// ----------------------------------------------------------------------------
retcode XmlParser::onToken(const XmlToken& token)
{
  const static xmlFnStateParse fnState[] = {
    &XmlParser::_Z0,
    &XmlParser::_Z1,
    &XmlParser::_Z2,
    &XmlParser::_Z3,
    &XmlParser::_Z4,
    &XmlParser::_Z5
  };

  if (failed(_state = (this->*(fnState[_state.state]))(token))) {
    return _state.err;
  }

  return rcSuccess;
}

// ----------------------------------------------------------------------------
retcode ccutil::parseXml(IReader* read, IXmlEvents* events)
{
  XmlParser parser{ events };
  XmlTokenizer tokenizer{ &parser };

  return tokenizer.tokenize(read);
}
