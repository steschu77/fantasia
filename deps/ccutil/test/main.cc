#include "ccunit.h"

// ----------------------------------------------------------------------------
ccunit::Tests test_byteswap();
ccunit::Tests test_filepath();
ccunit::Tests test_inflate();
ccunit::Tests test_xmlread();
ccunit::Tests test_zipfile();

  // ----------------------------------------------------------------------------
void emplace_back(ccunit::Tests& v2, ccunit::Tests&& v1)
{
  v2.insert(
    v2.end(),
    std::make_move_iterator(v1.begin()),
    std::make_move_iterator(v1.end()));
}

// ----------------------------------------------------------------------------
int main(int argc, char* argv[])
{
  ccunit::Tests combined;
  emplace_back(combined, test_byteswap());
  emplace_back(combined, test_inflate());
  emplace_back(combined, test_xmlread());
  emplace_back(combined, test_zipfile());
  emplace_back(combined, test_filepath());

  return ccunit::runTests(combined, ccunit::VerboseMode::printFailuresOnly);
}
