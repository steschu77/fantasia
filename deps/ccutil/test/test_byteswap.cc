#include "ccunit.h"
#include "ccutil_byteswap.h"

// ----------------------------------------------------------------------------
void test_ccutil_byteswap()
{
  EXPECT_EQ(byteswap16(0x0102), 0x0201);
  EXPECT_EQ(byteswap32(0x01020304), 0x04030201);
  EXPECT_EQ(byteswap64(0x0102030405060708), 0x0807060504030201);
}

// ----------------------------------------------------------------------------
ccunit::Tests test_byteswap()
{
  ccunit::Tests tests;

  tests.push_back(std::make_unique<ccunit::Test>("byteswap", test_ccutil_byteswap));

  return tests;
}
