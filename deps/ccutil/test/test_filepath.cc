#include "ccunit.h"
#include "ccutil_filepath.h"

// ----------------------------------------------------------------------------
void test_filepath_posix()
{
  ccutil::Path p = ccutil::createPath("/home/users/passwds.dat");
  EXPECT_EQ(p.path(), "/home/users/passwds.dat");
  EXPECT_EQ(p.directory(), "/home/users/");
  EXPECT_EQ(p.filename(), "passwds");
  EXPECT_EQ(p.extension(), ".dat");
}

// ----------------------------------------------------------------------------
void test_filepath_win32()
{
  ccutil::Path p = ccutil::createPath("c:\\Users\\passwds.dat");
  EXPECT_EQ(p.path(), "c:\\Users\\passwds.dat");
  EXPECT_EQ(p.directory(), "c:\\Users\\");
  EXPECT_EQ(p.filename(), "passwds");
  EXPECT_EQ(p.extension(), ".dat");
}

// ----------------------------------------------------------------------------
void test_pathsplit_posix()
{
  std::vector<std::string> dirs = ccutil::splitPath("/home/users/passwds.dat");
  ASSERT_EQ(dirs.size(), 3);
  EXPECT_EQ(dirs[0], "home");
  EXPECT_EQ(dirs[1], "users");
  EXPECT_EQ(dirs[2], "passwds.dat");
}

// ----------------------------------------------------------------------------
void test_pathsplit_win32()
{
  std::vector<std::string> dirs = ccutil::splitPath("c:\\Users\\passwds.dat");
  ASSERT_EQ(dirs.size(), 3);
  EXPECT_EQ(dirs[0], "c:");
  EXPECT_EQ(dirs[1], "Users");
  EXPECT_EQ(dirs[2], "passwds.dat");
}

// ----------------------------------------------------------------------------
void test_pathsplit_mixed()
{
  std::vector<std::string> dirs = ccutil::splitPath("c:\\users/passwds.dat");
  ASSERT_EQ(dirs.size(), 3);
  EXPECT_EQ(dirs[0], "c:");
  EXPECT_EQ(dirs[1], "users");
  EXPECT_EQ(dirs[2], "passwds.dat");
}

// ----------------------------------------------------------------------------
ccunit::Tests test_filepath()
{
  ccunit::Tests tests;

  tests.push_back(std::make_unique<ccunit::Test>("filepath(posix)", test_filepath_posix));
  tests.push_back(std::make_unique<ccunit::Test>("filepath(win32)", test_filepath_win32));
  tests.push_back(std::make_unique<ccunit::Test>("filepath(win32)", test_pathsplit_posix));
  tests.push_back(std::make_unique<ccunit::Test>("filepath(win32)", test_pathsplit_win32));
  tests.push_back(std::make_unique<ccunit::Test>("filepath(win32)", test_pathsplit_mixed));

  return tests;
}
