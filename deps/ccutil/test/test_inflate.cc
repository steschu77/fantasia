#include "ccunit.h"

#include "ccutil_inflate.h"
#include "test_zipfile.h"

#include <memory.h>

// ----------------------------------------------------------------------------
void test_inflate_functional()
{
  uint8_t out[sizeof(testzip_content) - 1];
  size_t len = inflate(out, sizeof(out), testzip_file, sizeof(testzip_file));
  ASSERT_EQ(len, sizeof(out));
  EXPECT_EQ(memcmp(out, testzip_content, len), 0);
}

// ----------------------------------------------------------------------------
void test_inflate_coverage()
{
  uint8_t out[320];
  const size_t cOut = sizeof(out);

  // invalid block size
  const uint8_t in0[] = { 0x06 };
  const size_t s0 = inflate(out, cOut, in0, sizeof(in0));
  EXPECT_EQ(s0, 0u);

  // stored mode
  const uint8_t in1[] = { 0x01, 0x01, 0x00, 0xfe, 0xff, 0x66 };
  const size_t s1 = inflate(out, cOut, in1, sizeof(in1));
  EXPECT_EQ(s1, 1u);
  EXPECT_EQ(out[0], 0x66);

  // fixed
  const uint8_t in2[] = { 0x03, 0x00 };
  const size_t s2 = inflate(out, cOut, in2, sizeof(in2));
  EXPECT_EQ(s2, 0u);

  // window wrap
  const uint8_t in3[] = { 0x63, 0x18, 0x05, 0x40, 0x0c, 0x00 };
  const size_t s3 = inflate(out, cOut, in3, sizeof(in3));
  EXPECT_EQ(s3, 262);

  // too many length or distance symbols
  const uint8_t in4[] = { 0xfc, 0x00, 0x00 };
  const size_t s4 = inflate(out, cOut, in4, sizeof(in4));
  EXPECT_EQ(s4, 0u);
}

// ----------------------------------------------------------------------------
ccunit::Tests test_inflate()
{
  ccunit::Tests tests;

  tests.push_back(std::make_unique<ccunit::Test>("inflate(functional)", test_inflate_functional));
  tests.push_back(std::make_unique<ccunit::Test>("inflate(coverage)", test_inflate_coverage));

  return tests;
}
