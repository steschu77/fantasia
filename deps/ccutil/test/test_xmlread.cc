/*
 * Test cases taken from:
 * http://cafeconleche.org/SAXTest/results/org.apache.crimson.parser.XMLReaderImpl/xmltest/valid/sa/
 */

#include "ccunit.h"

#include "ccutil_xmlread.h"

// ----------------------------------------------------------------------------
class XmlText : public ccutil::IReader
{
public:
  XmlText(const std::string& text);
  int readChar() override;

private:
  std::string _text;
  size_t _length = 0;
  size_t _pos = 0;
};

// ----------------------------------------------------------------------------
XmlText::XmlText(const std::string& text)
: _text(text)
, _length(text.length())
{
}

// ----------------------------------------------------------------------------
int XmlText::readChar()
{
  return _pos < _length ? _text[_pos++] : 0;
}

// ----------------------------------------------------------------------------
enum xml_eventid_e {
  idElementBegin,
  idElementEnd,
  idAttributeName,
  idAttributeValue,
  idCData,
  idProcessingInstruction
};

// ----------------------------------------------------------------------------
struct XmlNode
{
  int id;
  std::string name;
};

using XmlDocument = std::vector<XmlNode>;

// ----------------------------------------------------------------------------
class XmlContext : public ccutil::IXmlEvents
{
public:
  XmlContext(const XmlDocument& expected);
  size_t count() const;

  retcode onElementBegin(const std::string& name) override;
  retcode onElementEnd(const std::string&) override;
  retcode onAttribute(const std::string&, const std::string&) override;
  retcode onCData(const std::string&) override;
  retcode onProcessingInstruction(const std::string&) override;

private:
  size_t _idx = 0;
  XmlDocument _expected;

  retcode _check(const std::string& str, int id);
};

// ----------------------------------------------------------------------------
XmlContext::XmlContext(const XmlDocument& expected)
: _expected(expected)
{
}

// ----------------------------------------------------------------------------
size_t XmlContext::count() const
{
  return _idx;
}

// ----------------------------------------------------------------------------
retcode XmlContext::_check(const std::string& str, int id)
{
  const size_t idx = _idx++;
  return _expected[idx].id != id
    || str != _expected[idx].name;
}

// ----------------------------------------------------------------------------
retcode XmlContext::onElementBegin(const std::string& name)
{
  return _check(name, idElementBegin);
}

// ----------------------------------------------------------------------------
retcode XmlContext::onElementEnd(const std::string& name)
{
  return _check(name, idElementEnd);
}

// ----------------------------------------------------------------------------
retcode XmlContext::onAttribute(const std::string& name, const std::string& value)
{
  return _check(name, idAttributeName)
    || _check(value, idAttributeValue);
}

// ----------------------------------------------------------------------------
retcode XmlContext::onCData(const std::string& name)
{
  return _check(name, idCData);
}

// ----------------------------------------------------------------------------
retcode XmlContext::onProcessingInstruction(const std::string& name)
{
  return _check(name, idProcessingInstruction);
}

// ----------------------------------------------------------------------------
static void verifyXmlResult(const char* xml, const XmlDocument& doc, int exp_rv)
{
  XmlText text = { xml };
  XmlContext ctx{ doc };

  retcode rv = parseXml(&text, &ctx);
  EXPECT_EQ(rv, exp_rv);

  if (exp_rv == rcSuccess) {
    // don't verify count in error cases - that tampers with the expected return code
    EXPECT_EQ(ctx.count(), doc.size());
  }
}

// ----------------------------------------------------------------------------
static void verifyXml(const char* xml, const XmlDocument& doc)
{
  verifyXmlResult(xml, doc, rcSuccess);
}

// ----------------------------------------------------------------------------
void test_xmlReadDoc1()
{
  const XmlDocument doc = {
    { idElementBegin, "doc" },
    { idElementEnd, "doc" }
  };

  verifyXml("<doc/>", doc);
  verifyXml("<doc></doc>", doc);
  verifyXml("<doc ></doc>", doc);
  verifyXml("<doc></doc >", doc);
  verifyXml("<doc> </doc>", doc);
  verifyXml("<doc><!--a comment--></doc>", doc);
  verifyXml("<doc><!-- a comment ->--></doc>", doc);
  verifyXml("<doc\n></doc\n>", doc);
}

// ----------------------------------------------------------------------------
void test_xmlReadDoc2()
{
  const XmlDocument doc = {
    { idElementBegin, "doc" },
    { idAttributeName, "a1" },
    { idAttributeValue, "v1" },
    { idElementEnd, "doc" }
  };

  verifyXml("<doc a1=\"v1\"></doc>", doc);
  verifyXml("<doc a1 = \"v1\"></doc>", doc);
  verifyXml("<doc a1 = \"v1\" ></doc>", doc);
  verifyXml("<doc a1='v1'></doc>", doc);
}

// ----------------------------------------------------------------------------
void test_xmlReadDoc3()
{
  const XmlDocument doc = {
    { idElementBegin, "doc" },
    { idAttributeName, "a1" },
    { idAttributeValue, "v1" },
    { idAttributeName, "a2" },
    { idAttributeValue, "v2" },
    { idElementEnd, "doc" }
  };

  verifyXml("<doc a1=\"v1\" a2=\"v2\"></doc>", doc);
}

// ----------------------------------------------------------------------------
void test_xmlReadDoc4()
{
  const XmlDocument doc = {
    { idElementBegin, "doc" },
    { idAttributeName, "_.-0123456789" },
    { idAttributeValue, "v1" },
    { idElementEnd, "doc" }
  };

  verifyXml("<doc _.-0123456789=\"v1\"></doc>", doc);
}

// ----------------------------------------------------------------------------
void test_xmlReadDoc5()
{
  const XmlDocument doc = {
    { idElementBegin, "doc" },
    { idCData, "&amp;&lt;&gt;&quot;&apos;&#163;" },
    { idElementEnd, "doc" }
  };

  verifyXml("<doc>&amp;&lt;&gt;&quot;&apos;&#163;</doc>", doc);
}

// ----------------------------------------------------------------------------
void test_xmlReadDoc6()
{
  const XmlDocument doc = {
    { idElementBegin, "doc" },
    { idProcessingInstruction, "pi" },
    { idElementEnd, "doc" }
  };

  verifyXml("<doc><?pi?></doc>", doc);
  verifyXml("<doc><?pi ?></doc>", doc);
  verifyXml("<doc><?pi\n?></doc>", doc);
  //verifyXml("<doc><?pi ? ></doc>", doc);
}

// ----------------------------------------------------------------------------
void test_xmlReadDoc7()
{
  const XmlDocument doc = {
    { idElementBegin, "doc" },
    { idProcessingInstruction, "pi" },
    { idProcessingInstruction, "" },
    { idElementEnd, "doc" }
  };

  verifyXml("<doc><?pi data?> <??></doc>", doc);
  verifyXml("<doc><?pi data ?> <??></doc>", doc);
  verifyXml("<doc><?pi  data ?> <??></doc>", doc);
}

// ----------------------------------------------------------------------------
void test_xmlReadDoc8()
{
  const XmlDocument doc = {
    { idElementBegin, "doc" },
    { idElementBegin, "foo" },
    { idElementEnd, "foo" },
    { idElementBegin, "foo" },
    { idElementEnd, "foo" },
    { idElementEnd, "doc" }
  };

  verifyXml("<doc><foo/><foo></foo></doc>", doc);
}

// ----------------------------------------------------------------------------
void test_xmlReadDoc9()
{
  const XmlDocument doc = {
    { idElementBegin, "doc" },
    { idCData, u8"\U00010000\U0010fffd" },
    { idElementEnd, "doc" }
  };

  verifyXml(u8"<doc>\U00010000\U0010fffd</doc>", doc);
  //verifyXml(u8"<doc>&#x10000;&#x10FFFD;</doc>", doc);
}

// ----------------------------------------------------------------------------
void test_xmlReadDoc10()
{
  const XmlDocument doc = {
    { idElementBegin, u8"\u0E40\u0E08\u0E21\u0E2A\u0E4C" },
    { idElementEnd, u8"\u0E40\u0E08\u0E21\u0E2A\u0E4C" }
  };

  verifyXml(u8"<\u0E40\u0E08\u0E21\u0E2A\u0E4C></\u0E40\u0E08\u0E21\u0E2A\u0E4C>", doc);
}

// ----------------------------------------------------------------------------
void test_xmlReadErr1()
{
  const XmlDocument doc = {
    { idElementBegin, "doc" },
    { idElementBegin, ".doc" },
    { idElementEnd, ".doc" },
    { idElementEnd, "doc" }
  };

  // Names may not start with "."; it's not a Letter.
  verifyXmlResult("<doc><.doc></.doc></doc>", doc, rcXmlParsing);

  // Processing Instruction target name is required.
  //verifyXmlResult("<doc><? ?></doc>", doc, rcXmlParsing); // check not impl.

  // SGML-ism: processing instructions end in '?>' not '>'.
  verifyXmlResult("<doc><?target data></doc>", doc, rcXmlIncompleteFile);

  // Processing instructions end in '?>' not '?'.
  verifyXmlResult("<doc><?target some data?</doc>", doc, rcXmlIncompleteFile);

  // SGML-ism: attribute values must be explicitly assigned a value.
  verifyXmlResult("<doc a1></doc>", doc, rcXmlEqualSignExpected);

  // SGML-ism: attribute values must be quoted in all cases.
  verifyXmlResult("<doc a1=v1></doc>", doc, rcXmlStringExpected);

  // The quotes on both ends of an attribute value must match.
  verifyXmlResult("<doc a1=\"v1'></doc>", doc, rcXmlIncompleteFile);

  // Attribute values need a value, not just an equals sign.
  verifyXmlResult("<doc a1=></doc>", doc, rcXmlStringExpected);

  // Attribute values need an associated name.
  verifyXmlResult("<doc a1=\"v1\" \"v2\"></doc>", doc, rcXmlCloseTagExpected);

  // Digits are not valid name start characters.
  verifyXmlResult("<doc 12=\"34\"></doc>", doc, rcXmlParsing);

  // End tags may not be abbreviated as '</>'.
  verifyXmlResult("<doc></>", doc, rcXmlParsing);
}

// ----------------------------------------------------------------------------
ccunit::Tests test_xmlread()
{
  ccunit::Tests tests;

  tests.push_back(std::make_unique<ccunit::Test>("xmlread(<doc/>)", test_xmlReadDoc1));
  tests.push_back(std::make_unique<ccunit::Test>("xmlread(<doc a1=''/>)", test_xmlReadDoc2));
  tests.push_back(std::make_unique<ccunit::Test>("xmlread(<doc a1='' a2=''/>)", test_xmlReadDoc3));
  tests.push_back(std::make_unique<ccunit::Test>("xmlread(<doc _=''/>)", test_xmlReadDoc4));
  tests.push_back(std::make_unique<ccunit::Test>("xmlread(cdata)", test_xmlReadDoc5));
  tests.push_back(std::make_unique<ccunit::Test>("xmlread(processing-instruction)", test_xmlReadDoc6));
  tests.push_back(std::make_unique<ccunit::Test>("xmlread(empty processing-instruction)", test_xmlReadDoc7));
  tests.push_back(std::make_unique<ccunit::Test>("xmlread(child element)", test_xmlReadDoc8));
  tests.push_back(std::make_unique<ccunit::Test>("xmlread(unicode cdata)", test_xmlReadDoc9));
  tests.push_back(std::make_unique<ccunit::Test>("xmlread(unicode name)", test_xmlReadDoc10));
  tests.push_back(std::make_unique<ccunit::Test>("xmlread(errors)", test_xmlReadErr1));

  return tests;
}
