#include "ccunit.h"

#include "ccutil_zipfile.h"
#include "test_zipfile.h"

#include <memory.h>

// ----------------------------------------------------------------------------
class ZipContext
{
public:
  ZipContext();

  void read(void* pRead, size_t cRead);

private:
  const uint8_t* pData;
  const size_t cData;
  size_t cPtr;
};

// ----------------------------------------------------------------------------
ZipContext::ZipContext()
: pData(testzip_raw)
, cData(sizeof(testzip_raw))
, cPtr(0)
{
}

// ----------------------------------------------------------------------------
void ZipContext::read(void* pRead, size_t cRead)
{
  if (cRead + cPtr > cData) {
    return;
  }

  memcpy(pRead, pData + cPtr, cRead);
  cPtr += cRead;
}

// ----------------------------------------------------------------------------
void test_zipread()
{
  constexpr size_t sizeCompressed = sizeof(testzip_file);
  constexpr size_t sizeUncompressed = sizeof(testzip_content) - 1;
  ZipContext file;

  ZipLocalFileHeader zipLFH;
  file.read(&zipLFH, sizeof(ZipLocalFileHeader));

  EXPECT_EQ(zipLFH.fileNameLength, 8);
  EXPECT_EQ(zipLFH.extraFieldLength, 0);
  EXPECT_EQ(zipLFH.sizeCompressed, sizeCompressed);
  EXPECT_EQ(zipLFH.sizeUncompressed, sizeUncompressed);

  char zipName[9] = { 0 };
  file.read(zipName, zipLFH.fileNameLength);
  EXPECT_EQ(strcmp(zipName, "test.txt"), 0);

  uint8_t compressed[sizeCompressed];
  file.read(compressed, sizeCompressed);
  EXPECT_EQ(memcmp(compressed, testzip_file, sizeCompressed), 0);
}

// ----------------------------------------------------------------------------
ccunit::Tests test_zipfile()
{
  ccunit::Tests tests;

  tests.push_back(std::make_unique<ccunit::Test>("zipfile(test.zip)", test_zipread));

  return tests;
}
