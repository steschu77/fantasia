#pragma once

#include <stddef.h>

namespace gfx {

// clang-format off
// ----------------------------------------------------------------------------
enum colorformat_t
{
  cfNotSet = 0u,

  // -- Packed Formats --------------------------------------------------------
  cfPacked = 0x100,

  cfY1 = cfPacked, //!< 1 bit Grayscale
  cfY2, //!< 2 bit Grayscale
  cfY4, //!< 4 bit Grayscale
  cfY8, //!< 8 bit Grayscale
  cfYg, //!< Greyscale 16 bit

  cfPAL1, //!< 1 bit paletted
  cfPAL2, //!< 2 bit paletted
  cfPAL4, //!< 4 bit paletted
  cfPAL8, //!< 8 bit paletted

  cfRGB4444, //!< 16 bit RGB with 4 bit for alpha, red, green and blue
  cfRGB0555, //!< 16 bit RGB with 5 bit for red, green and blue
  cfRGB0565, //!< 16 bit RGB with 5 bit for red and blue and 6 bit for green
  cfRGB1555, //!< 16 bit RGB with 5 bit for red, green and blue and 1 bit for alpha
  cfRGB0888, //!< 24 bit RGB with 8 bit for red, green and blue
  cfRGB8888, //!< 32 bit RGBA with 8 bit for alpha, red, green and blue
  cfBGR0888, //!< 24 bit RGB reversed with 8 bit for red, green and blue
  cfBGR8888, //!< 32 bit RGBA reversed with 8 bit for alpha, red, green and blue
  cfRGB0ggg, //!< 48 bit RGB with 16 bit for red, green and blue
  cfRGBgggg, //!< 64 bit RGBA with 16 bit for red, green and blue

  cfYUY2,
  cfUYVY
};
// clang-format on

unsigned cf_isindexed(colorformat_t cf);
size_t cf_bpp(colorformat_t cf);

using stride_t = int;
stride_t cf_stride(colorformat_t cf, unsigned cx, unsigned alignment);

} // namespace gfx

// ----------------------------------------------------------------------------
inline unsigned gfx::cf_isindexed(colorformat_t cf)
{
  return (unsigned)cf >= cfPAL1 && (unsigned)cf <= cfPAL8;
}
