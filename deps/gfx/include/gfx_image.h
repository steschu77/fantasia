#pragma once

#include <stdint.h>
#include <vector>

#include "gfx_colorformat.h"

namespace gfx {

// ----------------------------------------------------------------------------
struct geo_t
{
  colorformat_t cf;
  unsigned cx;
  unsigned cy;
};

// ----------------------------------------------------------------------------
struct image_t
{
  stride_t stride;
  std::vector<uint8_t> data;
  std::vector<uint32_t> palette;
};

// ----------------------------------------------------------------------------
struct picture_t
{
  geo_t geo;
  image_t img;
};

// ----------------------------------------------------------------------------
size_t make_buffersize(colorformat_t cf, const stride_t& stride, unsigned cy);

image_t pal1_to_rgb32(const image_t& pal1, const geo_t& geo);
image_t pal2_to_rgb32(const image_t& pal2, const geo_t& geo);
image_t pal4_to_rgb32(const image_t& pal4, const geo_t& geo);
image_t pal8_to_rgb32(const image_t& pal8, const geo_t& geo);

} // namespace gfx
