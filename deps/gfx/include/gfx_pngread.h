#pragma once

#include "gfx_image.h"
#include "ccutil_retcode.h"

namespace gfx {

// ----------------------------------------------------------------------------
picture_t png_read(const uint8_t* ppng, size_t cpng);

}
