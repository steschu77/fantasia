#include "gfx_colorformat.h"
#include <assert.h>

// ----------------------------------------------------------------------------
size_t gfx::cf_bpp(colorformat_t cf)
{
  switch (cf) {
  case cfPAL1:
  case cfY1:
    return 1u;

  case cfY2:
  case cfPAL2:
    return 2u;

  case cfY4:
  case cfPAL4:
    return 4u;

  case cfY8:
  case cfPAL8:
    return 8u;


  case cfRGB4444:
  case cfRGB0555:
  case cfRGB0565:
  case cfRGB1555:
  case cfYUY2:
  case cfUYVY:
    return 16u;

  case cfBGR0888:
  case cfRGB0888:
    return 24u;

  case cfBGR8888:
  case cfRGB8888:
    return 32u;

  case cfRGB0ggg:
    return 48u;

  case cfRGBgggg:
    return 64u;

  default:
    return 0u;
  }
}

// ----------------------------------------------------------------------------
gfx::stride_t gfx::cf_stride(colorformat_t cf, unsigned cx, unsigned alignment)
{
  assert((alignment & (alignment - 1)) == 0); // Make sure alignment is a power of 2
  const unsigned mask = ~(alignment - 1);
  return ((cx * cf_bpp(cf) + 7) / 8 + alignment - 1) & mask;
}
