#include "gfx_image.h"

// ----------------------------------------------------------------------------
size_t gfx::make_buffersize(colorformat_t cf, const stride_t& stride, unsigned cy)
{
  return stride * cy;
}

// ----------------------------------------------------------------------------
gfx::image_t gfx::pal1_to_rgb32(const image_t& pal1, const geo_t& geo)
{
  const stride_t stride = geo.cx * 4;
  
  gfx::image_t rgb32 { stride };
  rgb32.data.resize(stride * geo.cy);

  const uint8_t* src = pal1.data.data();
  uint8_t* dst = rgb32.data.data();

  for (unsigned y = 0; y < geo.cy; y++) {
    for (unsigned x = 0; x < geo.cx; x++) {
      const unsigned idx = (src[x / 8] >> (7 - (x & 7))) & 1;
      ((uint32_t*)dst)[x] = pal1.palette[idx];
    }

    src += pal1.stride;
    dst += rgb32.stride;
  }

  return rgb32;
}

// ----------------------------------------------------------------------------
gfx::image_t gfx::pal2_to_rgb32(const image_t& pal2, const geo_t& geo)
{
  const stride_t stride = geo.cx * 4;

  gfx::image_t rgb32 { stride };
  rgb32.data.resize(stride * geo.cy);

  const uint8_t* src = pal2.data.data();
  uint8_t* dst = rgb32.data.data();

  for (unsigned y = 0; y < geo.cy; y++) {
    for (unsigned x = 0; x < geo.cx; x++) {
      const unsigned idx = (src[x / 4] >> (6 - 2 * (x & 3))) & 3;
      ((uint32_t*)dst)[x] = pal2.palette[idx];
    }

    src += pal2.stride;
    dst += rgb32.stride;
  }

  return rgb32;
}

// ----------------------------------------------------------------------------
gfx::image_t gfx::pal4_to_rgb32(const image_t& pal4, const geo_t& geo)
{
  const stride_t stride = geo.cx * 4;

  gfx::image_t rgb32 { stride };
  rgb32.data.resize(stride * geo.cy);

  const uint8_t* src = pal4.data.data();
  uint8_t* dst = rgb32.data.data();

  for (unsigned y = 0; y < geo.cy; y++) {
    for (unsigned x = 0; x < geo.cx; x++) {
      const unsigned idx = (src[x / 2] >> (4 - 4 * (x & 1))) & 15;
      ((uint32_t*)dst)[x] = pal4.palette[idx];
    }

    src += pal4.stride;
    dst += rgb32.stride;
  }

  return rgb32;
}

// ----------------------------------------------------------------------------
gfx::image_t gfx::pal8_to_rgb32(const image_t& pal8, const geo_t& geo)
{
  const stride_t stride = geo.cx * 4;

  gfx::image_t rgb32 { stride };
  rgb32.data.resize(stride * geo.cy);

  const uint8_t* src = pal8.data.data();
  uint8_t* dst = rgb32.data.data();

  for (unsigned y = 0; y < geo.cy; y++) {
    for (unsigned x = 0; x < geo.cx; x++) {
      ((uint32_t*)dst)[x] = pal8.palette[src[x]];
    }

    src += pal8.stride;
    dst += rgb32.stride;
  }

  return rgb32;
}
