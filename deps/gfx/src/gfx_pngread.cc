#include "gfx_pngread.h"

#include "ccutil_byteswap.h"
#include "ccutil_inflate.h"

#include <malloc.h>
#include <math.h>
#include <memory.h>

// ----------------------------------------------------------------------------
#ifndef fourcc
#define fourcc(a, b, c, d) ((a) | ((b) << 8) | ((c) << 16) | ((d) << 24))
#endif

// ----------------------------------------------------------------------------
enum pngfcc {
  chkNull = 0,
  chkIHDR = fourcc('I', 'H', 'D', 'R'),
  chkIDAT = fourcc('I', 'D', 'A', 'T'),
  chkIEND = fourcc('I', 'E', 'N', 'D'),
  chkPLTE = fourcc('P', 'L', 'T', 'E'),
};

#undef fourcc

#pragma pack(push)
#pragma pack(1)

// ----------------------------------------------------------------------------
struct ChunkHead {
  uint32_t length;
  uint32_t type;
};

// ----------------------------------------------------------------------------
struct ChunkIHDR {
  uint32_t Width;
  uint32_t Height;
  uint8_t BitDepth;
  uint8_t Color;
  uint8_t Compression;
  uint8_t Filter;
  uint8_t Interlace;
};

#pragma pack(pop)

// ----------------------------------------------------------------------------
enum ColorType {
  colGreyscale = 0,
  colTrueColor = 2,
  colIndexedColor = 3,
  colGreyscaleAplha = 4,
  colTrueColorAlpha = 6,
};

// ----------------------------------------------------------------------------
enum FilterMethod {
  filMethod0 = 0,
};

// ----------------------------------------------------------------------------
enum FilterType {
  filNone = 0,
  filSub = 1,
  filUp = 2,
  filAverage = 3,
  filPaeth = 4,
};

// ----------------------------------------------------------------------------
static gfx::colorformat_t mapColorFormat(const ChunkIHDR& ihdr);
static void decodeIDAT(const std::vector<uint8_t> pidat, const gfx::geo_t& geo, gfx::image_t& image);

// ----------------------------------------------------------------------------
gfx::picture_t gfx::png_read(const uint8_t* ppng, size_t cpng)
{
  const uint8_t png_signature[8] = { 137, 80, 78, 71, 13, 10, 26, 10 };
  if (cpng < 8 || memcmp(ppng, png_signature, 8) != 0) {
    return {};
  }

  ppng += 8;
  cpng -= 8;

  if (cpng < sizeof(struct ChunkHead)) {
    return {};
  }

  const struct ChunkHead* head = (const struct ChunkHead*)ppng;
  ppng += sizeof(struct ChunkHead);
  cpng -= sizeof(struct ChunkHead);

  const uint32_t ihdrLen = byteswap32(head->length);

  if (cpng < sizeof(struct ChunkIHDR)
    || head->type != chkIHDR
    || ihdrLen < sizeof(struct ChunkIHDR)) {
    return {};
  }

  const struct ChunkIHDR* ihdr = (const struct ChunkIHDR*)ppng;
  ppng += ihdrLen + 4;
  cpng -= ihdrLen + 4;

  if (ihdr->Compression != 0
    || ihdr->Filter != 0
    || ihdr->Interlace > 1) {
    return {};
  }

  gfx::geo_t geo;
  geo.cx = byteswap32(ihdr->Width);
  geo.cy = byteswap32(ihdr->Height);
  geo.cf = mapColorFormat(*ihdr);

  const stride_t stride = cf_stride(geo.cf, geo.cx, 1);
  const size_t bufsize = make_buffersize(geo.cf, stride, geo.cy);

  image_t img { stride };
  img.data.resize(bufsize);

  if (cf_isindexed(geo.cf)) {
    img.palette.resize(256);
  }

  std::vector<uint8_t> idat;
  idat.resize(cpng);
  uint8_t* pidat = idat.data();

  while (cpng > 0) {

    if (cpng < sizeof(struct ChunkHead)) {
      return {};
    }

    const struct ChunkHead* head = (const struct ChunkHead*)ppng;
    ppng += sizeof(struct ChunkHead);
    cpng -= sizeof(struct ChunkHead);

    size_t length = byteswap32(head->length);

    switch (head->type) {
    case chkPLTE:
      if (length < 256 * 3) {
        const uint8_t* p = ppng;
        for (unsigned i = 0; i < length / 3; ++i, p += 3) {
          img.palette[i] = (p[2] << 16) | (p[1] << 8) | p[0];
        }
      }
      break;

    case chkIDAT:
      if (idat.size() - (pidat - idat.data()) < length) {
        return {};
      }

      memcpy(pidat, ppng, length);
      pidat += length;
      break;

    case chkIEND:
      idat.resize(pidat - idat.data());
      decodeIDAT(idat, geo, img);
      break;
    }

    ppng += length + 4;
    cpng -= length + 4;
  }

  return { geo, img };
}

// ----------------------------------------------------------------------------
static gfx::colorformat_t mapColorFormat(const ChunkIHDR& ihdr)
{
  switch (ihdr.Color) {
  case colIndexedColor:
    switch (ihdr.BitDepth) {
    case 1:
      return gfx::cfPAL1;
    case 2:
      return gfx::cfPAL2;
    case 4:
      return gfx::cfPAL4;
    case 8:
      return gfx::cfPAL8;
    default:
      return gfx::cfNotSet;
    }
  case colTrueColor:
    switch (ihdr.BitDepth) {
    case 8:
      return gfx::cfRGB0888;
    case 16:
      return gfx::cfRGB0ggg;
    default:
      return gfx::cfNotSet;
    }
  case colTrueColorAlpha:
    switch (ihdr.BitDepth) {
    case 4:
      return gfx::cfRGB4444;
    case 8:
      return gfx::cfRGB8888;
    case 16:
      return gfx::cfRGBgggg;
    default:
      return gfx::cfNotSet;
    }
  case colGreyscale:
    switch (ihdr.BitDepth) {
    case 1:
      return gfx::cfY1;
    case 2:
      return gfx::cfY2;
    case 4:
      return gfx::cfY4;
    case 8:
      return gfx::cfY8;
    case 16:
      return gfx::cfYg;
    default:
      return gfx::cfNotSet;
    }
  default:
    return gfx::cfNotSet;
  }
}

// ----------------------------------------------------------------------------
static uint8_t paeth(int a, int b, int c)
{
  int pa = abs(b - c);
  int pb = abs(a - c);
  int pc = abs(a + b - c - c);

  if (pc < pa && pc < pb)
    return (uint8_t)c;
  else if (pb < pa)
    return (uint8_t)b;
  else
    return (uint8_t)a;
}

// ----------------------------------------------------------------------------
static void unfilterScanline0_byte1(uint8_t* recon, uint8_t filterType, size_t cx)
{
  switch (filterType) {
  case filNone:
  case filUp:
    return;

  case filSub:
  case filPaeth:
    // paeth(recon[i-1], 0, 0) is always recon[i-1]
    for (size_t i = 1; i < cx; i++) {
      recon[i] = recon[i] + recon[i - 1];
    }
    return;

  case filAverage:
    for (size_t i = 1; i < cx; i++) {
      recon[i] = recon[i] + recon[i - 1] / 2;
    }
    return;
  }
}

// ----------------------------------------------------------------------------
static void unfilterScanlineN_byte1(uint8_t* recon, const uint8_t* precon, enum FilterType filterType, size_t cx)
{
  switch (filterType) {
  case filNone:
    return;

  case filSub:
    for (size_t i = 1; i < cx; i++) {
      recon[i] = recon[i] + recon[i - 1];
    }
    return;

  case filUp:
    for (size_t i = 0; i < cx; i++) {
      recon[i] = recon[i] + precon[i];
    }
    return;

  case filAverage:
    recon[0] = recon[0] + precon[0] / 2;
    for (size_t i = 1; i < cx; i++) {
      recon[i] = recon[i] + ((recon[i - 1] + precon[i]) / 2);
    }
    return;

  case filPaeth:
    // paeth(0, precon[i], 0) is always precon[i]
    recon[0] = (recon[0] + precon[0]);

    for (size_t i = 1; i < cx; i++) {
      recon[i] = (recon[i] + paeth(recon[i - 1], precon[i], precon[i - 1]));
    }
    return;
  }
}

// ----------------------------------------------------------------------------
static void unfilterScanline0_byte3(uint8_t* recon, enum FilterType filterType, size_t cx)
{
  switch (filterType) {
  case filNone:
  case filUp:
    return;

  case filSub:
  case filPaeth:
    // paeth(recon[i-1], 0, 0) is always recon[i-1]
    for (size_t i = 3; i < 3 * cx; i += 3) {
      recon[i + 0] = recon[i + 0] + recon[i - 3 + 0];
      recon[i + 1] = recon[i + 1] + recon[i - 3 + 1];
      recon[i + 2] = recon[i + 2] + recon[i - 3 + 2];
    }
    return;

  case filAverage:
    for (size_t i = 3; i < 3 * cx; i += 3) {
      recon[i + 0] = recon[i + 0] + recon[i - 3 + 0] / 2;
      recon[i + 1] = recon[i + 1] + recon[i - 3 + 1] / 2;
      recon[i + 2] = recon[i + 2] + recon[i - 3 + 2] / 2;
    }
    return;
  }
}

// ----------------------------------------------------------------------------
static void unfilterScanlineN_byte3(uint8_t* recon, const uint8_t* precon, enum FilterType filterType, size_t cx)
{
  switch (filterType) {
  case filNone:
    return;

  case filSub:
    for (size_t i = 3; i < 3 * cx; i += 3) {
      recon[i + 0] = recon[i + 0] + recon[i - 3 + 0];
      recon[i + 1] = recon[i + 1] + recon[i - 3 + 1];
      recon[i + 2] = recon[i + 2] + recon[i - 3 + 2];
    }
    return;

  case filUp:
    for (size_t i = 0; i < 3 * cx; i += 3) {
      recon[i + 0] = recon[i + 0] + precon[i + 0];
      recon[i + 1] = recon[i + 1] + precon[i + 1];
      recon[i + 2] = recon[i + 2] + precon[i + 2];
    }
    return;

  case filAverage:
    recon[0] = recon[0] + precon[0] / 2;
    recon[1] = recon[1] + precon[1] / 2;
    recon[2] = recon[2] + precon[2] / 2;

    for (size_t i = 3; i < 3 * cx; i += 3) {
      recon[i + 0] = recon[i + 0] + ((recon[i - 3 + 0] + precon[i + 0]) / 2);
      recon[i + 1] = recon[i + 1] + ((recon[i - 3 + 1] + precon[i + 1]) / 2);
      recon[i + 2] = recon[i + 2] + ((recon[i - 3 + 2] + precon[i + 2]) / 2);
    }
    return;

  case filPaeth:
    // paeth(0, precon[i], 0) is always precon[i]
    recon[0] = (recon[0] + precon[0]);
    recon[1] = (recon[1] + precon[1]);
    recon[2] = (recon[2] + precon[2]);

    for (size_t i = 3; i < 3 * cx; i += 3) {
      recon[i + 0] = (recon[i + 0] + paeth(recon[i - 3 + 0], precon[i + 0], precon[i - 3 + 0]));
      recon[i + 1] = (recon[i + 1] + paeth(recon[i - 3 + 1], precon[i + 1], precon[i - 3 + 1]));
      recon[i + 2] = (recon[i + 2] + paeth(recon[i - 3 + 2], precon[i + 2], precon[i - 3 + 2]));
    }
    return;
  }
}

// ----------------------------------------------------------------------------
static void unfilterScanline0_byte4(uint8_t* recon, enum FilterType filterType, size_t cx)
{
  switch (filterType) {
  case filNone:
  case filUp:
    return;

  case filSub:
  case filPaeth:
    // paeth(recon[i-1], 0, 0) is always recon[i-1]
    for (size_t i = 4; i < 4 * cx; i += 4) {
      recon[i + 0] = recon[i + 0] + recon[i - 4 + 0];
      recon[i + 1] = recon[i + 1] + recon[i - 4 + 1];
      recon[i + 2] = recon[i + 2] + recon[i - 4 + 2];
      recon[i + 3] = recon[i + 3] + recon[i - 4 + 3];
    }
    return;

  case filAverage:
    for (size_t i = 4; i < 4 * cx; i += 4) {
      recon[i + 0] = recon[i + 0] + recon[i - 4 + 0] / 2;
      recon[i + 1] = recon[i + 1] + recon[i - 4 + 1] / 2;
      recon[i + 2] = recon[i + 2] + recon[i - 4 + 2] / 2;
      recon[i + 3] = recon[i + 3] + recon[i - 4 + 3] / 2;
    }
    return;
  }
}

// ----------------------------------------------------------------------------
static void unfilterScanlineN_byte4(uint8_t* recon, const uint8_t* precon, enum FilterType filterType, size_t cx)
{
  switch (filterType) {
  case filNone:
    return;

  case filSub:
    for (size_t i = 4; i < 4 * cx; i += 4) {
      recon[i + 0] = recon[i + 0] + recon[i - 4 + 0];
      recon[i + 1] = recon[i + 1] + recon[i - 4 + 1];
      recon[i + 2] = recon[i + 2] + recon[i - 4 + 2];
      recon[i + 3] = recon[i + 3] + recon[i - 4 + 3];
    }
    return;

  case filUp:
    for (size_t i = 0; i < 4 * cx; i += 4) {
      recon[i + 0] = recon[i + 0] + precon[i + 0];
      recon[i + 1] = recon[i + 1] + precon[i + 1];
      recon[i + 2] = recon[i + 2] + precon[i + 2];
      recon[i + 3] = recon[i + 3] + precon[i + 3];
    }
    return;

  case filAverage:
    recon[0] = recon[0] + precon[0] / 2;
    recon[1] = recon[1] + precon[1] / 2;
    recon[2] = recon[2] + precon[2] / 2;
    recon[3] = recon[3] + precon[3] / 2;

    for (size_t i = 4; i < 4 * cx; i += 4) {
      recon[i + 0] = recon[i + 0] + ((recon[i - 4 + 0] + precon[i + 0]) / 2);
      recon[i + 1] = recon[i + 1] + ((recon[i - 4 + 1] + precon[i + 1]) / 2);
      recon[i + 2] = recon[i + 2] + ((recon[i - 4 + 2] + precon[i + 2]) / 2);
      recon[i + 3] = recon[i + 3] + ((recon[i - 4 + 3] + precon[i + 3]) / 2);
    }
    return;

  case filPaeth:
    // paeth(0, precon[i], 0) is always precon[i]
    recon[0] = (recon[0] + precon[0]);
    recon[1] = (recon[1] + precon[1]);
    recon[2] = (recon[2] + precon[2]);
    recon[3] = (recon[3] + precon[3]);

    for (size_t i = 4; i < 4 * cx; i += 4) {
      recon[i + 0] = (recon[i + 0] + paeth(recon[i - 4 + 0], precon[i + 0], precon[i - 4 + 0]));
      recon[i + 1] = (recon[i + 1] + paeth(recon[i - 4 + 1], precon[i + 1], precon[i - 4 + 1]));
      recon[i + 2] = (recon[i + 2] + paeth(recon[i - 4 + 2], precon[i + 2], precon[i - 4 + 2]));
      recon[i + 3] = (recon[i + 3] + paeth(recon[i - 4 + 3], precon[i + 3], precon[i - 4 + 3]));
    }
    return;
  }
}

// ----------------------------------------------------------------------------
static void unfilter_byte1(uint8_t* data, size_t stride, const gfx::geo_t& geo)
{
  unfilterScanline0_byte1(data + 1, (enum FilterType)data[0], stride - 1);

  for (unsigned y = 1; y < geo.cy; y++) {
    const uint8_t* prev = data;
    data += stride;
    unfilterScanlineN_byte1(data + 1, prev + 1, (enum FilterType)data[0], stride - 1);
  }
}

// ----------------------------------------------------------------------------
static void unfilter_byte3(uint8_t* data, size_t stride, const gfx::geo_t& geo)
{
  unfilterScanline0_byte3(data + 1, (enum FilterType)data[0], geo.cx);

  for (unsigned y = 1; y < geo.cy; y++) {
    const uint8_t* prev = data;
    data += stride;
    unfilterScanlineN_byte3(data + 1, prev + 1, (enum FilterType)data[0], geo.cx);
  }
}

// ----------------------------------------------------------------------------
static void unfilter_byte4(uint8_t* data, size_t stride, const gfx::geo_t& geo)
{
  unfilterScanline0_byte4(data + 1, (enum FilterType)data[0], geo.cx);

  for (unsigned y = 1; y < geo.cy; y++) {
    const uint8_t* prev = data;
    data += stride;
    unfilterScanlineN_byte4(data + 1, prev + 1, (enum FilterType)data[0], geo.cx);
  }
}

// ----------------------------------------------------------------------------
void decodeIDAT(const std::vector<uint8_t> pidat, const gfx::geo_t& geo, gfx::image_t& image)
{
  const gfx::stride_t stride = gfx::cf_stride(geo.cf, geo.cx, 1);
  const size_t bufsize = make_buffersize(geo.cf, stride, geo.cy);

  uint8_t* scanlines = (uint8_t*)malloc(bufsize + geo.cy);
  zlib_decompress(scanlines, bufsize + geo.cy, pidat.data(), pidat.size());

  switch (geo.cf) {
  case gfx::cfY1:
  case gfx::cfY2:
  case gfx::cfY4:
  case gfx::cfY8:
  case gfx::cfPAL1:
  case gfx::cfPAL2:
  case gfx::cfPAL4:
  case gfx::cfPAL8:
    unfilter_byte1(scanlines, stride + 1, geo);
    break;

  case gfx::cfRGB0888:
    unfilter_byte3(scanlines, stride + 1, geo);
    break;

  case gfx::cfRGB8888:
    unfilter_byte4(scanlines, stride + 1, geo);
    break;

  default:
    break;
  }

  const uint8_t* data = scanlines;
  uint8_t* ptr = (uint8_t*)image.data.data() + image.stride * (geo.cy - 1);
  for (unsigned y = 0; y < geo.cy; y++) {
    memcpy(ptr, data + 1, stride);
    data += stride + 1;
    ptr -= image.stride;
  }

  free(scanlines);
}
