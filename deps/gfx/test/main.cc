#include "ccunit.h"

// ----------------------------------------------------------------------------
ccunit::Tests test_colorformat();
ccunit::Tests test_image();
ccunit::Tests test_pngread();

  // ----------------------------------------------------------------------------
void emplace_back(ccunit::Tests& v2, ccunit::Tests&& v1)
{
  v2.insert(
    v2.end(),
    std::make_move_iterator(v1.begin()),
    std::make_move_iterator(v1.end()));
}

// ----------------------------------------------------------------------------
int main(int argc, char* argv[])
{
  ccunit::Tests combined;
  emplace_back(combined, test_colorformat());
  emplace_back(combined, test_image());
  emplace_back(combined, test_pngread());

  return ccunit::runTests(combined, ccunit::VerboseMode::printFailuresOnly);
}
