#include "ccunit.h"

#include "gfx_colorformat.h"

// ----------------------------------------------------------------------------
static void test_bpp()
{
  EXPECT_EQ(1, gfx::cf_bpp(gfx::cfY1));
  EXPECT_EQ(2, gfx::cf_bpp(gfx::cfY2));
  EXPECT_EQ(4, gfx::cf_bpp(gfx::cfY4));
  EXPECT_EQ(8, gfx::cf_bpp(gfx::cfY8));

  EXPECT_EQ(1, gfx::cf_bpp(gfx::cfPAL1));
  EXPECT_EQ(2, gfx::cf_bpp(gfx::cfPAL2));
  EXPECT_EQ(4, gfx::cf_bpp(gfx::cfPAL4));
  EXPECT_EQ(8, gfx::cf_bpp(gfx::cfPAL8));

  EXPECT_EQ(16, gfx::cf_bpp(gfx::cfRGB0565));
  EXPECT_EQ(16, gfx::cf_bpp(gfx::cfRGB4444));

  EXPECT_EQ(24, gfx::cf_bpp(gfx::cfRGB0888));

  EXPECT_EQ(32, gfx::cf_bpp(gfx::cfRGB8888));

  EXPECT_EQ(48, gfx::cf_bpp(gfx::cfRGB0ggg));

  EXPECT_EQ(64, gfx::cf_bpp(gfx::cfRGBgggg));
}

// ----------------------------------------------------------------------------
static void test_isIndexed()
{
  EXPECT_TRUE(gfx::cf_isindexed(gfx::cfPAL8));
  EXPECT_TRUE(gfx::cf_isindexed(gfx::cfPAL4));
  EXPECT_TRUE(gfx::cf_isindexed(gfx::cfPAL2));
  EXPECT_TRUE(gfx::cf_isindexed(gfx::cfPAL1));

  EXPECT_FALSE(gfx::cf_isindexed(gfx::cfYg));
  EXPECT_FALSE(gfx::cf_isindexed(gfx::cfRGB0888));
  EXPECT_FALSE(gfx::cf_isindexed(gfx::cfRGB0565));
  EXPECT_FALSE(gfx::cf_isindexed(gfx::cfRGB4444));
}

// ----------------------------------------------------------------------------
static void test_stride()
{
  EXPECT_EQ(gfx::cf_stride(gfx::cfPAL8, 1, 1), 1);
  EXPECT_EQ(gfx::cf_stride(gfx::cfPAL8, 1, 2), 2);
  EXPECT_EQ(gfx::cf_stride(gfx::cfPAL8, 3, 4), 4);

  EXPECT_EQ(gfx::cf_stride(gfx::cfPAL4, 1, 1), 1);
  EXPECT_EQ(gfx::cf_stride(gfx::cfPAL4, 2, 1), 1);

  EXPECT_EQ(gfx::cf_stride(gfx::cfPAL8, 5, 2), 6);
  EXPECT_EQ(gfx::cf_stride(gfx::cfRGB0888, 5, 1), 15);
  EXPECT_EQ(gfx::cf_stride(gfx::cfRGB0888, 5, 2), 16);
  EXPECT_EQ(gfx::cf_stride(gfx::cfRGB8888, 5, 16), 32);
}

// ----------------------------------------------------------------------------
ccunit::Tests test_colorformat()
{
  ccunit::Tests tests;

  tests.push_back(std::make_unique<ccunit::Test>("gfx(bpp)", test_bpp));
  tests.push_back(std::make_unique<ccunit::Test>("gfx(isindexed)", test_isIndexed));
  tests.push_back(std::make_unique<ccunit::Test>("gfx(stride)", test_stride));

  return tests;
}
