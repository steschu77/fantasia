#include "ccunit.h"

#include "gfx_image.h"

// ----------------------------------------------------------------------------
static void test_pal1_to_rgb32()
{
  gfx::geo_t geo { gfx::cfPAL1, 2, 2 };
  gfx::image_t img { 2, { 64, 0, 192, 0 }, { 2, 4, 6, 8 } };
  gfx::image_t rgb = gfx::pal1_to_rgb32(img, geo);

  EXPECT_EQ(reinterpret_cast<uint32_t*>(rgb.data.data())[0], 2);
  EXPECT_EQ(reinterpret_cast<uint32_t*>(rgb.data.data())[1], 4);
  EXPECT_EQ(reinterpret_cast<uint32_t*>(rgb.data.data())[2], 4);
  EXPECT_EQ(reinterpret_cast<uint32_t*>(rgb.data.data())[3], 4);
}

// ----------------------------------------------------------------------------
static void test_pal2_to_rgb32()
{
  gfx::geo_t geo { gfx::cfPAL2, 2, 2 };
  gfx::image_t img { 2, { 16, 0, 0xb0, 0 }, { 2, 4, 6, 8 } };
  gfx::image_t rgb = gfx::pal2_to_rgb32(img, geo);

  EXPECT_EQ(reinterpret_cast<uint32_t*>(rgb.data.data())[0], 2);
  EXPECT_EQ(reinterpret_cast<uint32_t*>(rgb.data.data())[1], 4);
  EXPECT_EQ(reinterpret_cast<uint32_t*>(rgb.data.data())[2], 6);
  EXPECT_EQ(reinterpret_cast<uint32_t*>(rgb.data.data())[3], 8);
}

// ----------------------------------------------------------------------------
static void test_pal4_to_rgb32()
{
  gfx::geo_t geo { gfx::cfPAL4, 2, 2 };
  gfx::image_t img { 2, { 0x01, 1, 0x23, 3 }, { 2, 4, 6, 8 } };
  gfx::image_t rgb = gfx::pal4_to_rgb32(img, geo);

  EXPECT_EQ(reinterpret_cast<uint32_t*>(rgb.data.data())[0], 2);
  EXPECT_EQ(reinterpret_cast<uint32_t*>(rgb.data.data())[1], 4);
  EXPECT_EQ(reinterpret_cast<uint32_t*>(rgb.data.data())[2], 6);
  EXPECT_EQ(reinterpret_cast<uint32_t*>(rgb.data.data())[3], 8);
}

// ----------------------------------------------------------------------------
void test_pal8_to_rgb32()
{
  gfx::geo_t geo { gfx::cfPAL8, 2, 2 };
  gfx::image_t img { 2, { 0, 1, 2, 3 }, { 2, 4, 6, 8 } };
  gfx::image_t rgb = gfx::pal8_to_rgb32(img, geo);

  EXPECT_EQ(reinterpret_cast<uint32_t*>(rgb.data.data())[0], 2);
  EXPECT_EQ(reinterpret_cast<uint32_t*>(rgb.data.data())[1], 4);
  EXPECT_EQ(reinterpret_cast<uint32_t*>(rgb.data.data())[2], 6);
  EXPECT_EQ(reinterpret_cast<uint32_t*>(rgb.data.data())[3], 8);
}

// ----------------------------------------------------------------------------
ccunit::Tests test_image()
{
  ccunit::Tests tests;

  tests.push_back(std::make_unique<ccunit::Test>("image(pal1_rgb32)", test_pal1_to_rgb32));
  tests.push_back(std::make_unique<ccunit::Test>("image(pal2_rgb32)", test_pal2_to_rgb32));
  tests.push_back(std::make_unique<ccunit::Test>("image(pal4_rgb32)", test_pal4_to_rgb32));
  tests.push_back(std::make_unique<ccunit::Test>("image(pal8_rgb32)", test_pal8_to_rgb32));

  return tests;
}
