#include "ccunit.h"

#include "gfx_pngread.h"

#include <memory.h>

  // clang-format off
// ----------------------------------------------------------------------------
static const uint8_t png_data0[93] = {
	0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, 0x00, 0x00, 0x00, 0x0D,
	0x49, 0x48, 0x44, 0x52, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x08,
	0x01, 0x03, 0x00, 0x00, 0x00, 0xFE, 0xC1, 0x2C, 0xC8, 0x00, 0x00, 0x00,
	0x06, 0x50, 0x4C, 0x54, 0x45, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xA5,
	0xD9, 0x9F, 0xDD, 0x00, 0x00, 0x00, 0x12, 0x49, 0x44, 0x41, 0x54, 0x08,
	0xD7, 0x63, 0xB0, 0x61, 0x70, 0x62, 0x68, 0x84, 0x42, 0x27, 0x06, 0x1B,
	0x00, 0x18, 0x10, 0x03, 0x01, 0x2E, 0x59, 0x53, 0xB7, 0x00, 0x00, 0x00,
	0x00, 0x49, 0x45, 0x4E, 0x44, 0xAE, 0x42, 0x60, 0x82
};

// ----------------------------------------------------------------------------
static const uint8_t png_image0[8] = {
	0x3c, 0x42, 0x81, 0x81, 0x81, 0x81, 0x42, 0x3c
};

// ----------------------------------------------------------------------------
static const uint8_t png_data2[201] = {
	0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, 0x00, 0x00, 0x00, 0x0D,
	0x49, 0x48, 0x44, 0x52, 0x00, 0x00, 0x00, 0x09, 0x00, 0x00, 0x00, 0x06,
	0x08, 0x02, 0x00, 0x00, 0x00, 0x9E, 0xA5, 0x23, 0x92, 0x00, 0x00, 0x00,
	0x90, 0x49, 0x44, 0x41, 0x54, 0x08, 0xD7, 0x6D, 0xC1, 0xA1, 0x11, 0xC2,
	0x40, 0x10, 0x05, 0xD0, 0xBF, 0xDC, 0x5E, 0x2E, 0x77, 0xBB, 0x90, 0x19,
	0x0A, 0x08, 0x86, 0xCC, 0x10, 0x03, 0x51, 0x51, 0xA1, 0x02, 0xF0, 0xA9,
	0x00, 0x34, 0xA5, 0x50, 0x06, 0x25, 0xE0, 0x71, 0x34, 0x90, 0x06, 0x18,
	0x24, 0x0A, 0x83, 0xC8, 0x2C, 0x0A, 0xC7, 0x7B, 0xE4, 0x42, 0xC3, 0x51,
	0x39, 0x89, 0x4F, 0xCA, 0x51, 0xBC, 0x68, 0x51, 0x2E, 0xBB, 0xB6, 0x3E,
	0xEE, 0x37, 0x70, 0xA1, 0xB1, 0x7F, 0xEE, 0xC3, 0x93, 0x0D, 0x08, 0xF3,
	0xAD, 0x8F, 0xE2, 0xA3, 0xB0, 0x68, 0x96, 0x74, 0xB6, 0xA8, 0x86, 0xCB,
	0xC9, 0x80, 0x09, 0x80, 0xCF, 0xEB, 0x06, 0x02, 0x88, 0x6C, 0x1C, 0x0D,
	0x30, 0x33, 0x00, 0x00, 0x31, 0x00, 0x00, 0xEF, 0xC7, 0x15, 0x3F, 0xAB,
	0xFE, 0x0C, 0x80, 0xC8, 0xC8, 0xE5, 0x0D, 0x47, 0xE5, 0x28, 0x3E, 0xA9,
	0x0B, 0x79, 0x36, 0x2D, 0x8A, 0xB2, 0xEA, 0xDA, 0xFA, 0xB0, 0x5B, 0x7F,
	0x01, 0x5E, 0x70, 0x30, 0x08, 0xD2, 0x0E, 0x8A, 0xBB, 0x00, 0x00, 0x00,
	0x00, 0x49, 0x45, 0x4E, 0x44, 0xAE, 0x42, 0x60, 0x82
};

// ----------------------------------------------------------------------------
uint8_t png_image2[3 * 9 * 6] = {
  0x30, 0x07, 0x03, 0x3c, 0x10, 0x07, 0x47, 0x1a, 0x0b, 0x53, 0x24, 0x10, 0x5e, 0x2d, 0x14, 0x6a, 0x38, 0x19, 0x8e, 0x56, 0x28, 0xb7, 0x8c, 0x64, 0xe6, 0xd8, 0xca,
  0x30, 0x07, 0x03, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xe6, 0xd7, 0xca,
  0x30, 0x07, 0x02, 0x3c, 0x11, 0x06, 0x47, 0x1a, 0x0b, 0x52, 0x23, 0x10, 0x5e, 0x2e, 0x14, 0x6a, 0x38, 0x1a, 0x8f, 0x57, 0x28, 0xff, 0xff, 0xff, 0xe6, 0xd7, 0xc9,
  0x30, 0x07, 0x02, 0xff, 0xff, 0xff, 0x47, 0x1b, 0x0b, 0x53, 0x24, 0x10, 0x5b, 0x2b, 0x13, 0x6a, 0x38, 0x19, 0x8e, 0x56, 0x27, 0xff, 0xff, 0xff, 0xe7, 0xd7, 0xc9,
  0x30, 0x07, 0x02, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x8f, 0x56, 0x27, 0xff, 0xff, 0xff, 0xe6, 0xd8, 0xca,
  0x30, 0x08, 0x03, 0x3c, 0x11, 0x07, 0x47, 0x1a, 0x0b, 0x53, 0x24, 0x10, 0x5b, 0x2b, 0x13, 0x6a, 0x38, 0x19, 0x8f, 0x56, 0x28, 0xb8, 0x8c, 0x64, 0xe6, 0xd7, 0xc9,
};

// ----------------------------------------------------------------------------
static const uint8_t png_data3[502] = {
	0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, 0x00, 0x00, 0x00, 0x0D,
	0x49, 0x48, 0x44, 0x52, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x07,
	0x02, 0x03, 0x00, 0x00, 0x00, 0x03, 0x8C, 0x1D, 0xF8, 0x00, 0x00, 0x00,
	0x09, 0x50, 0x4C, 0x54, 0x45, 0x30, 0x00, 0x8F, 0x00, 0x00, 0x00, 0xFF,
	0xFF, 0xFF, 0xD3, 0xB2, 0x95, 0x1B, 0x00, 0x00, 0x00, 0x01, 0x74, 0x52,
	0x4E, 0x53, 0x00, 0x40, 0xE6, 0xD8, 0x66, 0x00, 0x00, 0x01, 0x9B, 0x49,
	0x44, 0x41, 0x54, 0x28, 0xCF, 0x6D, 0x93, 0x4B, 0x8E, 0x24, 0x31, 0x08,
	0x44, 0xFB, 0x88, 0xB3, 0x01, 0x09, 0xEF, 0xB1, 0x64, 0x4E, 0x33, 0x1B,
	0x52, 0xB2, 0xF7, 0x4E, 0xC9, 0x9C, 0x72, 0x02, 0xB2, 0x7A, 0xFA, 0xCB,
	0xCA, 0x45, 0x91, 0xC4, 0x73, 0x80, 0xDF, 0x88, 0x59, 0xA7, 0x0C, 0xDE,
	0xCA, 0x42, 0xAF, 0x30, 0xE6, 0x7D, 0x9A, 0xDD, 0x77, 0x58, 0xF3, 0x27,
	0x75, 0xAC, 0xAD, 0x7D, 0xC2, 0xE2, 0x1C, 0x8F, 0x08, 0x76, 0x1D, 0x72,
	0x02, 0xB9, 0x88, 0x3D, 0x1A, 0xFB, 0xB6, 0x33, 0xA8, 0x91, 0x93, 0x52,
	0xB6, 0x71, 0xDA, 0xE4, 0x5C, 0x89, 0xF7, 0xD0, 0xF7, 0x83, 0x88, 0xFC,
	0x79, 0xFB, 0x88, 0x02, 0xC0, 0xD7, 0xAA, 0xC2, 0x32, 0xAB, 0x8A, 0x7D,
	0x49, 0xE3, 0xAE, 0x82, 0xD3, 0x56, 0x57, 0x46, 0x0D, 0x4E, 0xA5, 0x23,
	0x38, 0x5D, 0x84, 0xD3, 0x38, 0xAD, 0x72, 0x24, 0x5A, 0x7F, 0xA8, 0xA8,
	0xA3, 0x15, 0xE4, 0x0B, 0x80, 0x13, 0x82, 0xF8, 0x37, 0x00, 0x08, 0x7D,
	0x05, 0x20, 0x8E, 0x49, 0x33, 0xD9, 0xAF, 0x5D, 0x55, 0x72, 0xDD, 0x24,
	0x7C, 0x01, 0x40, 0x3C, 0x33, 0x62, 0xFB, 0xE2, 0x95, 0x62, 0x09, 0x40,
	0x09, 0x20, 0xDE, 0xED, 0x33, 0x40, 0x17, 0x57, 0x05, 0x29, 0x22, 0x9D,
	0xB2, 0xB5, 0x4E, 0x84, 0x51, 0x88, 0x8F, 0x34, 0xAE, 0x1D, 0x38, 0xA5,
	0x2F, 0xA7, 0x7E, 0x02, 0xD0, 0x84, 0xE7, 0x89, 0xB8, 0x22, 0xC8, 0x36,
	0xE9, 0xBC, 0x48, 0x47, 0xC4, 0xDA, 0x6A, 0x6D, 0x17, 0xBB, 0xCA, 0x8C,
	0xB8, 0xC9, 0x63, 0xDD, 0x14, 0x00, 0xD8, 0x18, 0xC1, 0xE2, 0x50, 0xDB,
	0xA7, 0x00, 0x92, 0x0B, 0x8D, 0xEB, 0x86, 0xA5, 0xD3, 0x1F, 0xA7, 0xB6,
	0xF7, 0x47, 0xB6, 0x91, 0x16, 0x28, 0x30, 0x5D, 0xE8, 0xEF, 0x77, 0x07,
	0x64, 0xCA, 0x7C, 0x1C, 0x28, 0xFF, 0xF6, 0x05, 0x0F, 0x99, 0x86, 0xB8,
	0xBB, 0x42, 0xDC, 0xA0, 0xF7, 0xDF, 0x81, 0xF6, 0x38, 0xA0, 0xC3, 0x18,
	0xDD, 0x31, 0x37, 0x34, 0xEE, 0xD0, 0x23, 0xE5, 0xC7, 0x81, 0x83, 0x3A,
	0xC0, 0xC6, 0xCB, 0x29, 0xC8, 0xC2, 0x01, 0x3A, 0x05, 0x20, 0x89, 0xFE,
	0xCD, 0x01, 0xC4, 0x0C, 0x3D, 0x98, 0x5E, 0xEE, 0x80, 0xD4, 0xE0, 0x2E,
	0x0C, 0xFD, 0x1D, 0xA0, 0x76, 0x00, 0x54, 0xFA, 0xB1, 0x03, 0xD8, 0x90,
	0xE7, 0x27, 0xD6, 0x17, 0x00, 0xBD, 0x2F, 0xE4, 0xF2, 0x02, 0x35, 0xE2,
	0xAA, 0xC3, 0x37, 0x9F, 0xEA, 0x68, 0x10, 0xEB, 0xEE, 0x08, 0xEA, 0xF4,
	0xDB, 0x08, 0x84, 0xEE, 0x49, 0xF5, 0x0A, 0x34, 0x97, 0xC7, 0x72, 0xD9,
	0xD9, 0xAE, 0xED, 0xF9, 0x0A, 0x6A, 0x8F, 0xD4, 0xD2, 0xEE, 0x30, 0x5C,
	0x05, 0xD7, 0xBB, 0x19, 0xBB, 0x2F, 0xC7, 0xDB, 0x68, 0x47, 0x60, 0x09,
	0xC4, 0x5A, 0xBD, 0x02, 0x0E, 0x6A, 0x91, 0x3B, 0x70, 0xBF, 0xEA, 0x2C,
	0x5F, 0x0B, 0x00, 0xF8, 0x34, 0x5C, 0x7E, 0x3A, 0x96, 0xE3, 0xEB, 0x2B,
	0xF8, 0x07, 0x6B, 0x8A, 0x6A, 0x05, 0x3B, 0xA2, 0xF3, 0xC7, 0x00, 0x00,
	0x00, 0x00, 0x49, 0x45, 0x4E, 0x44, 0xAE, 0x42, 0x60, 0x82
};

// clang-format on

// ----------------------------------------------------------------------------
static void test_file1()
{
	gfx::picture_t pic = gfx::png_read(png_data0, sizeof(png_data0));

  EXPECT_EQ(pic.geo.cf, gfx::cfPAL1);
  EXPECT_EQ(pic.geo.cx, 8);
  EXPECT_EQ(pic.geo.cy, 8);
  EXPECT_EQ(memcmp(pic.img.data.data(), png_image0, 8), 0);
}

// ----------------------------------------------------------------------------
static void test_file2()
{
  gfx::picture_t pic = gfx::png_read(png_data2, sizeof(png_data2));
  EXPECT_EQ(pic.geo.cx, 9);
  EXPECT_EQ(pic.geo.cy, 6);
  //EXPECT_EQ(memcmp(pic.img.data.data(), png_image2, 3 * 9 * 6), 0); // fixme: decode with reference RGB vs BGR
}

// ----------------------------------------------------------------------------
static void test_file3()
{
  gfx::picture_t pic = gfx::png_read(png_data3, sizeof(png_data3));

  EXPECT_EQ(pic.geo.cf, gfx::cfPAL2);
  EXPECT_EQ(pic.geo.cx, 512);
  EXPECT_EQ(pic.geo.cy, 7);
}

// ----------------------------------------------------------------------------
ccunit::Tests test_pngread()
{
  ccunit::Tests tests;

  tests.push_back(std::make_unique<ccunit::Test>("pngread(file1)", test_file1));
	tests.push_back(std::make_unique<ccunit::Test>("pngread(file2)", test_file2));
	tests.push_back(std::make_unique<ccunit::Test>("pngread(file3)", test_file3));

  return tests;
}
