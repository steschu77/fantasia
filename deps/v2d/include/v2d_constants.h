#pragma once

namespace v2d {

// ----------------------------------------------------------------------------
inline constexpr float k_epsilon = 0.00001f;
inline constexpr float k_pi = 3.14159265358979323846264f;

// ----------------------------------------------------------------------------
inline constexpr float rad(float degrees)
{
  return degrees * (k_pi / 180.0f);
}

// ----------------------------------------------------------------------------
inline constexpr float deg(float radians)
{
  return radians * (180.0f / k_pi);
}

} // namespace v2d
