#pragma once

#include <float.h>
#include <math.h>

#include "v2d_v2.h"

namespace v2d {

// ----------------------------------------------------------------------------
struct m2x2
{
  constexpr m2x2();
  constexpr m2x2(float const& d0, float const& d1);
  constexpr m2x2(const v2& u0, const v2& u1);

  constexpr static m2x2 zero();
  constexpr static m2x2 identity();
  constexpr static m2x2 diag(float d0, float d1);
  static m2x2 rotation(float r);

  template <unsigned idx>
  constexpr v2 col() const;
  template <unsigned idx>
  constexpr v2 row() const;

  constexpr m2x2 operator-() const;
  constexpr m2x2 operator+(const m2x2& v) const;
  constexpr m2x2 operator-(const m2x2& v) const;
  constexpr m2x2 operator*(float s) const;

  constexpr m2x2 transpose() const;
  constexpr float det() const;

  m2x2 inverse() const;
  v2 solve(const v2& v) const;

  m2x2 abs() const;

  float m[2][2];
};

constexpr m2x2 operator*(float s, const m2x2& m);

v2 operator*(const v2& v, const m2x2& m);
v2 operator*(const m2x2& m, const v2& v);
m2x2 operator*(const m2x2& m0, const m2x2& m1);

} // namespace v2d

// ============================================================================
constexpr v2d::m2x2::m2x2()
: m { { 0, 0 }, { 0, 0 } }
{
}

// ----------------------------------------------------------------------------
constexpr v2d::m2x2::m2x2(float const& d0, float const& d1)
: m { { d0, 0 }, { 0, d1 } }
{
}

// ----------------------------------------------------------------------------
constexpr v2d::m2x2::m2x2(const v2& u0, const v2& u1)
: m { { u0.m[0], u0.m[1] }, { u1.m[0], u1.m[1] } }
{
}

// ----------------------------------------------------------------------------
constexpr v2d::m2x2 v2d::m2x2::zero()
{
  return m2x2 {};
}

// ----------------------------------------------------------------------------
constexpr v2d::m2x2 v2d::m2x2::identity()
{
  return m2x2 { 1.0f, 1.0f };
}

// ----------------------------------------------------------------------------
constexpr v2d::m2x2 v2d::m2x2::diag(float d0, float d1)
{
  return m2x2 { d0, d1 };
}

// ----------------------------------------------------------------------------
inline v2d::m2x2 v2d::m2x2::rotation(float r)
{
  const float c = cosf(r), s = sinf(r);
  return m2x2 { { c, -s }, { s, c } };
}

// ----------------------------------------------------------------------------
template <unsigned idx>
constexpr v2d::v2 v2d::m2x2::col() const
{
  static_assert(idx < 2, "index out of range");
  return v2 { m[0][idx], m[1][idx] };
}

// ----------------------------------------------------------------------------
template <unsigned idx>
constexpr v2d::v2 v2d::m2x2::row() const
{
  static_assert(idx < 2, "index out of range");
  return v2 { m[idx][0], m[idx][1] };
}

// ----------------------------------------------------------------------------
constexpr v2d::m2x2 v2d::m2x2::operator-() const
{
  return m2x2 {
    { -m[0][0], -m[0][1] },
    { -m[1][0], -m[1][1] }
  };
}

// ----------------------------------------------------------------------------
constexpr v2d::m2x2 v2d::m2x2::operator+(const v2d::m2x2& v) const
{
  return m2x2 {
    { m[0][0] + v.m[0][0], m[0][1] + v.m[0][1] },
    { m[1][0] + v.m[1][0], m[1][1] + v.m[1][1] }
  };
}

// ----------------------------------------------------------------------------
constexpr v2d::m2x2 v2d::m2x2::operator-(const v2d::m2x2& v) const
{
  return m2x2 {
    { m[0][0] - v.m[0][0], m[0][1] - v.m[0][1] },
    { m[1][0] - v.m[1][0], m[1][1] - v.m[1][1] }
  };
}

// ----------------------------------------------------------------------------
constexpr v2d::m2x2 v2d::m2x2::operator*(float s) const
{
  return m2x2 {
    { m[0][0] * s, m[0][1] * s },
    { m[1][0] * s, m[1][1] * s }
  };
}

// ----------------------------------------------------------------------------
constexpr v2d::m2x2 v2d::m2x2::transpose() const
{
  return m2x2 {
    { m[0][0], m[1][0] },
    { m[0][1], m[1][1] }
  };
}

// ----------------------------------------------------------------------------
constexpr float v2d::m2x2::det() const
{
  return m[0][0] * m[1][1] - m[0][1] * m[1][0];
}

// ----------------------------------------------------------------------------
inline v2d::m2x2 v2d::m2x2::inverse() const
{
  const float d = det();
  if (fabs(d) < k_epsilon) {
    return zero();
  } else {
    const float inv_d = 1.0f / d;
    return m2x2 {
      { inv_d * m[1][1], -inv_d * m[1][0] },
      { -inv_d * m[0][1], inv_d * m[0][0] }
    };
  }
}

// ----------------------------------------------------------------------------
// Solve M * x = v, where v is a column vector. This is more efficient
// than computing the inverse in one-shot cases.
inline v2d::v2 v2d::m2x2::solve(const v2d::v2& v) const
{
  const float d = det();
  if (fabs(d) < k_epsilon) {
    return v2 { 0.0f, 0.0f };
  } else {
    const float inv_d = 1.0f / d;
    const float u0 = inv_d * (m[1][1] * v.m[0] - m[0][1] * v.m[1]);
    const float u1 = inv_d * (m[0][0] * v.m[1] - m[1][0] * v.m[0]);
    return v2 { u0, u1 };
  }
}

// ----------------------------------------------------------------------------
inline v2d::m2x2 v2d::m2x2::abs() const
{
  return m2x2 {
    { fabsf(m[0][0]), fabsf(m[0][1]) },
    { fabsf(m[1][0]), fabsf(m[1][1]) }
  };
}

// ----------------------------------------------------------------------------
constexpr v2d::m2x2 v2d::operator*(float s, const v2d::m2x2& m)
{
  return m * s;
}

// ----------------------------------------------------------------------------
inline v2d::v2 v2d::operator*(const v2d::v2& v1, const v2d::m2x2& m2)
{
  const float u0 = v1.m[0] * m2.m[0][0] + v1.m[1] * m2.m[1][0];
  const float u1 = v1.m[0] * m2.m[0][1] + v1.m[1] * m2.m[1][1];
  return v2 { u0, u1 };
}

// ----------------------------------------------------------------------------
inline v2d::v2 v2d::operator*(const v2d::m2x2& m2, const v2d::v2& v1)
{
  const float u0 = m2.m[0][0] * v1.m[0] + m2.m[0][1] * v1.m[1];
  const float u1 = m2.m[1][0] * v1.m[0] + m2.m[1][1] * v1.m[1];
  return v2 { u0, u1 };
}

// ----------------------------------------------------------------------------
inline v2d::m2x2 v2d::operator*(const v2d::m2x2& m1, const v2d::m2x2& m2)
{
  const float u00 = m1.m[0][0] * m2.m[0][0] + m1.m[0][1] * m2.m[1][0];
  const float u01 = m1.m[0][0] * m2.m[0][1] + m1.m[0][1] * m2.m[1][1];
  const float u10 = m1.m[1][0] * m2.m[0][0] + m1.m[1][1] * m2.m[1][0];
  const float u11 = m1.m[1][0] * m2.m[0][1] + m1.m[1][1] * m2.m[1][1];
  return m2x2 { { u00, u01 }, { u10, u11 } };
}
