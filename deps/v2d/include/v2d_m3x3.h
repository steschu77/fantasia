#pragma once

#include <float.h>
#include <math.h>

#include "v2d_v3.h"

namespace v2d {

// ----------------------------------------------------------------------------
struct m3x3
{
  constexpr m3x3();
  constexpr m3x3(float const& d0, float const& d1, float const& d2);
  constexpr m3x3(const v3& u0, const v3& u1, const v3& u2);

  constexpr static m3x3 zero();
  constexpr static m3x3 identity();
  constexpr static m3x3 diag(float d0, float d1, float d2);

  template <unsigned idx>
  constexpr v3 col() const;
  template <unsigned idx>
  constexpr v3 row() const;

  constexpr m3x3 operator-() const;
  constexpr m3x3 operator+(const m3x3& v) const;
  constexpr m3x3 operator-(const m3x3& v) const;
  constexpr m3x3 operator*(float s) const;

  constexpr m3x3 transpose() const;
  constexpr float det() const;

  m3x3 inverse() const;
  v3 solve(const v3& v) const;

  m3x3 abs() const;

  float m[3][3];
};

constexpr m3x3 operator*(float s, const m3x3& m);

v3 operator*(const v3& v, const m3x3& m);
v3 operator*(const m3x3& m, const v3& v);
m3x3 operator*(const m3x3& m0, const m3x3& m1);

} // namespace v2d

// ============================================================================
constexpr v2d::m3x3::m3x3()
: m { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } }
{
}

// ----------------------------------------------------------------------------
constexpr v2d::m3x3::m3x3(float const& d0, float const& d1, float const& d2)
: m { { d0, 0, 0 }, { 0, d1, 0 }, { 0, 0, d2 } }
{
}

// ----------------------------------------------------------------------------
constexpr v2d::m3x3::m3x3(const v3& u0, const v3& u1, const v3& u2)
: m {
  { u0.m[0], u0.m[1], u0.m[2] },
  { u1.m[0], u1.m[1], u1.m[2] },
  { u2.m[0], u2.m[1], u2.m[2] }
}
{
}

// ----------------------------------------------------------------------------
constexpr v2d::m3x3 v2d::m3x3::zero()
{
  return m3x3 {};
}

// ----------------------------------------------------------------------------
constexpr v2d::m3x3 v2d::m3x3::identity()
{
  return m3x3 { 1.0f, 1.0f, 1.0f };
}

// ----------------------------------------------------------------------------
constexpr v2d::m3x3 v2d::m3x3::diag(float d0, float d1, float d2)
{
  return m3x3 { d0, d1, d2 };
}

// ----------------------------------------------------------------------------
template <unsigned idx>
constexpr v2d::v3 v2d::m3x3::col() const
{
  static_assert(idx < 3, "index out of range");
  return v3 { m[0][idx], m[1][idx], m[2][idx] };
}

// ----------------------------------------------------------------------------
template <unsigned idx>
constexpr v2d::v3 v2d::m3x3::row() const
{
  static_assert(idx < 3, "index out of range");
  return v3 { m[idx][0], m[idx][1], m[idx][2] };
}

// ----------------------------------------------------------------------------
constexpr v2d::m3x3 v2d::m3x3::operator-() const
{
  return m3x3 {
    { -m[0][0], -m[0][1], -m[0][2] },
    { -m[1][0], -m[1][1], -m[1][2] },
    { -m[2][0], -m[2][1], -m[2][2] }
  };
}

// ----------------------------------------------------------------------------
constexpr v2d::m3x3 v2d::m3x3::operator+(const v2d::m3x3& v) const
{
  return m3x3 {
    { m[0][0] + v.m[0][0], m[0][1] + v.m[0][1], m[0][2] + v.m[0][2] },
    { m[1][0] + v.m[1][0], m[1][1] + v.m[1][1], m[1][2] + v.m[1][2] },
    { m[2][0] + v.m[2][0], m[2][1] + v.m[2][1], m[2][2] + v.m[2][2] }
  };
}

// ----------------------------------------------------------------------------
constexpr v2d::m3x3 v2d::m3x3::operator-(const v2d::m3x3& v) const
{
  return m3x3 {
    { m[0][0] - v.m[0][0], m[0][1] - v.m[0][1], m[0][2] - v.m[0][2] },
    { m[1][0] - v.m[1][0], m[1][1] - v.m[1][1], m[1][2] - v.m[1][2] },
    { m[2][0] - v.m[2][0], m[2][1] - v.m[2][1], m[2][2] - v.m[2][2] }
  };
}

// ----------------------------------------------------------------------------
constexpr v2d::m3x3 v2d::m3x3::operator*(float s) const
{
  return m3x3 {
    { m[0][0] * s, m[0][1] * s, m[0][2] * s },
    { m[1][0] * s, m[1][1] * s, m[1][2] * s },
    { m[2][0] * s, m[2][1] * s, m[2][2] * s }
  };
}

// ----------------------------------------------------------------------------
constexpr v2d::m3x3 v2d::m3x3::transpose() const
{
  return m3x3 {
    { m[0][0], m[1][0], m[2][0] },
    { m[0][1], m[1][1], m[2][1] },
    { m[0][2], m[1][2], m[2][2] }
  };
}

// ----------------------------------------------------------------------------
constexpr float v2d::m3x3::det() const
{
  // clang-format off
  return
      m[0][0] * (m[1][1] * m[2][2] - m[1][2] * m[2][1])
    - m[0][1] * (m[1][0] * m[2][2] - m[1][2] * m[2][0])
    + m[0][2] * (m[1][0] * m[2][1] - m[1][1] * m[2][0]);
  // clang-format on
}

// ----------------------------------------------------------------------------
inline v2d::m3x3 v2d::m3x3::inverse() const
{
  const float d = det();
  if (fabs(d) < k_epsilon) {
    return zero();
  } else {
    const float inv_d = 1.0f / d;

    const float u00 = inv_d * (m[1][1] * m[2][2] - m[1][2] * m[2][1]);
    const float u01 = inv_d * (m[0][2] * m[2][1] - m[0][1] * m[2][2]);
    const float u02 = inv_d * (m[0][1] * m[1][2] - m[0][2] * m[1][1]);

    const float u10 = inv_d * (m[1][2] * m[2][0] - m[1][0] * m[2][2]);
    const float u11 = inv_d * (m[0][0] * m[2][2] - m[0][2] * m[2][0]);
    const float u12 = inv_d * (m[0][2] * m[1][0] - m[0][0] * m[1][2]);

    const float u20 = inv_d * (m[1][0] * m[2][1] - m[1][1] * m[2][0]);
    const float u21 = inv_d * (m[0][1] * m[2][0] - m[0][0] * m[2][1]);
    const float u22 = inv_d * (m[0][0] * m[1][1] - m[0][1] * m[1][0]);

    return m3x3 {
      { u00, u01, u02 },
      { u10, u11, u12 },
      { u20, u21, u22 }
    };
  }
}

// ----------------------------------------------------------------------------
// Solve M * x = v, where v is a column vector. This is more efficient
// than computing the inverse in one-shot cases.
inline v2d::v3 v2d::m3x3::solve(const v2d::v3& v) const
{
  const float d = det();
  if (fabs(d) < k_epsilon) {
    return v3 { 0.0f, 0.0f, 0.0f };
  } else {
    const float inv_d = 1.0f / d;
    const float u0 = inv_d * (m[1][1] * m[2][2] - m[1][2] * m[2][1]) * v.m[0] + inv_d * (m[0][2] * m[2][1] - m[0][1] * m[2][2]) * v.m[1] + inv_d * (m[0][1] * m[1][2] - m[0][2] * m[1][1]) * v.m[2];
    const float u1 = inv_d * (m[1][2] * m[2][0] - m[1][0] * m[2][2]) * v.m[0] + inv_d * (m[0][0] * m[2][2] - m[0][2] * m[2][0]) * v.m[1] + inv_d * (m[0][2] * m[1][0] - m[0][0] * m[1][2]) * v.m[2];
    const float u2 = inv_d * (m[1][0] * m[2][1] - m[1][1] * m[2][0]) * v.m[0] + inv_d * (m[0][1] * m[2][0] - m[0][0] * m[2][1]) * v.m[1] + inv_d * (m[0][0] * m[1][1] - m[0][1] * m[1][0]) * v.m[2];
    return v3 { u0, u1, u2 };
  }
}

// ----------------------------------------------------------------------------
inline v2d::m3x3 v2d::m3x3::abs() const
{
  return m3x3 {
    { fabsf(m[0][0]), fabsf(m[0][1]), fabsf(m[0][2]) },
    { fabsf(m[1][0]), fabsf(m[1][1]), fabsf(m[1][2]) },
    { fabsf(m[2][0]), fabsf(m[2][1]), fabsf(m[2][2]) }
  };
}

// ----------------------------------------------------------------------------
constexpr v2d::m3x3 v2d::operator*(float s, const m3x3& m)
{
  return m * s;
}

// ----------------------------------------------------------------------------
inline v2d::v3 v2d::operator*(const v2d::v3& v, const v2d::m3x3& m)
{
  const float u0 = v.m[0] * m.m[0][0] + v.m[1] * m.m[1][0] + v.m[2] * m.m[2][0];
  const float u1 = v.m[0] * m.m[0][1] + v.m[1] * m.m[1][1] + v.m[2] * m.m[2][1];
  const float u2 = v.m[0] * m.m[0][2] + v.m[1] * m.m[1][2] + v.m[2] * m.m[2][2];
  return v3 { u0, u1, u2 };
}

// ----------------------------------------------------------------------------
inline v2d::v3 v2d::operator*(const v2d::m3x3& m, const v2d::v3& v)
{
  const float u0 = m.m[0][0] * v.m[0] + m.m[0][1] * v.m[1] + m.m[0][2] * v.m[2];
  const float u1 = m.m[1][0] * v.m[0] + m.m[1][1] * v.m[1] + m.m[1][2] * v.m[2];
  const float u2 = m.m[2][0] * v.m[0] + m.m[2][1] * v.m[1] + m.m[2][2] * v.m[2];
  return v3 { u0, u1, u2 };
}

// ----------------------------------------------------------------------------
inline v2d::m3x3 v2d::operator*(const v2d::m3x3& m0, const v2d::m3x3& m1)
{
  const float u00 = m0.m[0][0] * m1.m[0][0] + m0.m[0][1] * m1.m[1][0] + m0.m[0][2] * m1.m[2][0];
  const float u01 = m0.m[0][0] * m1.m[0][1] + m0.m[0][1] * m1.m[1][1] + m0.m[0][2] * m1.m[2][1];
  const float u02 = m0.m[0][0] * m1.m[0][2] + m0.m[0][1] * m1.m[1][2] + m0.m[0][2] * m1.m[2][2];

  const float u10 = m0.m[1][0] * m1.m[0][0] + m0.m[1][1] * m1.m[1][0] + m0.m[1][2] * m1.m[2][0];
  const float u11 = m0.m[1][0] * m1.m[0][1] + m0.m[1][1] * m1.m[1][1] + m0.m[1][2] * m1.m[2][1];
  const float u12 = m0.m[1][0] * m1.m[0][2] + m0.m[1][1] * m1.m[1][2] + m0.m[1][2] * m1.m[2][2];

  const float u20 = m0.m[2][0] * m1.m[0][0] + m0.m[2][1] * m1.m[1][0] + m0.m[2][2] * m1.m[2][0];
  const float u21 = m0.m[2][0] * m1.m[0][1] + m0.m[2][1] * m1.m[1][1] + m0.m[2][2] * m1.m[2][1];
  const float u22 = m0.m[2][0] * m1.m[0][2] + m0.m[2][1] * m1.m[1][2] + m0.m[2][2] * m1.m[2][2];

  return m3x3 {
    { u00, u01, u02 },
    { u10, u11, u12 },
    { u20, u21, u22 }
  };
}
