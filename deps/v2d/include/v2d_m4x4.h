#pragma once

#include <float.h>
#include <math.h>

#include "v2d_v4.h"

namespace v2d {

// ----------------------------------------------------------------------------
struct m4x4
{
  constexpr m4x4();
  constexpr m4x4(float d0, float d1, float d2, float d3);
  constexpr m4x4(const v4& u0, const v4& u1, const v4& u2, const v4& u3);

  constexpr static m4x4 zero();
  constexpr static m4x4 identity();
  constexpr static m4x4 diag(float d0, float d1, float d2, float d3);

  template <unsigned idx>
  constexpr v4 col() const;
  template <unsigned idx>
  constexpr v4 row() const;

  constexpr m4x4 operator-() const;
  constexpr m4x4 operator+(const m4x4& v) const;
  constexpr m4x4 operator-(const m4x4& v) const;
  constexpr m4x4 operator*(float s) const;

  constexpr m4x4 transpose() const;

  float det() const;

  float m[4][4];
};

// ----------------------------------------------------------------------------
constexpr m4x4 operator*(float s, const m4x4& m);

constexpr v4 operator*(const v4& v, const m4x4& m);
constexpr v4 operator*(const m4x4& m, const v4& v);
constexpr m4x4 operator*(const m4x4& m0, const m4x4& m1);

} // namespace v2d

// ============================================================================
inline constexpr v2d::m4x4::m4x4()
: m { { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 } }
{
}

// ----------------------------------------------------------------------------
inline constexpr v2d::m4x4::m4x4(float d0, float d1, float d2, float d3)
: m { { d0, 0, 0, 0 }, { 0, d1, 0, 0 }, { 0, 0, d2, 0 }, { 0, 0, 0, d3 } }
{
}

// ----------------------------------------------------------------------------
constexpr v2d::m4x4::m4x4(const v4& u0, const v4& u1, const v4& u2, const v4& u3)
: m {
  { u0.m[0], u0.m[1], u0.m[2], u0.m[3] },
  { u1.m[0], u1.m[1], u1.m[2], u1.m[3] },
  { u2.m[0], u2.m[1], u2.m[2], u2.m[3] },
  { u3.m[0], u3.m[1], u3.m[2], u3.m[3] }
}
{
}

// ----------------------------------------------------------------------------
constexpr v2d::m4x4 v2d::m4x4::zero()
{
  return m4x4 {};
}

// ----------------------------------------------------------------------------
constexpr v2d::m4x4 v2d::m4x4::identity()
{
  return m4x4 { 1.0f, 1.0f, 1.0f, 1.0f };
}

// ----------------------------------------------------------------------------
constexpr v2d::m4x4 v2d::m4x4::diag(float d0, float d1, float d2, float d3)
{
  return m4x4 { d0, d1, d2, d3 };
}

// ----------------------------------------------------------------------------
template <unsigned idx>
constexpr v2d::v4 v2d::m4x4::col() const
{
  static_assert(idx < 4, "index out of bounds");
  return v4 { m[0][idx], m[1][idx], m[2][idx], m[3][idx] };
}

// ----------------------------------------------------------------------------
template <unsigned idx>
constexpr v2d::v4 v2d::m4x4::row() const
{
  static_assert(idx < 4, "index out of bounds");
  return v4 { m[idx][0], m[idx][1], m[idx][2], m[idx][3] };
}

// ----------------------------------------------------------------------------
constexpr v2d::m4x4 v2d::m4x4::operator-() const
{
  return m4x4 {
    { -m[0][0], -m[0][1], -m[0][2], -m[0][3] },
    { -m[1][0], -m[1][1], -m[1][2], -m[1][3] },
    { -m[2][0], -m[2][1], -m[2][2], -m[2][3] },
    { -m[3][0], -m[3][1], -m[3][2], -m[3][3] }
  };
}

// ----------------------------------------------------------------------------
constexpr v2d::m4x4 v2d::m4x4::operator+(const m4x4& v) const
{
  return m4x4 {
    { m[0][0] + v.m[0][0], m[0][1] + v.m[0][1], m[0][2] + v.m[0][2], m[0][3] + v.m[0][3] },
    { m[1][0] + v.m[1][0], m[1][1] + v.m[1][1], m[1][2] + v.m[1][2], m[1][3] + v.m[1][3] },
    { m[2][0] + v.m[2][0], m[2][1] + v.m[2][1], m[2][2] + v.m[2][2], m[2][3] + v.m[2][3] },
    { m[3][0] + v.m[3][0], m[3][1] + v.m[3][1], m[3][2] + v.m[3][2], m[3][3] + v.m[3][3] }
  };
}

// ----------------------------------------------------------------------------
constexpr v2d::m4x4 v2d::m4x4::operator-(const m4x4& v) const
{
  return m4x4 {
    { m[0][0] - v.m[0][0], m[0][1] - v.m[0][1], m[0][2] - v.m[0][2], m[0][3] - v.m[0][3] },
    { m[1][0] - v.m[1][0], m[1][1] - v.m[1][1], m[1][2] - v.m[1][2], m[1][3] - v.m[1][3] },
    { m[2][0] - v.m[2][0], m[2][1] - v.m[2][1], m[2][2] - v.m[2][2], m[2][3] - v.m[2][3] },
    { m[3][0] - v.m[3][0], m[3][1] - v.m[3][1], m[3][2] - v.m[3][2], m[3][3] - v.m[3][3] }
  };
}

// ----------------------------------------------------------------------------
constexpr v2d::m4x4 v2d::m4x4::operator*(float s) const
{
  return m4x4 {
    { m[0][0] * s, m[0][1] * s, m[0][2] * s, m[0][3] * s },
    { m[1][0] * s, m[1][1] * s, m[1][2] * s, m[1][3] * s },
    { m[2][0] * s, m[2][1] * s, m[2][2] * s, m[2][3] * s },
    { m[3][0] * s, m[3][1] * s, m[3][2] * s, m[3][3] * s }
  };
}

// ----------------------------------------------------------------------------
constexpr v2d::m4x4 v2d::m4x4::transpose() const
{
  return m4x4 {
    { m[0][0], m[1][0], m[2][0], m[3][0] },
    { m[0][1], m[1][1], m[2][1], m[3][1] },
    { m[0][2], m[1][2], m[2][2], m[3][2] },
    { m[0][3], m[1][3], m[2][3], m[3][3] }
  };
}

// ----------------------------------------------------------------------------
inline float v2d::m4x4::det() const
{
  // clang-format off
  return
    m[0][0] * (m[1][1] * (m[2][2] * m[3][3] - m[2][3] * m[3][2]) -
               m[1][2] * (m[2][1] * m[3][3] - m[2][3] * m[3][1]) +
               m[1][3] * (m[2][1] * m[3][2] - m[2][2] * m[3][1])) -
    m[0][1] * (m[1][0] * (m[2][2] * m[3][3] - m[2][3] * m[3][2]) -
               m[1][2] * (m[2][0] * m[3][3] - m[2][3] * m[3][0]) +
               m[1][3] * (m[2][0] * m[3][2] - m[2][2] * m[3][0])) +
    m[0][2] * (m[1][0] * (m[2][1] * m[3][3] - m[2][3] * m[3][1]) -
               m[1][1] * (m[2][0] * m[3][3] - m[2][3] * m[3][0]) +
               m[1][3] * (m[2][0] * m[3][1] - m[2][1] * m[3][0])) -
    m[0][3] * (m[1][0] * (m[2][1] * m[3][2] - m[2][2] * m[3][1]) -
               m[1][1] * (m[2][0] * m[3][2] - m[2][2] * m[3][0]) +
               m[1][2] * (m[2][0] * m[3][1] - m[2][1] * m[3][0]));
  // clang-format on
}

// ----------------------------------------------------------------------------
constexpr v2d::m4x4 v2d::operator*(float s, const m4x4& m)
{
  return m * s;
}

// ----------------------------------------------------------------------------
constexpr v2d::v4 v2d::operator*(const v4& v, const m4x4& m)
{
  return v4 {
    v.m[0] * m.m[0][0] + v.m[1] * m.m[1][0] + v.m[2] * m.m[2][0] + v.m[3] * m.m[3][0],
    v.m[0] * m.m[0][1] + v.m[1] * m.m[1][1] + v.m[2] * m.m[2][1] + v.m[3] * m.m[3][1],
    v.m[0] * m.m[0][2] + v.m[1] * m.m[1][2] + v.m[2] * m.m[2][2] + v.m[3] * m.m[3][2],
    v.m[0] * m.m[0][3] + v.m[1] * m.m[1][3] + v.m[2] * m.m[2][3] + v.m[3] * m.m[3][3]
  };
}

// ----------------------------------------------------------------------------
constexpr v2d::v4 v2d::operator*(const m4x4& m, const v4& v)
{
  return v4 {
    m.m[0][0] * v.m[0] + m.m[0][1] * v.m[1] + m.m[0][2] * v.m[2] + m.m[0][3] * v.m[3],
    m.m[1][0] * v.m[0] + m.m[1][1] * v.m[1] + m.m[1][2] * v.m[2] + m.m[1][3] * v.m[3],
    m.m[2][0] * v.m[0] + m.m[2][1] * v.m[1] + m.m[2][2] * v.m[2] + m.m[2][3] * v.m[3],
    m.m[3][0] * v.m[0] + m.m[3][1] * v.m[1] + m.m[3][2] * v.m[2] + m.m[3][3] * v.m[3]
  };
}
