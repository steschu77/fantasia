#pragma once

#include <float.h>
#include <math.h>

#include "v2d_constants.h"

namespace v2d {

// ----------------------------------------------------------------------------
struct v2
{
  constexpr v2();

  constexpr v2(float const& u0, float const& u1);
  constexpr v2(const float (&u)[2]);

  constexpr float u0() const;
  constexpr float u1() const;

  constexpr v2 operator-() const;
  constexpr v2 operator+(const v2& v) const;
  constexpr v2 operator-(const v2& v) const;

  constexpr v2 operator*(float s) const;

  constexpr float operator*(const v2& v) const;

  constexpr v2 perpendicular() const;

  constexpr float length2() const;
  float length() const;
  v2 norm() const;

  v2 abs() const;

  constexpr v2 operator+=(const v2& v);
  constexpr v2 operator-=(const v2& v);

  float m[2];
};

constexpr v2 operator*(float s, const v2& v);

// dot & cross products
constexpr float dot(const v2& v0, const v2& v1);
constexpr float cross(const v2& v0, const v2& v1);
constexpr v2 cross(const v2& v, float s);
constexpr v2 cross(float s, const v2& v);
constexpr float winding(const v2& v0, const v2& v1, const v2& v2);

float distance(const v2& x0, const v2& x1);

} // namespace v2d

// ============================================================================
inline constexpr v2d::v2::v2()
: m { 0, 0 }
{
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v2::v2(float const& u0, float const& u1)
: m { u0, u1 }
{
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v2::v2(const float (&u)[2])
: m { u[0], u[1] }
{
}

// ----------------------------------------------------------------------------
inline constexpr float v2d::v2::u0() const
{
  return m[0];
}

// ----------------------------------------------------------------------------
inline constexpr float v2d::v2::u1() const
{
  return m[1];
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v2 v2d::v2::operator+(const v2d::v2& v) const
{
  return v2 { m[0] + v.m[0], m[1] + v.m[1] };
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v2 v2d::v2::operator-(const v2d::v2& v) const
{
  return v2 { m[0] - v.m[0], m[1] - v.m[1] };
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v2 v2d::v2::operator*(float s) const
{
  return v2 { m[0] * s, m[1] * s };
}

// ----------------------------------------------------------------------------
inline constexpr float v2d::v2::operator*(const v2d::v2& v) const
{
  return m[0] * v.m[0] + m[1] * v.m[1];
}

// ----------------------------------------------------------------------------
// skew vector such that dot(perpendicular, other) == cross(vec, other)
inline constexpr v2d::v2 v2d::v2::perpendicular() const
{
  return v2(-m[1], m[0]);
}

// ----------------------------------------------------------------------------
inline constexpr float v2d::v2::length2() const
{
  return m[0] * m[0] + m[1] * m[1];
}

// ----------------------------------------------------------------------------
inline float v2d::v2::length() const
{
  return sqrtf(length2());
}

// ----------------------------------------------------------------------------
inline v2d::v2 v2d::v2::norm() const
{
  const float l2 = length2();
  if (l2 < k_epsilon) {
    return v2 { 0.0f, 0.0f };
  } else {
    const float inv_l = 1.0f / sqrtf(l2);
    return v2 { inv_l * m[0], inv_l * m[1] };
  }
}

// ----------------------------------------------------------------------------
inline v2d::v2 v2d::v2::abs() const
{
  return v2 { fabsf(m[0]), fabsf(m[1]) };
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v2 v2d::v2::operator-() const
{
  return v2 { -m[0], -m[1] };
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v2 v2d::v2::operator+=(const v2d::v2& v)
{
  m[0] += v.m[0];
  m[1] += v.m[1];
  return *this;
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v2 v2d::v2::operator-=(const v2d::v2& v)
{
  m[0] -= v.m[0];
  m[1] -= v.m[1];
  return *this;
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v2 v2d::operator*(float f, const v2d::v2& v)
{
  return v2 { f * v.m[0], f * v.m[1] };
}

// ----------------------------------------------------------------------------
inline constexpr float v2d::dot(const v2& v0, const v2& v1)
{
  return v0 * v1;
}

// ----------------------------------------------------------------------------
// Two "crossed" vectors return a scalar, which is:
// * area of the parallelogram of the 2 vectors
// * magnitude of the Z vector of 3D cross product
// * signed and determines v0 rotates CW or CCW to v1
// * determinant of the 2x2 matrix built from vectors v0 and v1
inline constexpr float v2d::cross(const v2d::v2& v0, const v2d::v2& v1)
{
  return v0.m[0] * v1.m[1] - v0.m[1] * v1.m[0];
}

// ----------------------------------------------------------------------------
// forms of the "cross product" with a vector v and scalar s, both returning a vector
inline constexpr v2d::v2 v2d::cross(const v2d::v2& v, float s)
{
  return v2 { s * v.m[1], -s * v.m[0] };
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v2 v2d::cross(float s, const v2d::v2& v)
{
  return -cross(v, s);
}

// ----------------------------------------------------------------------------
// k == 0: v0, v1, v2 triplet is co-linear
// k >  0: v0, v1, v2 triplet is clockwise
// k <  0: v0, v1, v2 triplet is counter clockwise
inline constexpr float v2d::winding(const v2d::v2& v0, const v2d::v2& v1, const v2d::v2& v2)
{
  return cross(v0 - v1, v0 - v2);
}

// ----------------------------------------------------------------------------
inline float v2d::distance(const v2& x0, const v2& x1)
{
  const v2 d = x1 - x0;
  return d.length();
}
