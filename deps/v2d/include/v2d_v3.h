#pragma once

#include <float.h>
#include <math.h>

#include "v2d_constants.h"

namespace v2d {

// ----------------------------------------------------------------------------
struct v3
{
  constexpr v3();

  constexpr v3(float const& u0, float const& u1, float const& u2);
  constexpr v3(const float (&u)[3]);

  constexpr float u0() const;
  constexpr float u1() const;
  constexpr float u2() const;

  constexpr v3 operator-() const;
  constexpr v3 operator+(const v3& v) const;
  constexpr v3 operator-(const v3& v) const;

  constexpr v3 operator*(float s) const;

  constexpr float operator*(const v3& v) const;

  constexpr float length2() const;
  float length() const;
  v3 norm() const;

  v3 abs() const;

  constexpr v3 operator+=(const v3& v);
  constexpr v3 operator-=(const v3& v);

  float m[3];
};

constexpr v3 operator*(float s, const v3& v);

// dot & cross products
constexpr float dot(const v3& v0, const v3& v1);
constexpr v3 cross(const v3& v0, const v3& v1);

float distance(const v3& x0, const v3& x1);

} // namespace v2d

// ============================================================================
inline constexpr v2d::v3::v3()
: m { 0, 0, 0 }
{
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v3::v3(float const& u0, float const& u1, float const& u2)
: m { u0, u1, u2 }
{
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v3::v3(const float (&u)[3])
: m { u[0], u[1], u[2] }
{
}

// ----------------------------------------------------------------------------
inline constexpr float v2d::v3::u0() const
{
  return m[0];
}

// ----------------------------------------------------------------------------
inline constexpr float v2d::v3::u1() const
{
  return m[1];
}

// ----------------------------------------------------------------------------
inline constexpr float v2d::v3::u2() const
{
  return m[2];
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v3 v2d::v3::operator+(const v2d::v3& v) const
{
  return v3 { m[0] + v.m[0], m[1] + v.m[1], m[2] + v.m[2] };
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v3 v2d::v3::operator-(const v2d::v3& v) const
{
  return v3 { m[0] - v.m[0], m[1] - v.m[1], m[2] - v.m[2] };
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v3 v2d::v3::operator*(float s) const
{
  return v3 { m[0] * s, m[1] * s, m[2] * s };
}

// ----------------------------------------------------------------------------
inline constexpr float v2d::v3::operator*(const v2d::v3& v) const
{
  return m[0] * v.m[0] + m[1] * v.m[1] + m[2] * v.m[2];
}

// ----------------------------------------------------------------------------
inline constexpr float v2d::v3::length2() const
{
  return m[0] * m[0] + m[1] * m[1] + m[2] * m[2];
}

// ----------------------------------------------------------------------------
inline float v2d::v3::length() const
{
  return sqrtf(length2());
}

// ----------------------------------------------------------------------------
inline v2d::v3 v2d::v3::norm() const
{
  const float l2 = length2();
  if (l2 < k_epsilon) {
    return v3 { 0.0f, 0.0f, 0.0f };
  } else {
    const float inv_l = 1.0f / sqrtf(l2);
    return v3 { inv_l * m[0], inv_l * m[1], inv_l * m[2] };
  }
}

// ----------------------------------------------------------------------------
inline v2d::v3 v2d::v3::abs() const
{
  return v3 { fabsf(m[0]), fabsf(m[1]), fabsf(m[2]) };
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v3 v2d::v3::operator-() const
{
  return v3 { -m[0], -m[1], -m[2] };
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v3 v2d::v3::operator+=(const v2d::v3& v)
{
  m[0] += v.m[0];
  m[1] += v.m[1];
  m[2] += v.m[2];
  return *this;
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v3 v2d::v3::operator-=(const v2d::v3& v)
{
  m[0] -= v.m[0];
  m[1] -= v.m[1];
  m[2] -= v.m[2];
  return *this;
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v3 v2d::operator*(float f, const v2d::v3& v)
{
  return v3 { f * v.m[0], f * v.m[1], f * v.m[2] };
}

// ----------------------------------------------------------------------------
inline constexpr float v2d::dot(const v3& v0, const v3& v1)
{
  return v0 * v1;
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v3 v2d::cross(const v2d::v3& v0, const v2d::v3& v1)
{
  const float m0 = v0.m[1] * v1.m[2] - v0.m[2] * v1.m[1];
  const float m1 = v0.m[2] * v1.m[0] - v0.m[0] * v1.m[2];
  const float m2 = v0.m[0] * v1.m[1] - v0.m[1] * v1.m[0];
  return v3 { m0, m1, m2 };
}

// ----------------------------------------------------------------------------
inline float v2d::distance(const v3& x0, const v3& x1)
{
  const v3 d = x1 - x0;
  return d.length();
}
