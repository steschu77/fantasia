#pragma once

#include <float.h>
#include <math.h>

#include "v2d_constants.h"

namespace v2d {

// ----------------------------------------------------------------------------
struct v4
{
  constexpr v4();

  constexpr v4(float const& u0, float const& u1, float const& u2, float const& u3);
  constexpr v4(const float (&u)[4]);

  constexpr float u0() const;
  constexpr float u1() const;
  constexpr float u2() const;
  constexpr float u3() const;

  constexpr v4 operator-() const;
  constexpr v4 operator+(const v4& v) const;
  constexpr v4 operator-(const v4& v) const;

  constexpr v4 operator*(float s) const;

  constexpr float operator*(const v4& v) const;

  constexpr float length2() const;
  float length() const;
  v4 norm() const;

  v4 abs() const;

  constexpr v4 operator+=(const v4& v);
  constexpr v4 operator-=(const v4& v);

  float m[4];
};

constexpr v4 operator*(float s, const v4& v);

// dot product
constexpr float dot(const v4& v0, const v4& v1);

float distance(const v4& x0, const v4& x1);

} // namespace v2d

// ============================================================================
inline constexpr v2d::v4::v4()
: m { 0, 0, 0, 0 }
{
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v4::v4(float const& u0, float const& u1, float const& u2, float const& u3)
: m { u0, u1, u2, u3 }
{
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v4::v4(const float (&u)[4])
: m { u[0], u[1], u[2], u[3] }
{
}

// ----------------------------------------------------------------------------
inline constexpr float v2d::v4::u0() const
{
  return m[0];
}

// ----------------------------------------------------------------------------
inline constexpr float v2d::v4::u1() const
{
  return m[1];
}

// ----------------------------------------------------------------------------
inline constexpr float v2d::v4::u2() const
{
  return m[2];
}

// ----------------------------------------------------------------------------
inline constexpr float v2d::v4::u3() const
{
  return m[3];
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v4 v2d::v4::operator+(const v2d::v4& v) const
{
  return v4 { m[0] + v.m[0], m[1] + v.m[1], m[2] + v.m[2], m[3] + v.m[3] };
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v4 v2d::v4::operator-(const v2d::v4& v) const
{
  return v4 { m[0] - v.m[0], m[1] - v.m[1], m[2] - v.m[2], m[3] - v.m[3] };
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v4 v2d::v4::operator*(float s) const
{
  return v4 { m[0] * s, m[1] * s, m[2] * s, m[3] * s };
}

// ----------------------------------------------------------------------------
inline constexpr float v2d::v4::operator*(const v2d::v4& v) const
{
  return m[0] * v.m[0] + m[1] * v.m[1] + m[2] * v.m[2] + m[3] * v.m[3];
}

// ----------------------------------------------------------------------------
inline constexpr float v2d::v4::length2() const
{
  return m[0] * m[0] + m[1] * m[1] + m[2] * m[2] + m[3] * m[3];
}

// ----------------------------------------------------------------------------
inline float v2d::v4::length() const
{
  return sqrtf(length2());
}

// ----------------------------------------------------------------------------
inline v2d::v4 v2d::v4::norm() const
{
  const float l2 = length2();
  if (l2 < k_epsilon) {
    return v4 { 0.0f, 0.0f, 0.0f, 0.0f };
  } else {
    const float inv_l = 1.0f / sqrtf(l2);
    return v4 { inv_l * m[0], inv_l * m[1], inv_l * m[2], inv_l * m[3] };
  }
}

// ----------------------------------------------------------------------------
inline v2d::v4 v2d::v4::abs() const
{
  return v4 { fabsf(m[0]), fabsf(m[1]), fabsf(m[2]), fabsf(m[3]) };
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v4 v2d::v4::operator-() const
{
  return v4 { -m[0], -m[1], -m[2], -m[3] };
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v4 v2d::v4::operator+=(const v2d::v4& v)
{
  m[0] += v.m[0];
  m[1] += v.m[1];
  m[2] += v.m[2];
  m[3] += v.m[3];
  return *this;
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v4 v2d::v4::operator-=(const v2d::v4& v)
{
  m[0] -= v.m[0];
  m[1] -= v.m[1];
  m[2] -= v.m[2];
  m[3] -= v.m[3];
  return *this;
}

// ----------------------------------------------------------------------------
inline constexpr v2d::v4 v2d::operator*(float f, const v2d::v4& v)
{
  return v4 { f * v.m[0], f * v.m[1], f * v.m[2], f * v.m[3] };
}

// ----------------------------------------------------------------------------
inline constexpr float v2d::dot(const v4& v0, const v4& v1)
{
  return v0 * v1;
}

// ----------------------------------------------------------------------------
inline float v2d::distance(const v4& x0, const v4& x1)
{
  const v4 d = x1 - x0;
  return d.length();
}
