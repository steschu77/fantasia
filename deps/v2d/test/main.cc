#include "ccunit.h"

// ----------------------------------------------------------------------------
ccunit::Tests matrix2x2();
ccunit::Tests matrix3x3();
ccunit::Tests matrix4x4();
ccunit::Tests vector2();
ccunit::Tests vector3();
ccunit::Tests vector4();

// ----------------------------------------------------------------------------
void emplace_back(ccunit::Tests& v2, ccunit::Tests&& v1)
{
  v2.insert(
    v2.end(),
    std::make_move_iterator(v1.begin()),
    std::make_move_iterator(v1.end()));
}

// ----------------------------------------------------------------------------
int main(int argc, char* argv[])
{
  ccunit::Tests combined;
  emplace_back(combined, matrix2x2());
  emplace_back(combined, matrix3x3());
  emplace_back(combined, matrix4x4());
  emplace_back(combined, vector2());
  emplace_back(combined, vector3());
  emplace_back(combined, vector4());

  return ccunit::runTests(combined, ccunit::VerboseMode::printFailuresOnly);
}
