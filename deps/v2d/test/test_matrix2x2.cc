#include "ccunit.h"
#include "v2d.h"

// ----------------------------------------------------------------------------
constexpr v2d::m2x2 m1 { { 1.0f, 2.0f }, { 3.0f, 4.0f } };
constexpr v2d::m2x2 m2 { { 1.0f, 1.0f }, { 1.0f, 2.0f } };
constexpr v2d::m2x2 m2_inv { { 2.0f, -1.0f }, { -1.0f, 1.0f } };

constexpr v2d::v2 v0 { 1.0f, 2.0f };

// ----------------------------------------------------------------------------
void test_v2d_m2x2_zero()
{
  const v2d::m2x2 m = v2d::m2x2::zero();
  for (int i = 0; i < 2; ++i) {
    for (int j = 0; j < 2; ++j) {
      EXPECT_FLOAT_EQ(m.m[i][j], 0.0f);
    }
  }
}

// ----------------------------------------------------------------------------
void test_v2d_m2x2_identity()
{
  const v2d::m2x2 m = v2d::m2x2::identity();
  for (int i = 0; i < 2; ++i) {
    for (int j = 0; j < 2; ++j) {
      if (i == j) {
        EXPECT_FLOAT_EQ(m.m[i][j], 1.0f);
      } else {
        EXPECT_FLOAT_EQ(m.m[i][j], 0.0f);
      }
    }
  }
}

// ----------------------------------------------------------------------------
void test_v2d_m2x2_diag()
{
  const v2d::m2x2 m = v2d::m2x2::diag(1.0f, 2.0f);
  for (int i = 0; i < 2; ++i) {
    for (int j = 0; j < 2; ++j) {
      if (i == j) {
        EXPECT_FLOAT_EQ(m.m[i][j], static_cast<float>(i + 1));
      } else {
        EXPECT_FLOAT_EQ(m.m[i][j], 0.0f);
      }
    }
  }
}

// ----------------------------------------------------------------------------
void test_v2d_m2x2_rotation()
{
  const v2d::m2x2 m = v2d::m2x2::rotation(v2d::k_pi);
  for (int i = 0; i < 2; ++i) {
    for (int j = 0; j < 2; ++j) {
      if (i == j) {
        EXPECT_FLOAT_NEAR(m.m[i][j], -1.0f);
      } else {
        EXPECT_FLOAT_NEAR(m.m[i][j], 0.0f);
      }
    }
  }
}

// ----------------------------------------------------------------------------
void test_v2d_m2x2_col()
{
  const v2d::v2 v0 = m1.col<0>();
  const v2d::v2 v1 = m1.col<1>();

  EXPECT_FLOAT_EQ(v0.m[0], 1.0f);
  EXPECT_FLOAT_EQ(v0.m[1], 3.0f);
  EXPECT_FLOAT_EQ(v1.m[0], 2.0f);
  EXPECT_FLOAT_EQ(v1.m[1], 4.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_m2x2_row()
{
  const v2d::v2 v0 = m1.row<0>();
  const v2d::v2 v1 = m1.row<1>();

  EXPECT_FLOAT_EQ(v0.m[0], 1.0f);
  EXPECT_FLOAT_EQ(v0.m[1], 2.0f);
  EXPECT_FLOAT_EQ(v1.m[0], 3.0f);
  EXPECT_FLOAT_EQ(v1.m[1], 4.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_m2x2_neg()
{
  const v2d::m2x2 m = -m1;
  for (int i = 0; i < 2; ++i) {
    for (int j = 0; j < 2; ++j) {
      EXPECT_FLOAT_EQ(m.m[i][j], -m1.m[i][j]);
    }
  }
}

// ----------------------------------------------------------------------------
void test_v2d_m2x2_add()
{
  const v2d::m2x2 m = m1 + m2;
  EXPECT_FLOAT_EQ(m.m[0][0], 2.0f);
  EXPECT_FLOAT_EQ(m.m[0][1], 3.0f);
  EXPECT_FLOAT_EQ(m.m[1][0], 4.0f);
  EXPECT_FLOAT_EQ(m.m[1][1], 6.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_m2x2_sub()
{
  const v2d::m2x2 m = m1 - m2;
  EXPECT_FLOAT_EQ(m.m[0][0], 0.0f);
  EXPECT_FLOAT_EQ(m.m[0][1], 1.0f);
  EXPECT_FLOAT_EQ(m.m[1][0], 2.0f);
  EXPECT_FLOAT_EQ(m.m[1][1], 2.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_m2x2_mul()
{
  const v2d::m2x2 res0 = m1 * 2.0f;
  const v2d::m2x2 res1 = 2.0f * m1;

  EXPECT_FLOAT_EQ(res0.m[0][0], 2.0f);
  EXPECT_FLOAT_EQ(res0.m[0][1], 4.0f);
  EXPECT_FLOAT_EQ(res0.m[1][0], 6.0f);
  EXPECT_FLOAT_EQ(res0.m[1][1], 8.0f);

  EXPECT_FLOAT_EQ(res1.m[0][0], 2.0f);
  EXPECT_FLOAT_EQ(res1.m[0][1], 4.0f);
  EXPECT_FLOAT_EQ(res1.m[1][0], 6.0f);
  EXPECT_FLOAT_EQ(res1.m[1][1], 8.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_m2x2_det()
{
  const float res = m2.det();
  EXPECT_FLOAT_EQ(res, 1.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_m2x2_transpose()
{
  const v2d::m2x2 m = m2.transpose();
  for (int i = 0; i < 2; ++i) {
    for (int j = 0; j < 2; ++j) {
      EXPECT_FLOAT_EQ(m.m[i][j], m2.m[j][i]);
    }
  }
}

// ----------------------------------------------------------------------------
void test_v2d_m2x2_inverse()
{
  const v2d::m2x2 m = m2.inverse();
  for (int i = 0; i < 2; ++i) {
    for (int j = 0; j < 2; ++j) {
      EXPECT_FLOAT_EQ(m.m[i][j], m2_inv.m[i][j]);
    }
  }
}

// ----------------------------------------------------------------------------
void test_v2d_m2x2_solve()
{
  // Solve for x in m2 * x = v0
  const v2d::v2 res = m2.solve(v0);
  EXPECT_FLOAT_EQ(res.m[0], 0.0f);
  EXPECT_FLOAT_EQ(res.m[1], 1.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_m2x2_mul_matrix()
{
  const v2d::m2x2 m = m2 * m2_inv;
  for (int i = 0; i < 2; ++i) {
    for (int j = 0; j < 2; ++j) {
      if (i == j) {
        EXPECT_FLOAT_EQ(m.m[i][j], 1.0f);
      } else {
        EXPECT_FLOAT_EQ(m.m[i][j], 0.0f);
      }
    }
  }
}

// ----------------------------------------------------------------------------
void test_v2d_m2x2_mul_vector()
{
  const v2d::v2 res0 = m2 * v0;
  EXPECT_FLOAT_EQ(res0.m[0], 3.0f);
  EXPECT_FLOAT_EQ(res0.m[1], 5.0f);

  const v2d::v2 res1 = v0 * m2;
  EXPECT_FLOAT_EQ(res1.m[0], 3.0f);
  EXPECT_FLOAT_EQ(res1.m[1], 5.0f);
}

// ----------------------------------------------------------------------------
ccunit::Tests matrix2x2()
{
  ccunit::Tests tests;

  tests.push_back(std::make_unique<ccunit::Test>("m2x2(zero)", test_v2d_m2x2_zero));
  tests.push_back(std::make_unique<ccunit::Test>("m2x2(identity)", test_v2d_m2x2_identity));
  tests.push_back(std::make_unique<ccunit::Test>("m2x2(diag)", test_v2d_m2x2_diag));
  tests.push_back(std::make_unique<ccunit::Test>("m2x2(rotation)", test_v2d_m2x2_rotation));
  tests.push_back(std::make_unique<ccunit::Test>("m2x2(col)", test_v2d_m2x2_col));
  tests.push_back(std::make_unique<ccunit::Test>("m2x2(row)", test_v2d_m2x2_row));
  tests.push_back(std::make_unique<ccunit::Test>("m2x2(neg)", test_v2d_m2x2_neg));
  tests.push_back(std::make_unique<ccunit::Test>("m2x2(add)", test_v2d_m2x2_add));
  tests.push_back(std::make_unique<ccunit::Test>("m2x2(sub)", test_v2d_m2x2_sub));
  tests.push_back(std::make_unique<ccunit::Test>("m2x2(mul)", test_v2d_m2x2_mul));
  tests.push_back(std::make_unique<ccunit::Test>("m2x2(det)", test_v2d_m2x2_det));
  tests.push_back(std::make_unique<ccunit::Test>("m2x2(transpose)", test_v2d_m2x2_transpose));
  tests.push_back(std::make_unique<ccunit::Test>("m2x2(inverse)", test_v2d_m2x2_inverse));
  tests.push_back(std::make_unique<ccunit::Test>("m2x2(solve)", test_v2d_m2x2_solve));
  tests.push_back(std::make_unique<ccunit::Test>("m2x2(mxm)", test_v2d_m2x2_mul_matrix));
  tests.push_back(std::make_unique<ccunit::Test>("m2x2(vxm)", test_v2d_m2x2_mul_vector));

  return tests;
}
