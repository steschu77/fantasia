#include "ccunit.h"
#include "v2d.h"

// ----------------------------------------------------------------------------
constexpr v2d::m3x3 m2 { { 1.0f, 1.0f, 1.0f }, { 1.0f, 2.0f, 3.0f }, { 1.0f, 4.0f, 6.0f } };
constexpr v2d::m3x3 m2_inv { { 0.0f, 2.0f, -1.0f }, { 3.0f, -5.0f, 2.0f }, { -2.0f, 3.0f, -1.0f } };

constexpr v2d::v3 v0 { 1.0f, 2.0f, 3.0f };

// ----------------------------------------------------------------------------
void test_v2d_m3x3_zero()
{
  const v2d::m3x3 m = v2d::m3x3::zero();
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      EXPECT_FLOAT_EQ(m.m[i][j], 0.0f);
    }
  }
}

// ----------------------------------------------------------------------------
void test_v2d_m3x3_identity()
{
  const v2d::m3x3 m = v2d::m3x3::identity();
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      if (i == j) {
        EXPECT_FLOAT_EQ(m.m[i][j], 1.0f);
      } else {
        EXPECT_FLOAT_EQ(m.m[i][j], 0.0f);
      }
    }
  }
}

// ----------------------------------------------------------------------------
void test_v2d_m3x3_diag()
{
  const v2d::m3x3 m = v2d::m3x3::diag(1.0f, 2.0f, 3.0f);
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      if (i == j) {
        EXPECT_FLOAT_EQ(m.m[i][j], static_cast<float>(i + 1));
      } else {
        EXPECT_FLOAT_EQ(m.m[i][j], 0.0f);
      }
    }
  }
}

// ----------------------------------------------------------------------------
void test_v2d_m3x3_neg()
{
  const v2d::m3x3 m = -m2;
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      EXPECT_FLOAT_EQ(m.m[i][j], -m2.m[i][j]);
    }
  }
}

// ----------------------------------------------------------------------------
void test_v2d_m3x3_add()
{
  const v2d::m3x3 m = m2 + m2_inv;
  EXPECT_FLOAT_EQ(m.m[0][0], 1.0f);
  EXPECT_FLOAT_EQ(m.m[0][1], 3.0f);
  EXPECT_FLOAT_EQ(m.m[0][2], 0.0f);
  EXPECT_FLOAT_EQ(m.m[1][0], 4.0f);
  EXPECT_FLOAT_EQ(m.m[1][1], -3.0f);
  EXPECT_FLOAT_EQ(m.m[1][2], 5.0f);
  EXPECT_FLOAT_EQ(m.m[2][0], -1.0f);
  EXPECT_FLOAT_EQ(m.m[2][1], 7.0f);
  EXPECT_FLOAT_EQ(m.m[2][2], 5.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_m3x3_sub()
{
  const v2d::m3x3 m = m2 - m2_inv;
  EXPECT_FLOAT_EQ(m.m[0][0], 1.0f);
  EXPECT_FLOAT_EQ(m.m[0][1], -1.0f);
  EXPECT_FLOAT_EQ(m.m[0][2], 2.0f);
  EXPECT_FLOAT_EQ(m.m[1][0], -2.0f);
  EXPECT_FLOAT_EQ(m.m[1][1], 7.0f);
  EXPECT_FLOAT_EQ(m.m[1][2], 1.0f);
  EXPECT_FLOAT_EQ(m.m[2][0], 3.0f);
  EXPECT_FLOAT_EQ(m.m[2][1], 1.0f);
  EXPECT_FLOAT_EQ(m.m[2][2], 7.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_m3x3_mul()
{
  const v2d::m3x3 res0 = m2 * 2.0f;
  const v2d::m3x3 res1 = 2.0f * m2;

  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      EXPECT_FLOAT_EQ(res0.m[i][j], 2.0f * m2.m[i][j]);
      EXPECT_FLOAT_EQ(res1.m[i][j], 2.0f * m2.m[i][j]);
    }
  }
}

// ----------------------------------------------------------------------------
void test_v2d_m3x3_det()
{
  const float res = m2.det();
  EXPECT_FLOAT_EQ(res, -1.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_m3x3_transpose()
{
  const v2d::m3x3 m = m2.transpose();
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      EXPECT_FLOAT_EQ(m.m[i][j], m2.m[j][i]);
    }
  }
}

// ----------------------------------------------------------------------------
void test_v2d_m3x3_inverse()
{
  const v2d::m3x3 m = m2.inverse();
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      EXPECT_FLOAT_EQ(m.m[i][j], m2_inv.m[i][j]);
    }
  }
}

// ----------------------------------------------------------------------------
void test_v2d_m3x3_mul_matrix()
{
  const v2d::m3x3 m = m2 * m2_inv;
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      if (i == j) {
        EXPECT_FLOAT_EQ(m.m[i][j], 1.0f);
      } else {
        EXPECT_FLOAT_EQ(m.m[i][j], 0.0f);
      }
    }
  }
}

// ----------------------------------------------------------------------------
void test_v2d_m3x3_mul_vector()
{
  const v2d::v3 res0 = m2 * v0;
  EXPECT_FLOAT_EQ(res0.m[0], 6.0f);
  EXPECT_FLOAT_EQ(res0.m[1], 14.0f);
  EXPECT_FLOAT_EQ(res0.m[2], 27.0f);

  const v2d::v3 res1 = v0 * m2;
  EXPECT_FLOAT_EQ(res1.m[0], 6.0f);
  EXPECT_FLOAT_EQ(res1.m[1], 17.0f);
  EXPECT_FLOAT_EQ(res1.m[2], 25.0f);
}

// ----------------------------------------------------------------------------
ccunit::Tests matrix3x3()
{
  ccunit::Tests tests;

  tests.push_back(std::make_unique<ccunit::Test>("m3x3(zero)", test_v2d_m3x3_zero));
  tests.push_back(std::make_unique<ccunit::Test>("m3x3(identity)", test_v2d_m3x3_identity));
  tests.push_back(std::make_unique<ccunit::Test>("m3x3(diag)", test_v2d_m3x3_diag));
  tests.push_back(std::make_unique<ccunit::Test>("m3x3(neg)", test_v2d_m3x3_neg));
  tests.push_back(std::make_unique<ccunit::Test>("m3x3(add)", test_v2d_m3x3_add));
  tests.push_back(std::make_unique<ccunit::Test>("m3x3(sub)", test_v2d_m3x3_sub));
  tests.push_back(std::make_unique<ccunit::Test>("m3x3(mul)", test_v2d_m3x3_mul));
  tests.push_back(std::make_unique<ccunit::Test>("m3x3(det)", test_v2d_m3x3_det));
  tests.push_back(std::make_unique<ccunit::Test>("m3x3(inverse)", test_v2d_m3x3_inverse));
  tests.push_back(std::make_unique<ccunit::Test>("m3x3(mxm)", test_v2d_m3x3_mul_matrix));
  tests.push_back(std::make_unique<ccunit::Test>("m3x3(mxv)", test_v2d_m3x3_mul_vector));

  return tests;
}
