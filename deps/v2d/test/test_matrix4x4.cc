#include "ccunit.h"
#include "v2d.h"

// ----------------------------------------------------------------------------
void test_v2d_m4x4_identity()
{
  v2d::m4x4 m = v2d::m4x4::identity();
  for (int i = 0; i < 4; ++i) {
    for (int j = 0; j < 4; ++j) {
      if (i == j) {
        EXPECT_FLOAT_EQ(m.m[i][j], 1.0f);
      } else {
        EXPECT_FLOAT_EQ(m.m[i][j], 0.0f);
      }
    }
  }
}

// ----------------------------------------------------------------------------
void test_v2d_m4x4_diag()
{
  v2d::m4x4 m = v2d::m4x4::diag(1.0f, 2.0f, 3.0f, 4.0f);
  for (int i = 0; i < 4; ++i) {
    for (int j = 0; j < 4; ++j) {
      if (i == j) {
        EXPECT_FLOAT_EQ(m.m[i][j], static_cast<float>(i + 1));
      } else {
        EXPECT_FLOAT_EQ(m.m[i][j], 0.0f);
      }
    }
  }
}

// ----------------------------------------------------------------------------
ccunit::Tests matrix4x4()
{
  ccunit::Tests tests;

  tests.push_back(std::make_unique<ccunit::Test>("m4x4(identity)", test_v2d_m4x4_identity));
  tests.push_back(std::make_unique<ccunit::Test>("m4x4(diag)", test_v2d_m4x4_diag));

  return tests;
}
