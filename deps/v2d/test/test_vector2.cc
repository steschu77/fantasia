#include "ccunit.h"
#include "v2d.h"

// ----------------------------------------------------------------------------
constexpr v2d::v2 null { 0.0f, 0.0f };
constexpr v2d::v2 v0 { 1.0f, 2.0f };
constexpr v2d::v2 v1 { 2.0f, 3.0f };
constexpr v2d::v2 v2 { 3.0f, 4.0f };
constexpr v2d::v2 v3 { 2.0f, 1.0f };

// ----------------------------------------------------------------------------
void test_v2d_v2_add()
{
  const v2d::v2 res = v0 + v1;
  EXPECT_FLOAT_EQ(res.u0(), 3.0f);
  EXPECT_FLOAT_EQ(res.u1(), 5.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_v2_sub()
{
  const v2d::v2 res = v0 - v1;
  EXPECT_FLOAT_EQ(res.u0(), -1.0f);
  EXPECT_FLOAT_EQ(res.u1(), -1.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_v2_mul()
{
  const v2d::v2 res0 = v0 * 2.0f;
  EXPECT_FLOAT_EQ(res0.u0(), 2.0f);
  EXPECT_FLOAT_EQ(res0.u1(), 4.0f);

  const v2d::v2 res1 = 2.0f * v0;
  EXPECT_FLOAT_EQ(res1.u0(), 2.0f);
  EXPECT_FLOAT_EQ(res1.u1(), 4.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_v2_dot()
{
  const float res = v0 * v1;
  EXPECT_FLOAT_EQ(res, 8.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_v2_cross()
{
  const float res0 = v2d::cross(v0, v1);
  EXPECT_FLOAT_EQ(res0, -1.0f);

  const float res1 = v2d::cross(v1, v0);
  EXPECT_FLOAT_EQ(res1, 1.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_v2_scalar_cross()
{
  constexpr float s = 3.0f;

  const v2d::v2 res0 = v2d::cross(s, v0);
  const v2d::v2 res1 = v2d::cross(v0, s);

  EXPECT_FLOAT_EQ(res0.u0(), -6.0f);
  EXPECT_FLOAT_EQ(res0.u1(), 3.0f);
  EXPECT_FLOAT_EQ(res1.u0(), 6.0f);
  EXPECT_FLOAT_EQ(res1.u1(), -3.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_v2_length()
{
  const float res = v2.length();
  EXPECT_FLOAT_EQ(res, 5.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_v2_normalize()
{
  const v2d::v2 res = v2.norm();
  EXPECT_FLOAT_EQ(res.u0(), 0.6f);
  EXPECT_FLOAT_EQ(res.u1(), 0.8f);
}

// ----------------------------------------------------------------------------
void test_v2d_v2_normalize_null()
{
  const v2d::v2 res = null.norm();
  EXPECT_FLOAT_EQ(res.u0(), 0.0f);
  EXPECT_FLOAT_EQ(res.u1(), 0.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_v2_winding()
{
  const float res0 = v2d::winding(v0, v1, v2);
  EXPECT_FLOAT_EQ(res0, 0.0f); // colinear

  const float res1 = v2d::winding(v0, v1, v3);
  EXPECT_LT(res1, 0.0f); // CCW

  const float res2 = v2d::winding(v3, v1, v0);
  EXPECT_GT(res2, 0.0f); // CW
}

// ----------------------------------------------------------------------------
ccunit::Tests vector2()
{
  ccunit::Tests tests;

  tests.push_back(std::make_unique<ccunit::Test>("v2(add)", test_v2d_v2_add));
  tests.push_back(std::make_unique<ccunit::Test>("v2(sub)", test_v2d_v2_sub));
  tests.push_back(std::make_unique<ccunit::Test>("v2(mul)", test_v2d_v2_mul));
  tests.push_back(std::make_unique<ccunit::Test>("v2(dot)", test_v2d_v2_dot));
  tests.push_back(std::make_unique<ccunit::Test>("v2(cross)", test_v2d_v2_cross));
  tests.push_back(std::make_unique<ccunit::Test>("v2(scalar_cross)", test_v2d_v2_scalar_cross));
  tests.push_back(std::make_unique<ccunit::Test>("v2(length)", test_v2d_v2_length));
  tests.push_back(std::make_unique<ccunit::Test>("v2(normalize)", test_v2d_v2_normalize));
  tests.push_back(std::make_unique<ccunit::Test>("v2(normalize_0)", test_v2d_v2_normalize_null));
  tests.push_back(std::make_unique<ccunit::Test>("v2(winding)", test_v2d_v2_winding));

  return tests;
}
