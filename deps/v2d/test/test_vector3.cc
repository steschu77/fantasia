#include "ccunit.h"
#include "v2d.h"

// ----------------------------------------------------------------------------
constexpr v2d::v3 null { 0.0f, 0.0f, 0.0f };
constexpr v2d::v3 v0 { 1.0f, 2.0f, 3.0f };
constexpr v2d::v3 v1 { 2.0f, 3.0f, 4.0f };
constexpr v2d::v3 v2 { 1.0f, 2.0f, 2.0f };
constexpr v2d::v3 v3 { 3.0f, 4.0f, 0.0f };

// ----------------------------------------------------------------------------
void test_v2d_v3_add()
{
  const v2d::v3 res = v0 + v1;
  EXPECT_FLOAT_EQ(res.u0(), 3.0f);
  EXPECT_FLOAT_EQ(res.u1(), 5.0f);
  EXPECT_FLOAT_EQ(res.u2(), 7.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_v3_sub()
{
  const v2d::v3 res = v0 - v1;
  EXPECT_FLOAT_EQ(res.u0(), -1.0f);
  EXPECT_FLOAT_EQ(res.u1(), -1.0f);
  EXPECT_FLOAT_EQ(res.u2(), -1.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_v3_mul()
{
  const v2d::v3 res0 = v0 * 2.0f;
  EXPECT_FLOAT_EQ(res0.u0(), 2.0f);
  EXPECT_FLOAT_EQ(res0.u1(), 4.0f);
  EXPECT_FLOAT_EQ(res0.u2(), 6.0f);

  const v2d::v3 res1 = 2.0f * v0;
  EXPECT_FLOAT_EQ(res1.u0(), 2.0f);
  EXPECT_FLOAT_EQ(res1.u1(), 4.0f);
  EXPECT_FLOAT_EQ(res1.u2(), 6.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_v3_dot()
{
  const float res = v0 * v1;
  EXPECT_FLOAT_EQ(res, 20.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_v3_cross()
{
  const v2d::v3 res = v2d::cross(v0, v1);
  EXPECT_FLOAT_EQ(res.u0(), -1.0f);
  EXPECT_FLOAT_EQ(res.u1(), 2.0f);
  EXPECT_FLOAT_EQ(res.u2(), -1.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_v3_length()
{
  EXPECT_FLOAT_EQ(v2.length(), 3.0f);
  EXPECT_FLOAT_EQ(v3.length(), 5.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_v3_normalize()
{
  const v2d::v3 res = v3.norm();
  EXPECT_FLOAT_EQ(res.u0(), 0.6f);
  EXPECT_FLOAT_EQ(res.u1(), 0.8f);
  EXPECT_FLOAT_EQ(res.u2(), 0.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_v3_normalize_null()
{
  const v2d::v3 res = null.norm();
  EXPECT_FLOAT_EQ(res.u0(), 0.0f);
  EXPECT_FLOAT_EQ(res.u1(), 0.0f);
  EXPECT_FLOAT_EQ(res.u2(), 0.0f);
}

// ----------------------------------------------------------------------------
ccunit::Tests vector3()
{
  ccunit::Tests tests;

  tests.push_back(std::make_unique<ccunit::Test>("v3(add)", test_v2d_v3_add));
  tests.push_back(std::make_unique<ccunit::Test>("v3(sub)", test_v2d_v3_sub));
  tests.push_back(std::make_unique<ccunit::Test>("v3(mul)", test_v2d_v3_mul));
  tests.push_back(std::make_unique<ccunit::Test>("v3(dot)", test_v2d_v3_dot));
  tests.push_back(std::make_unique<ccunit::Test>("v3(cross)", test_v2d_v3_cross));
  tests.push_back(std::make_unique<ccunit::Test>("v3(length)", test_v2d_v3_length));
  tests.push_back(std::make_unique<ccunit::Test>("v3(normalize)", test_v2d_v3_normalize));
  tests.push_back(std::make_unique<ccunit::Test>("v3(normalize_0)", test_v2d_v3_normalize_null));

  return tests;
}
