#include "ccunit.h"
#include "v2d.h"

// ----------------------------------------------------------------------------
constexpr v2d::v4 null { 0.0f, 0.0f, 0.0f, 0.0f };
constexpr v2d::v4 v0 { 1.0f, 2.0f, 3.0f, 4.0f };
constexpr v2d::v4 v1 { 2.0f, 3.0f, 4.0f, 5.0f };
constexpr v2d::v4 v2 { 1.0f, 1.0f, 3.0f, 5.0f };
constexpr v2d::v4 v3 { 2.0f, 2.0f, 2.0f, 2.0f };

// ----------------------------------------------------------------------------
void test_v2d_v4_add()
{
  const v2d::v4 res = v0 + v1;
  EXPECT_FLOAT_EQ(res.u0(), 3.0f);
  EXPECT_FLOAT_EQ(res.u1(), 5.0f);
  EXPECT_FLOAT_EQ(res.u2(), 7.0f);
  EXPECT_FLOAT_EQ(res.u3(), 9.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_v4_sub()
{
  const v2d::v4 res = v0 - v1;
  EXPECT_FLOAT_EQ(res.u0(), -1.0f);
  EXPECT_FLOAT_EQ(res.u1(), -1.0f);
  EXPECT_FLOAT_EQ(res.u2(), -1.0f);
  EXPECT_FLOAT_EQ(res.u3(), -1.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_v4_mul()
{
  const v2d::v4 res0 = v0 * 2.0f;
  EXPECT_FLOAT_EQ(res0.u0(), 2.0f);
  EXPECT_FLOAT_EQ(res0.u1(), 4.0f);
  EXPECT_FLOAT_EQ(res0.u2(), 6.0f);
  EXPECT_FLOAT_EQ(res0.u3(), 8.0f);

  const v2d::v4 res1 = 2.0f * v0;
  EXPECT_FLOAT_EQ(res1.u0(), 2.0f);
  EXPECT_FLOAT_EQ(res1.u1(), 4.0f);
  EXPECT_FLOAT_EQ(res1.u2(), 6.0f);
  EXPECT_FLOAT_EQ(res1.u3(), 8.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_v4_dot()
{
  const float res = v0 * v1;
  EXPECT_FLOAT_EQ(res, 40.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_v4_length()
{
  EXPECT_FLOAT_EQ(v2.length(), 6.0f);
  EXPECT_FLOAT_EQ(v3.length(), 4.0f);
}

// ----------------------------------------------------------------------------
void test_v2d_v4_normalize()
{
  const v2d::v4 res = v3.norm();
  EXPECT_FLOAT_EQ(res.u0(), 0.5f);
  EXPECT_FLOAT_EQ(res.u1(), 0.5f);
  EXPECT_FLOAT_EQ(res.u2(), 0.5f);
  EXPECT_FLOAT_EQ(res.u3(), 0.5f);
}

// ----------------------------------------------------------------------------
void test_v2d_v4_normalize_null()
{
  const v2d::v4 res = null.norm();
  EXPECT_FLOAT_EQ(res.u0(), 0.0f);
  EXPECT_FLOAT_EQ(res.u1(), 0.0f);
  EXPECT_FLOAT_EQ(res.u2(), 0.0f);
  EXPECT_FLOAT_EQ(res.u3(), 0.0f);
}

// ----------------------------------------------------------------------------
ccunit::Tests vector4()
{
  ccunit::Tests tests;

  tests.push_back(std::make_unique<ccunit::Test>("v4(add)", test_v2d_v4_add));
  tests.push_back(std::make_unique<ccunit::Test>("v4(sub)", test_v2d_v4_sub));
  tests.push_back(std::make_unique<ccunit::Test>("v4(mul)", test_v2d_v4_mul));
  tests.push_back(std::make_unique<ccunit::Test>("v4(dot)", test_v2d_v4_dot));
  tests.push_back(std::make_unique<ccunit::Test>("v4(length)", test_v2d_v4_length));
  tests.push_back(std::make_unique<ccunit::Test>("v4(normalize)", test_v2d_v4_normalize));
  tests.push_back(std::make_unique<ccunit::Test>("v4(normalize_0)", test_v2d_v4_normalize_null));

  return tests;
}
