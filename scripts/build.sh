set -e
source /etc/profile
time -p cmake -B_ninja -S. -GNinja
time -p cmake --build _ninja
time -p ctest --rerun-failed --output-on-failure --test-dir _ninja
ccache -s
