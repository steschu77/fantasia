find deps -type f -name "*.cc" -exec clang-format -i {} +
find src -type f -name "*.cc" -exec clang-format -i {} +
find test -type f -name "*.cc" -exec clang-format -i {} +

find deps -type f -name "*.h" -exec clang-format -i {} +
find src -type f -name "*.h" -exec clang-format -i {} +
find test -type f -name "*.h" -exec clang-format -i {} +
