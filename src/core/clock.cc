#include "clock.h"
#include "fantasia.h"

#include <chrono>
#include <thread>

// ----------------------------------------------------------------------------
fantasia::Clock::Clock()
{
  _t0 = std::chrono::steady_clock::now();
}

// ----------------------------------------------------------------------------
uint64_t fantasia::Clock::now() const
{
  const auto t1 = std::chrono::steady_clock::now();
  const auto dt = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - _t0);
  return dt.count();
}

// ----------------------------------------------------------------------------
void fantasia::Clock::sleep(uint64_t dt)
{
  std::this_thread::sleep_for(std::chrono::milliseconds(dt));
}

// ----------------------------------------------------------------------------
fantasia::IClock& fantasia::clock()
{
  static Clock instance;
  return instance;
}
