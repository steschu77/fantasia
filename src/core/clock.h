#include "fantasia.h"
#include <chrono>

namespace fantasia {

// ----------------------------------------------------------------------------
class Clock : public IClock
{
public:
  Clock();

  uint64_t now() const override;
  void sleep(uint64_t dt) override;

private:
  std::chrono::steady_clock::time_point _t0;
};

}
