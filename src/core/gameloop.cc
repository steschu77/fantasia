#include "fantasia.h"
#include <algorithm>

// ----------------------------------------------------------------------------
int fantasia::gameloop(IGame& game, IClock& clock)
{
  // game loop: https://gameprogrammingpatterns.com/game-loop.html
  const uint64_t dtUpdate = game.getUpdateTime();

  uint64_t tNow = clock.now();
  uint64_t tLag = 0;
  int res = 0;

  for (;;) {

    // slow machines: reduce rendering calls, i.e. multiple updates per frame
    const uint64_t cUpdatesNeeded = std::max(UINT64_C(1), tLag / dtUpdate);
    tLag = tLag % dtUpdate;

    // process system messages and input
    if ((res = game.preprocess(tNow)) != 0) {
      return res;
    }

    // clamp number of updates to avoid spiral of death (otherwise the next loop will be late again)
    const uint64_t cUpdates = std::min(cUpdatesNeeded, UINT64_C(4));
    for (uint64_t i = 0; i < cUpdates; ++i) {
      if ((res = game.update()) != 0) {
        return res;
      }
    }

    if ((res = game.render()) != 0) {
      return res;
    }

    uint64_t tPrevious = tNow;
    tNow = clock.now();

    uint64_t tElapsed = tNow - tPrevious;

    // fast machines: sleep until tPrevious + dtUpdate
    while (dtUpdate > tElapsed) {

      uint64_t tSleep = dtUpdate - tElapsed;
      clock.sleep(tSleep);

      tNow = clock.now();
      tElapsed = tNow - tPrevious;
    }

    tLag += tElapsed;
  }
}
