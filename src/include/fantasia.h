#pragma once

#include <inttypes.h>
#include <memory>

namespace fantasia {

// ----------------------------------------------------------------------------
struct IClock
{
  virtual ~IClock() = default;
  virtual uint64_t now() const = 0;
  virtual void sleep(uint64_t dt) = 0;
};

// ----------------------------------------------------------------------------
struct IGame
{
  virtual ~IGame() = default;

  virtual uint64_t getUpdateTime() const = 0;
  virtual int preprocess(uint64_t tNow) = 0;
  virtual int update() = 0;
  virtual int render() = 0;
};

// ----------------------------------------------------------------------------
struct IRenderer
{
  virtual ~IRenderer() = default;

  virtual void resize(unsigned cx, unsigned cy) = 0;
  virtual void setZoom(float zoom) = 0;

  virtual void runGBuffer() = 0;
  virtual void runShading() = 0;
};

using IRendererPtr = std::unique_ptr<IRenderer>;

// ----------------------------------------------------------------------------
struct IEngine
{
  virtual ~IEngine() = default;
};

// ----------------------------------------------------------------------------
IClock& clock();
IRendererPtr rendererOpenGL();

int gameloop(IGame& game, IClock& clock);

}
