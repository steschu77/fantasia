#pragma once

#if defined(_MSC_VER)
#include "../opengl/glew_win32.h"
#else
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
#endif
