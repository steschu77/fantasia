#include "deferred_renderer.h"

// clang-format off
// ----------------------------------------------------------------------------
static const char* vsPassthrough =
    "#version 330\n"
    "layout (location = 0) in vec2 position;\n"
    "layout (location = 1) in vec2 texcoord;\n"
    "out vec2 Texcoord;\n"
    "\n"
    "void main() {\n"
    "  Texcoord = texcoord;\n"
    "  gl_Position = vec4(position, 0, 1);\n"
    "}";

// ----------------------------------------------------------------------------
static const char* fsTexture =
    "#version 330\n"
    "in vec2 Texcoord;\n"
    "layout (location = 0) out vec4 outColor;\n"
    "uniform sampler2D tex;\n"
    "void main() {\n"
    "  outColor = texture(tex, Texcoord.st);"
    "}";

// ----------------------------------------------------------------------------
static const char* fsShading =
    "#version 330\n"
    "in vec2 Texcoord;\n"
    "layout (location = 0) out vec4 outColor;\n"
    "\n"
    "uniform sampler2D texColor;\n"
    "uniform sampler2D texNormal;\n"
    "uniform sampler2D texAO;\n"
    "\n"
    "float rand(vec2 n) {\n"
	  "  return fract(sin(dot(n, vec2(12.9898, 4.1414))) * 43758.5453);\n"
    "}\n"
    "float noise(vec2 n) {\n"
	  "  const vec2 d = vec2(0.0, 1.0);\n"
    "  vec2 b = floor(n), f = smoothstep(vec2(0.0), vec2(1.0), fract(n));\n"
    "  float m0 = mix(rand(b), rand(b + d.yx), f.x);"
    "  float m1 = mix(rand(b + d.xy), rand(b + d.yy), f.x);\n"
	  "  return mix(m0, m1, f.y);\n"
    "}\n"
    "\n"
    "void main() {\n"
    "  float n0 = noise( gl_FragCoord.xy) - 0.5;\n"
    "  float n1 = noise(-gl_FragCoord.xy) - 0.5;\n"
    "  vec2 ns2 = 0.005f * vec2(n0, n1);"
    "  vec4 ns4 = 0.2f * vec4(n1, n1, n1, 0.0);"
    "  outColor = texture(texColor, Texcoord.st + ns2) + ns4;\n"
    "}";
// clang-format on

// ----------------------------------------------------------------------------
fantasia::IRendererPtr fantasia::rendererOpenGL()
{
  return std::make_unique<DeferredRenderer>();
}

// ----------------------------------------------------------------------------
fantasia::DeferredRenderer::DeferredRenderer()
: _gbuffer(vsPassthrough, fsTexture)
, _shading(vsPassthrough, fsShading)
{
  glGenFramebuffers(1, &_glFBO);
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, _glFBO);

  constexpr unsigned cBuffers = 3;
  static const GLint fmt[cBuffers] = { GL_RGBA8, GL_RGBA16F, GL_RGBA16F };
  static const GLenum type[cBuffers] = { GL_UNSIGNED_BYTE, GL_FLOAT, GL_FLOAT };

  glGenTextures(cBuffers, &_glTexFb[0]);
  for (unsigned i = 0; i < cBuffers; ++i) {
    glBindTexture(GL_TEXTURE_2D, _glTexFb[i]);
    glTexImage2D(GL_TEXTURE_2D, 0, fmt[i], 1280, 720, 0, GL_RGB, type[i], 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glFramebufferTexture2D(
      GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, _glTexFb[i], 0);
    glBindTexture(GL_TEXTURE_2D, 0);
  }

  // specify that we can render to the attachment.
  GLenum tgts[cBuffers] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
  glDrawBuffers(cBuffers, tgts);

  // make sure nothing went wrong:
  GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
  if (status != GL_FRAMEBUFFER_COMPLETE) {
    // printf("Framebuffer not complete. Status: %d", status);
  }

  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  // clang-format off
  static const vert_pt_uv_t verts[] = {
    { {-1.0f,-1.0f }, { 0.0f, 0.0f } },
    { {-1.0f, 1.0f }, { 0.0f, 1.0f } },
    { { 1.0f, 1.0f }, { 1.0f, 1.0f } },
    { { 1.0f,-1.0f }, { 1.0f, 0.0f } }
  };
  // clang-format on

  _viewbox = _shading.createMesh(verts, 4);

  // only temporary for testing
  // clang-format off
  static const float TexturePixels[] = {
    1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
  };
  // clang-format on

  glGenTextures(1, &_glTex);
  glBindTexture(GL_TEXTURE_2D, _glTex);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 2, 2, 0, GL_RGB, GL_FLOAT, TexturePixels);
}

// ----------------------------------------------------------------------------
fantasia::DeferredRenderer::~DeferredRenderer()
{
}

// ----------------------------------------------------------------------------
void fantasia::DeferredRenderer::resize(unsigned cx, unsigned cy)
{
  _cx = cx;
  _cy = cy;
  _updateProjection();
}

// ----------------------------------------------------------------------------
void fantasia::DeferredRenderer::setZoom(float zoom)
{
  _zoom = zoom;
  _updateProjection();
}

// ----------------------------------------------------------------------------
void fantasia::DeferredRenderer::runGBuffer()
{
  glBindFramebuffer(GL_FRAMEBUFFER, _glFBO);
  glDisable(GL_DEPTH_TEST);

  glClearColor(0.1f, 0.4f, 0.5f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT);

  _gbuffer.useShader(0.0f);

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, _glTex);
  _viewbox->draw(GL_TRIANGLE_FAN);
}

// ----------------------------------------------------------------------------
void fantasia::DeferredRenderer::runShading()
{
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glDisable(GL_DEPTH_TEST);

  _shading.useShader(0.0f);
  _shading.setUniform("texColor", 0);
  _shading.setUniform("texNormal", 1);
  _shading.setUniform("texAO", 2);

  for (unsigned i = 0; i < 3; ++i) {
    glActiveTexture(GL_TEXTURE0 + i);
    glBindTexture(GL_TEXTURE_2D, _glTexFb[i]);
  }

  _viewbox->draw(GL_TRIANGLE_FAN);
}

// ----------------------------------------------------------------------------
void fantasia::DeferredRenderer::_updateProjection()
{
  glViewport(0, 0, _cx, _cy);

  _aspect = static_cast<float>(_cx) / static_cast<float>(_cy);
  const float fx = 1.0f / (_zoom * _aspect);
  const float fy = 1.0f / _zoom;

  _proj = v2d::m4x4::diag(fx, fy, 1.0f, 1.0f);
}
