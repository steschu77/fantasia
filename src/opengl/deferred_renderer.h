#include "fantasia.h"

#include "gl_shader_pt_uv.h"
#include "opengl.h"
#include "v2d.h"

namespace fantasia {

// ----------------------------------------------------------------------------
class DeferredRenderer : public fantasia::IRenderer
{
public:
  DeferredRenderer();
  ~DeferredRenderer();

  // non-copyable
  DeferredRenderer(const DeferredRenderer&) = delete;
  DeferredRenderer& operator=(const DeferredRenderer&) = delete;

  void resize(unsigned cx, unsigned cy) override;
  void setZoom(float zoom) override;

  void runGBuffer() override;
  void runShading() override;

protected:
  GLShader_Pt_UV _gbuffer;
  GLShader_Pt_UV _shading;
  GLMeshPtr _viewbox;

  GLuint _glTexFb[3];
  GLuint _glFBO = 0;

  unsigned _cx = 0;
  unsigned _cy = 0;
  float _zoom = 10.0f;
  float _aspect = 1.0f;

  v2d::m4x4 _proj;
  void _updateProjection();

  GLuint _glTex = 0;
};

} // namespace fantasia
