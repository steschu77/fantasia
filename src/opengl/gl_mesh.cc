#include "gl_mesh.h"

// ----------------------------------------------------------------------------
fantasia::GLMesh::GLMesh(const void* verts, GLuint cVert, GLuint count)
: _count(count)
{
  glGenVertexArrays(1, &_iVAO);
  glBindVertexArray(_iVAO);

  glGenBuffers(1, &_iVBO);
  glBindBuffer(GL_ARRAY_BUFFER, _iVBO);
  glBufferData(GL_ARRAY_BUFFER, cVert * count, verts, GL_STATIC_DRAW);
}

// ----------------------------------------------------------------------------
fantasia::GLMesh::~GLMesh()
{
  glDeleteBuffers(1, &_iVBO);
  glDeleteVertexArrays(1, &_iVAO);
}

// ----------------------------------------------------------------------------
void fantasia::GLMesh::vertexAttrib(GLuint iAttrib, GLint size, GLenum type,
  GLboolean normalized, GLsizei stride, const GLvoid* pointer) const
{
  glBindVertexArray(_iVAO);
  glEnableVertexAttribArray(iAttrib);
  glVertexAttribPointer(iAttrib, size, type, normalized, stride, pointer);
}

// ----------------------------------------------------------------------------
void fantasia::GLMesh::draw(GLuint mode) const
{
  glBindVertexArray(_iVAO);
  glDrawArrays(mode, 0, _count);
}
