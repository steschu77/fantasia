#include "opengl.h"
#include "v2d.h"

#include <memory>

namespace fantasia {

// ----------------------------------------------------------------------------
class GLMesh
{
public:
  GLMesh(const void* verts, GLuint cVert, GLuint count);
  ~GLMesh();

  // non-copyable
  GLMesh(const GLMesh&) = delete;
  GLMesh& operator=(const GLMesh&) = delete;

  void vertexAttrib(GLuint id, GLint size, GLenum type,
    GLboolean normalized, GLsizei stride, const GLvoid* pointer) const;

  void draw(GLuint mode) const;

private:
  GLuint _iVAO = 0;
  GLuint _iVBO = 0;
  GLuint _count = 0;
};

using GLMeshPtr = std::unique_ptr<GLMesh>;

} // namespace fantasia
