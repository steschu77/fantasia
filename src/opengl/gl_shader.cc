#include "gl_shader.h"
#include <stdio.h>

// ----------------------------------------------------------------------------
static GLuint createShader(GLuint shader, const char* code)
{
  GLuint iShader = glCreateShader(shader);
  glShaderSource(iShader, 1, &code, nullptr);
  glCompileShader(iShader);

  char info[1024];
  GLint maxLength = 0;
  glGetShaderInfoLog(iShader, 1024, &maxLength, info);

  if (maxLength != 0) {
    printf("shader error: %s\n", info);
  }

  return iShader;
}

// ----------------------------------------------------------------------------
static GLuint createProgram(const char* vsCode, const char* fsCode)
{
  GLuint iVS = createShader(GL_VERTEX_SHADER, vsCode);
  GLuint iFS = createShader(GL_FRAGMENT_SHADER, fsCode);

  GLuint iProgram = glCreateProgram();
  glAttachShader(iProgram, iVS);
  glAttachShader(iProgram, iFS);
  glLinkProgram(iProgram);

  return iProgram;
}

// ----------------------------------------------------------------------------
fantasia::GLShader::GLShader(const char* vsCode, const char* fsCode)
{
  _iProgram = createProgram(vsCode, fsCode);
}

// ----------------------------------------------------------------------------
fantasia::GLShader::~GLShader()
{
  glDeleteProgram(_iProgram);
}

// ----------------------------------------------------------------------------
void fantasia::GLShader::useShader(float t)
{
  glUseProgram(_iProgram);
}

// ----------------------------------------------------------------------------
void fantasia::GLShader::setUniform(const char* name, GLint value)
{
  GLint loc = glGetUniformLocation(_iProgram, name);
  glUniform1i(loc, value);
}

// ----------------------------------------------------------------------------
void fantasia::GLShader::setUniform(const char* name, GLfloat value)
{
  GLint loc = glGetUniformLocation(_iProgram, name);
  glUniform1f(loc, value);
}
