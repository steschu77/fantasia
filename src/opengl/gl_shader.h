#include "opengl.h"
#include "v2d.h"

namespace fantasia {

// ----------------------------------------------------------------------------
class GLShader
{
public:
  GLShader(const char* vsCode, const char* fsCode);
  ~GLShader();

  // non-copyable
  GLShader(const GLShader&) = delete;
  GLShader& operator=(const GLShader&) = delete;

  virtual void useShader(float t);
  virtual void setUniform(const char* name, GLint value);
  virtual void setUniform(const char* name, GLfloat value);

protected:
  GLuint _iProgram = 0;
};

} // namespace fantasia
