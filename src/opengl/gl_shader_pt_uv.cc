#include "gl_shader_pt_uv.h"

// ----------------------------------------------------------------------------
fantasia::GLShader_Pt_UV::GLShader_Pt_UV(const char* vsCode, const char* fsCode)
: GLShader(vsCode, fsCode)
{
  _iPosition = glGetAttribLocation(_iProgram, "position");
  _iTexcoord = glGetAttribLocation(_iProgram, "texcoord");
}

// ----------------------------------------------------------------------------
fantasia::GLShader_Pt_UV::~GLShader_Pt_UV()
{
}

// ----------------------------------------------------------------------------
fantasia::GLMeshPtr fantasia::GLShader_Pt_UV::createMesh(
  const vert_pt_uv_t* verts, unsigned count)
{
  if (count == 0) {
    return nullptr;
  }

  constexpr GLuint cVert = sizeof(vert_pt_uv_t);
  GLMeshPtr mesh = std::make_unique<GLMesh>(verts, cVert, count);

  mesh->vertexAttrib(_iPosition, 2, GL_FLOAT, GL_FALSE, cVert, 0);
  mesh->vertexAttrib(_iTexcoord, 2, GL_FLOAT, GL_FALSE, cVert, (void*)(2 * sizeof(float)));

  return mesh;
}

// ----------------------------------------------------------------------------
void fantasia::GLShader_Pt_UV::useShader(float t)
{
  GLShader::useShader(t);

  glEnableVertexAttribArray(_iPosition);
  glEnableVertexAttribArray(_iTexcoord);
}

// ----------------------------------------------------------------------------
void fantasia::GLShader_Pt_UV::renderMeshAt(const GLMesh& m, const v2d::v2& pos)
{
  m.draw(GL_TRIANGLE_STRIP);
}
