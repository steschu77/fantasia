#include "gl_mesh.h"
#include "gl_shader.h"
#include "opengl.h"
#include "v2d.h"

namespace fantasia {

// ----------------------------------------------------------------------------
struct vert_pt_uv_t
{
  v2d::v2 pt;
  v2d::v2 uv;
};

// ----------------------------------------------------------------------------
class GLShader_Pt_UV : public GLShader
{
public:
  GLShader_Pt_UV(const char* vsCode, const char* fsCode);
  ~GLShader_Pt_UV();

  void useShader(float t) override;

  GLMeshPtr createMesh(const vert_pt_uv_t* verts, unsigned count);

  void renderMeshAt(const GLMesh&, const v2d::v2& pos);

private:
  GLuint _iPosition = 0;
  GLuint _iTexcoord = 0;
};

} // namespace fantasia
