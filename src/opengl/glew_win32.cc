#include "glew_win32.h"

// clang-format off

// ----------------------------------------------------------------------------
// OpenGL 1.3
typedef void (* PFNGLACTIVETEXTUREPROC) (GLenum texture);

// OpenGL 1.5
typedef void (* PFNGLBINDBUFFERPROC) (GLenum target, GLuint buffer);
typedef void (* PFNGLBUFFERDATAPROC) (GLenum target, GLsizeiptr size, const GLvoid* data, GLenum usage);
typedef void (* PFNGLDELETEBUFFERSPROC) (GLsizei n, const GLuint* buffers);
typedef void (* PFNGLGENBUFFERSPROC) (GLsizei n, GLuint* buffers);

// OpenGL 2.0
typedef void (* PFNGLATTACHSHADERPROC) (GLuint program, GLuint shader);
typedef void (* PFNGLBINDATTRIBLOCATIONPROC) (GLuint program, GLuint index, const GLchar* name);
typedef void (* PFNGLBLENDEQUATIONSEPARATEPROC) (GLenum, GLenum);
typedef void (* PFNGLCOMPILESHADERPROC) (GLuint shader);
typedef GLuint (* PFNGLCREATEPROGRAMPROC) (void);
typedef GLuint (* PFNGLCREATESHADERPROC) (GLenum type);
typedef void (* PFNGLDELETEPROGRAMPROC) (GLuint program);
typedef void (* PFNGLDELETESHADERPROC) (GLuint shader);
typedef void (* PFNGLDETACHSHADERPROC) (GLuint program, GLuint shader);
typedef void (* PFNGLDISABLEVERTEXATTRIBARRAYPROC) (GLuint);
typedef void (* PFNGLDRAWBUFFERSPROC) (GLsizei n, const GLenum* bufs);
typedef void (* PFNGLENABLEVERTEXATTRIBARRAYPROC) (GLuint);
typedef GLint (* PFNGLGETATTRIBLOCATIONPROC) (GLuint program, const GLchar* name);
typedef void (* PFNGLGETPROGRAMINFOLOGPROC) (GLuint program, GLsizei bufSize, GLsizei* length, GLchar* infoLog);
typedef void (* PFNGLGETSHADERINFOLOGPROC) (GLuint shader, GLsizei bufSize, GLsizei* length, GLchar* infoLog);
typedef void (* PFNGLGETSHADERIVPROC) (GLuint shader, GLenum pname, GLint* param);
typedef GLint (* PFNGLGETUNIFORMLOCATIONPROC) (GLuint program, const GLchar* name);
typedef void (* PFNGLGETVERTEXATTRIBPOINTERVPROC) (GLuint, GLenum, GLvoid**);
typedef void (* PFNGLLINKPROGRAMPROC) (GLuint program);
typedef void (* PFNGLSHADERSOURCEPROC) (GLuint shader, GLsizei count, const GLchar** strings, const GLint* lengths);
typedef void (* PFNGLUNIFORM1FPROC) (GLint location, GLfloat v0);
typedef void (* PFNGLUNIFORM1FVPROC) (GLint location, GLsizei count, const GLfloat* value);
typedef void (* PFNGLUNIFORM1IPROC) (GLint location, GLint v0);
typedef void (* PFNGLUNIFORM1IVPROC) (GLint location, GLsizei count, const GLint* value);
typedef void (* PFNGLUNIFORM2FPROC) (GLint location, GLfloat v0, GLfloat v1);
typedef void (* PFNGLUNIFORM2FVPROC) (GLint location, GLsizei count, const GLfloat* value);
typedef void (* PFNGLUNIFORM2IPROC) (GLint location, GLint v0, GLint v1);
typedef void (* PFNGLUNIFORM2IVPROC) (GLint location, GLsizei count, const GLint* value);
typedef void (* PFNGLUNIFORM3FPROC) (GLint location, GLfloat v0, GLfloat v1, GLfloat v2);
typedef void (* PFNGLUNIFORM3FVPROC) (GLint location, GLsizei count, const GLfloat* value);
typedef void (* PFNGLUNIFORM3IPROC) (GLint location, GLint v0, GLint v1, GLint v2);
typedef void (* PFNGLUNIFORM3IVPROC) (GLint location, GLsizei count, const GLint* value);
typedef void (* PFNGLUNIFORM4FPROC) (GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3);
typedef void (* PFNGLUNIFORM4FVPROC) (GLint location, GLsizei count, const GLfloat* value);
typedef void (* PFNGLUNIFORM4IPROC) (GLint location, GLint v0, GLint v1, GLint v2, GLint v3);
typedef void (* PFNGLUNIFORM4IVPROC) (GLint location, GLsizei count, const GLint* value);
typedef void (* PFNGLUNIFORMMATRIX2FVPROC) (GLint location, GLsizei count, GLboolean transpose, const GLfloat* value);
typedef void (* PFNGLUNIFORMMATRIX3FVPROC) (GLint location, GLsizei count, GLboolean transpose, const GLfloat* value);
typedef void (* PFNGLUNIFORMMATRIX4FVPROC) (GLint location, GLsizei count, GLboolean transpose, const GLfloat* value);
typedef void (* PFNGLUSEPROGRAMPROC) (GLuint program);
typedef void (* PFNGLVALIDATEPROGRAMPROC) (GLuint program);
typedef void (* PFNGLVERTEXATTRIBPOINTERPROC) (GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid* pointer);

// OpenGL VAO
typedef void(* PFNGLBINDVERTEXARRAYPROC)(GLuint array);
typedef void(* PFNGLDELETEVERTEXARRAYSPROC)(GLsizei n, const GLuint* arrays);
typedef void(* PFNGLGENVERTEXARRAYSPROC)(GLsizei n, GLuint* arrays);

// OpenGL FBO
typedef void (* PFNGLBINDFRAMEBUFFERPROC) (GLenum target, GLuint framebuffer);
typedef GLenum (* PFNGLCHECKFRAMEBUFFERSTATUSPROC) (GLenum target);
typedef void (* PFNGLDELETEFRAMEBUFFERSPROC) (GLsizei n, const GLuint* framebuffers);
typedef void (* PFNGLFRAMEBUFFERTEXTURE2DPROC) (GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level);
typedef void (* PFNGLGENFRAMEBUFFERSPROC) (GLsizei n, GLuint* framebuffers);

// ----------------------------------------------------------------------------
struct OGLContext
{
  PFNGLACTIVETEXTUREPROC _glActiveTexture;

  PFNGLBINDBUFFERPROC _glBindBuffer;
  PFNGLBUFFERDATAPROC _glBufferData;
  PFNGLDELETEBUFFERSPROC _glDeleteBuffers;
  PFNGLGENBUFFERSPROC _glGenBuffers;

  PFNGLATTACHSHADERPROC _glAttachShader;
  PFNGLBINDATTRIBLOCATIONPROC _glBindAttribLocation;
  PFNGLCOMPILESHADERPROC _glCompileShader;
  PFNGLCREATEPROGRAMPROC _glCreateProgram;
  PFNGLCREATESHADERPROC _glCreateShader;
  PFNGLDELETEPROGRAMPROC _glDeleteProgram;
  PFNGLDELETESHADERPROC _glDeleteShader;
  PFNGLDETACHSHADERPROC _glDetachShader;
  PFNGLDISABLEVERTEXATTRIBARRAYPROC _glDisableVertexAttribArray;
  PFNGLDRAWBUFFERSPROC _glDrawBuffers;
  PFNGLENABLEVERTEXATTRIBARRAYPROC _glEnableVertexAttribArray;
  PFNGLGETATTRIBLOCATIONPROC _glGetAttribLocation;
  PFNGLGETSHADERINFOLOGPROC _glGetShaderInfoLog;
  PFNGLGETUNIFORMLOCATIONPROC _glGetUniformLocation;
  PFNGLGETVERTEXATTRIBPOINTERVPROC _glGetVertexAttribPointerv;
  PFNGLLINKPROGRAMPROC _glLinkProgram;
  PFNGLSHADERSOURCEPROC _glShaderSource;
  PFNGLUNIFORM1FPROC _glUniform1f;
  PFNGLUNIFORM1FVPROC _glUniform1fv;
  PFNGLUNIFORM1IPROC _glUniform1i;
  PFNGLUNIFORM1IVPROC _glUniform1iv;
  PFNGLUNIFORM2FPROC _glUniform2f;
  PFNGLUNIFORM2FVPROC _glUniform2fv;
  PFNGLUNIFORM2IPROC _glUniform2i;
  PFNGLUNIFORM2IVPROC _glUniform2iv;
  PFNGLUNIFORM3FPROC _glUniform3f;
  PFNGLUNIFORM3FVPROC _glUniform3fv;
  PFNGLUNIFORM3IPROC _glUniform3i;
  PFNGLUNIFORM3IVPROC _glUniform3iv;
  PFNGLUNIFORM4FPROC _glUniform4f;
  PFNGLUNIFORM4FVPROC _glUniform4fv;
  PFNGLUNIFORM4IPROC _glUniform4i;
  PFNGLUNIFORM4IVPROC _glUniform4iv;
  PFNGLUNIFORMMATRIX2FVPROC _glUniformMatrix2fv;
  PFNGLUNIFORMMATRIX3FVPROC _glUniformMatrix3fv;
  PFNGLUNIFORMMATRIX4FVPROC _glUniformMatrix4fv;
  PFNGLUSEPROGRAMPROC _glUseProgram;
  PFNGLVALIDATEPROGRAMPROC _glValidateProgram;
  PFNGLVERTEXATTRIBPOINTERPROC _glVertexAttribPointer;

  PFNGLBINDVERTEXARRAYPROC _glBindVertexArray;
  PFNGLDELETEVERTEXARRAYSPROC _glDeleteVertexArrays;
  PFNGLGENVERTEXARRAYSPROC _glGenVertexArrays;

  PFNGLBINDFRAMEBUFFERPROC _glBindFramebuffer;
  PFNGLCHECKFRAMEBUFFERSTATUSPROC _glCheckFramebufferStatus;
  PFNGLDELETEFRAMEBUFFERSPROC _glDeleteFramebuffers;
  PFNGLFRAMEBUFFERTEXTURE2DPROC _glFramebufferTexture2D;
  PFNGLGENFRAMEBUFFERSPROC _glGenFramebuffers;
};

static OGLContext gl;

// ----------------------------------------------------------------------------
void initOpenGL()
{
  gl._glActiveTexture = (PFNGLACTIVETEXTUREPROC)wglGetProcAddress("glActiveTexture");

  gl._glBindBuffer = (PFNGLBINDBUFFERPROC)wglGetProcAddress("glBindBuffer");
  gl._glBufferData = (PFNGLBUFFERDATAPROC)wglGetProcAddress("glBufferData");
  gl._glDeleteBuffers = (PFNGLDELETEBUFFERSPROC)wglGetProcAddress("glDeleteBuffers");
  gl._glGenBuffers = (PFNGLGENBUFFERSPROC)wglGetProcAddress("glGenBuffers");

  gl._glAttachShader = (PFNGLATTACHSHADERPROC)wglGetProcAddress("glAttachShader");
  gl._glBindAttribLocation = (PFNGLBINDATTRIBLOCATIONPROC)wglGetProcAddress("glBindAttribLocation");
  gl._glCompileShader = (PFNGLCOMPILESHADERPROC)wglGetProcAddress("glCompileShader");
  gl._glCreateProgram = (PFNGLCREATEPROGRAMPROC)wglGetProcAddress("glCreateProgram");
  gl._glCreateShader = (PFNGLCREATESHADERPROC)wglGetProcAddress("glCreateShader");
  gl._glDeleteProgram = (PFNGLDELETEPROGRAMPROC)wglGetProcAddress("glDeleteProgram");
  gl._glDeleteShader = (PFNGLDELETESHADERPROC)wglGetProcAddress("glDeleteShader");
  gl._glDetachShader = (PFNGLDETACHSHADERPROC)wglGetProcAddress("glDetachShader");
  gl._glDisableVertexAttribArray = (PFNGLDISABLEVERTEXATTRIBARRAYPROC)wglGetProcAddress("glDisableVertexAttribArray");
  gl._glDrawBuffers = (PFNGLDRAWBUFFERSPROC)wglGetProcAddress("glDrawBuffers");
  gl._glEnableVertexAttribArray = (PFNGLENABLEVERTEXATTRIBARRAYPROC)wglGetProcAddress("glEnableVertexAttribArray");
  gl._glGetAttribLocation = (PFNGLGETATTRIBLOCATIONPROC)wglGetProcAddress("glGetAttribLocation");
  gl._glGetShaderInfoLog = (PFNGLGETSHADERINFOLOGPROC)wglGetProcAddress("glGetShaderInfoLog");
  gl._glGetUniformLocation = (PFNGLGETUNIFORMLOCATIONPROC)wglGetProcAddress("glGetUniformLocation");
  gl._glGetVertexAttribPointerv = (PFNGLGETVERTEXATTRIBPOINTERVPROC)wglGetProcAddress("glGetVertexAttribPointerv");
  gl._glLinkProgram = (PFNGLLINKPROGRAMPROC)wglGetProcAddress("glLinkProgram");
  gl._glShaderSource = (PFNGLSHADERSOURCEPROC)wglGetProcAddress("glShaderSource");
  gl._glUniform1f = (PFNGLUNIFORM1FPROC)wglGetProcAddress("glUniform1f");
  gl._glUniform1fv = (PFNGLUNIFORM1FVPROC)wglGetProcAddress("glUniform1fv");
  gl._glUniform1i = (PFNGLUNIFORM1IPROC)wglGetProcAddress("glUniform1i");
  gl._glUniform1iv = (PFNGLUNIFORM1IVPROC)wglGetProcAddress("glUniform1iv");
  gl._glUniform2f = (PFNGLUNIFORM2FPROC)wglGetProcAddress("glUniform2f");
  gl._glUniform2fv = (PFNGLUNIFORM2FVPROC)wglGetProcAddress("glUniform2fv");
  gl._glUniform2i = (PFNGLUNIFORM2IPROC)wglGetProcAddress("glUniform2i");
  gl._glUniform2iv = (PFNGLUNIFORM2IVPROC)wglGetProcAddress("glUniform2iv");
  gl._glUniform3f = (PFNGLUNIFORM3FPROC)wglGetProcAddress("glUniform3f");
  gl._glUniform3fv = (PFNGLUNIFORM3FVPROC)wglGetProcAddress("glUniform3fv");
  gl._glUniform3i = (PFNGLUNIFORM3IPROC)wglGetProcAddress("glUniform3i");
  gl._glUniform3iv = (PFNGLUNIFORM3IVPROC)wglGetProcAddress("glUniform3iv");
  gl._glUniform4f = (PFNGLUNIFORM4FPROC)wglGetProcAddress("glUniform4f");
  gl._glUniform4fv = (PFNGLUNIFORM4FVPROC)wglGetProcAddress("glUniform4fv");
  gl._glUniform4i = (PFNGLUNIFORM4IPROC)wglGetProcAddress("glUniform4i");
  gl._glUniform4iv = (PFNGLUNIFORM4IVPROC)wglGetProcAddress("glUniform4iv");
  gl._glUniformMatrix2fv = (PFNGLUNIFORMMATRIX2FVPROC)wglGetProcAddress("glUniformMatrix2fv");
  gl._glUniformMatrix3fv = (PFNGLUNIFORMMATRIX3FVPROC)wglGetProcAddress("glUniformMatrix3fv");
  gl._glUniformMatrix4fv = (PFNGLUNIFORMMATRIX4FVPROC)wglGetProcAddress("glUniformMatrix4fv");
  gl._glUseProgram = (PFNGLUSEPROGRAMPROC)wglGetProcAddress("glUseProgram");
  gl._glValidateProgram = (PFNGLVALIDATEPROGRAMPROC)wglGetProcAddress("glValidateProgram");
  gl._glVertexAttribPointer = (PFNGLVERTEXATTRIBPOINTERPROC)wglGetProcAddress("glVertexAttribPointer");

  gl._glBindVertexArray = (PFNGLBINDVERTEXARRAYPROC)wglGetProcAddress("glBindVertexArray");
  gl._glDeleteVertexArrays = (PFNGLDELETEVERTEXARRAYSPROC)wglGetProcAddress("glDeleteVertexArrays");
  gl._glGenVertexArrays = (PFNGLGENVERTEXARRAYSPROC)wglGetProcAddress("glGenVertexArrays");

  gl._glBindFramebuffer = (PFNGLBINDFRAMEBUFFERPROC)wglGetProcAddress("glBindFramebuffer");
  gl._glCheckFramebufferStatus = (PFNGLCHECKFRAMEBUFFERSTATUSPROC)wglGetProcAddress("glCheckFramebufferStatus");
  gl._glDeleteFramebuffers = (PFNGLDELETEFRAMEBUFFERSPROC)wglGetProcAddress("glDeleteFramebuffers");
  gl._glFramebufferTexture2D = (PFNGLFRAMEBUFFERTEXTURE2DPROC)wglGetProcAddress("glFramebufferTexture2D");
  gl._glGenFramebuffers = (PFNGLGENFRAMEBUFFERSPROC)wglGetProcAddress("glGenFramebuffers");
}

// ----------------------------------------------------------------------------
void glActiveTexture(GLenum texture) {
  return gl._glActiveTexture(texture); }

// ----------------------------------------------------------------------------
void glBindBuffer(GLenum target, GLuint buffer) {
  return gl._glBindBuffer(target, buffer); }
void glBufferData(GLenum target, GLsizeiptr size, const GLvoid* data, GLenum usage) {
  return gl._glBufferData(target, size, data, usage); }
void glDeleteBuffers(GLsizei n, const GLuint* buffers) {
  return gl._glDeleteBuffers(n, buffers); }
void glGenBuffers(GLsizei n, GLuint* buffers) {
  return gl._glGenBuffers(n, buffers); }

// ----------------------------------------------------------------------------
void glAttachShader(GLuint program, GLuint shader) {
  return gl._glAttachShader(program, shader); }
void glBindAttribLocation(GLuint program, GLuint index, const GLchar* name) {
  return gl._glBindAttribLocation(program, index, name); }
void glCompileShader(GLuint shader) {
  return gl._glCompileShader(shader); }
GLuint glCreateProgram(void) {
  return gl._glCreateProgram(); }
GLuint glCreateShader(GLenum type) {
  return gl._glCreateShader(type); }
void glDeleteProgram(GLuint program) {
  return gl._glDeleteProgram(program); }
void glDeleteShader(GLuint shader) {
  return gl._glDeleteShader(shader); }
void glDetachShader(GLuint program, GLuint shader) {
  return gl._glDetachShader(program, shader); }
void glDisableVertexAttribArray(GLuint i) {
  return gl._glDisableVertexAttribArray(i); }
void glDrawBuffers(GLsizei n, const GLenum* bufs) {
  return gl._glDrawBuffers(n, bufs); }
void glEnableVertexAttribArray(GLuint i) {
  return gl._glEnableVertexAttribArray(i); }
void glGetShaderInfoLog(GLuint shader, GLsizei bufSize, GLsizei* length, GLchar* infoLog) {
  return gl._glGetShaderInfoLog(shader, bufSize, length, infoLog); }
GLuint glGetAttribLocation(GLuint program, const GLchar* name) {
  return gl._glGetAttribLocation(program, name); }
GLuint glGetUniformLocation(GLuint program, const GLchar* name) {
  return gl._glGetUniformLocation(program, name); }
void glGetVertexAttribPointerv(GLuint index, GLenum pname, GLvoid** params) {
  return gl._glGetVertexAttribPointerv(index, pname, params); }
void glLinkProgram(GLuint program) {
  return gl._glLinkProgram(program); }
void glShaderSource(GLuint shader, GLsizei count, const GLchar** strings, const GLint* lengths) {
  return gl._glShaderSource(shader, count, strings, lengths); }
void glUniform1f(GLint location, GLfloat v0) {
  return gl._glUniform1f(location, v0); }
void glUniform1fv(GLint location, GLsizei count, const GLfloat* value) {
  return gl._glUniform1fv(location, count, value); }
void glUniform1i(GLint location, GLint v0) {
  return gl._glUniform1i(location, v0); }
void glUniform1iv(GLint location, GLsizei count, const GLint* value) {
  return gl._glUniform1iv(location, count, value); }
void glUniform2f(GLint location, GLfloat v0, GLfloat v1) {
  return gl._glUniform2f(location, v0, v1); }
void glUniform2fv(GLint location, GLsizei count, const GLfloat* value) {
  return gl._glUniform2fv(location, count, value); }
void glUniform2i(GLint location, GLint v0, GLint v1) {
  return gl._glUniform2i(location, v0, v1); }
void glUniform2iv(GLint location, GLsizei count, const GLint* value) {
  return gl._glUniform2iv(location, count, value); }
void glUniform3f(GLint location, GLfloat v0, GLfloat v1, GLfloat v2) {
  return gl._glUniform3f(location, v0, v1, v2); }
void glUniform3fv(GLint location, GLsizei count, const GLfloat* value) {
  return gl._glUniform3fv(location, count, value); }
void glUniform3i(GLint location, GLint v0, GLint v1, GLint v2) {
  return gl._glUniform3i(location, v0, v1, v2); }
void glUniform3iv(GLint location, GLsizei count, const GLint* value) {
  return gl._glUniform3iv(location, count, value); }
void glUniform4f(GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3) {
  return gl._glUniform4f(location, v0, v1, v2, v3); }
void glUniform4fv(GLint location, GLsizei count, const GLfloat* value) {
  return gl._glUniform4fv(location, count, value); }
void glUniform4i(GLint location, GLint v0, GLint v1, GLint v2, GLint v3) {
  return gl._glUniform4i(location, v0, v1, v2, v3); }
void glUniform4iv(GLint location, GLsizei count, const GLint* value) {
  return gl._glUniform4iv(location, count, value); }
void glUniformMatrix2fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat* value) {
  return gl._glUniformMatrix2fv(location, count, transpose, value); }
void glUniformMatrix3fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat* value) {
  return gl._glUniformMatrix3fv(location, count, transpose, value); }
void glUniformMatrix4fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat* value) {
  return gl._glUniformMatrix4fv(location, count, transpose, value); }
void glUseProgram(GLuint program) {
  return gl._glUseProgram(program); }
void glValidateProgram(GLuint program) {
  return gl._glValidateProgram(program); }
void glVertexAttribPointer(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid* pointer) {
  return gl._glVertexAttribPointer(index, size, type, normalized, stride, pointer); }

// ----------------------------------------------------------------------------
void glBindVertexArray(GLuint array) {
  return gl._glBindVertexArray(array); }
void glDeleteVertexArrays(GLsizei n, const GLuint* arrays) {
  return gl._glDeleteVertexArrays(n, arrays); }
void glGenVertexArrays(GLsizei n, GLuint* arrays) {
  return gl._glGenVertexArrays(n, arrays); }

// ----------------------------------------------------------------------------
void glBindFramebuffer(GLenum target, GLuint framebuffer) {
  return gl._glBindFramebuffer(target, framebuffer); }
GLenum glCheckFramebufferStatus(GLenum target) {
  return gl._glCheckFramebufferStatus(target); }
void glDeleteFramebuffers(GLsizei n, const GLuint* framebuffers) {
  return gl._glDeleteFramebuffers(n, framebuffers); }
void glFramebufferTexture2D(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level) {
  return gl._glFramebufferTexture2D(target, attachment, textarget, texture, level); }
void glGenFramebuffers(GLsizei n, GLuint* framebuffers) {
  return gl._glGenFramebuffers(n, framebuffers); }
