#include "fantasia.h"
#include "opengl.h"

// ----------------------------------------------------------------------------
class GameTest : public fantasia::IGame
{
public:
  GameTest();
  ~GameTest() override = default;

  uint64_t getUpdateTime() const override;
  int preprocess(uint64_t tNow) override;
  int update() override;
  int render() override;

  fantasia::IRendererPtr _renderer;
};

// ----------------------------------------------------------------------------
GameTest::GameTest()
{
  _renderer = fantasia::rendererOpenGL();
}

// ----------------------------------------------------------------------------
uint64_t GameTest::getUpdateTime() const
{
  return 20; // 20ms = 50fps
}

// ----------------------------------------------------------------------------
int GameTest::preprocess(uint64_t tNow)
{
  return 0;
}

// ----------------------------------------------------------------------------
int GameTest::update()
{
  return 0;
}

// ----------------------------------------------------------------------------
int GameTest::render()
{
  _renderer->resize(1280, 720);

  glClearColor(0.1f, 0.4f, 0.5f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT);

  _renderer->runShading();
  return 1; // stop after 1 loop
}

// ----------------------------------------------------------------------------
int main(int argc, char const* argv[])
{
  GameTest game;
  fantasia::gameloop(game, fantasia::clock());
  return 0;
}
