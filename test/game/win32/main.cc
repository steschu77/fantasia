#include "fantasia.h"
#include "win_opengl.h"

// ----------------------------------------------------------------------------
class GameTest : public fantasia::IGame
{
public:
  GameTest(HINSTANCE hinst, int cmdShow);
  ~GameTest() override;

  uint64_t getUpdateTime() const override;
  int preprocess(uint64_t tNow) override;
  int update() override;
  int render() override;

  WinOpenGLFactory _Fab;
  WinOpenGL* _pWin;
};

// ----------------------------------------------------------------------------
GameTest::GameTest(HINSTANCE hinst, int cmdShow)
: _Fab { hinst }
{
  _pWin = _Fab.createWindow(cmdShow);
}

// ----------------------------------------------------------------------------
GameTest::~GameTest()
{
  delete _pWin;
}

// ----------------------------------------------------------------------------
uint64_t GameTest::getUpdateTime() const
{
  return 20; // 20ms = 50fps
}

// ----------------------------------------------------------------------------
int GameTest::preprocess(uint64_t tNow)
{
  MSG msg;
  while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE)) {
    GetMessage(&msg, NULL, 0, 0);
    if (msg.message == WM_QUIT) {
      return 1;
    }

    TranslateMessage(&msg);
    DispatchMessage(&msg);
  }
  return 0;
}

// ----------------------------------------------------------------------------
int GameTest::update()
{
  return 0;
}

// ----------------------------------------------------------------------------
int GameTest::render()
{
  _pWin->render();
  return 0;
}

// ----------------------------------------------------------------------------
int WINAPI WinMain(_In_ HINSTANCE hInst, _In_opt_ HINSTANCE, _In_ LPSTR, _In_ int cmdShow)
{
  GameTest game(hInst, cmdShow);
  fantasia::gameloop(game, fantasia::clock());
  return 0;
}
