#include "win_opengl.h"

// ----------------------------------------------------------------------------
constexpr wchar_t* gWinClassName = L"OpenGLClass";

// ----------------------------------------------------------------------------
WinOpenGLFactory::WinOpenGLFactory(HINSTANCE hInstance)
: _hinstance(hInstance)
{
  _registerClass(gWinClassName, nullptr);
}

// ----------------------------------------------------------------------------
WinOpenGL* WinOpenGLFactory::createWindow(int cmdShow)
{
  WinOpenGL* pWin = new WinOpenGL();

  HWND hwnd = CreateWindowExW(0L, gWinClassName, L"", WS_POPUP,
    10, 10, 1280, 720, nullptr, nullptr, _hinstance, pWin);

  if (hwnd == nullptr) {
    delete pWin;
    return nullptr;
  }

  ShowWindow(hwnd, SW_SHOW);
  UpdateWindow(hwnd);

  return pWin;
}

// ----------------------------------------------------------------------------
void WinOpenGLFactory::_registerClass(const wchar_t* className, HBRUSH hbrBackground)
{
  WNDCLASSEXW wcex = { 0 };

  wcex.cbSize = sizeof(WNDCLASSEX);
  wcex.lpfnWndProc = _procStatic1;
  wcex.hInstance = _hinstance;
  wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
  wcex.hbrBackground = hbrBackground;
  wcex.lpszClassName = className;
  RegisterClassExW(&wcex);
}

// ----------------------------------------------------------------------------
// Window Proc handler until WM_CREATE with create params is received
LRESULT CALLBACK WinOpenGLFactory::_procStatic1(
  HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  if (msg == WM_CREATE) {
    CREATESTRUCT* cs = (CREATESTRUCT*)lParam;
    WinOpenGL* pHandler = (cs != nullptr) ? static_cast<WinOpenGL*>(cs->lpCreateParams) : nullptr;

    if (pHandler == nullptr) {
      return 0;
    }

    pHandler->_hwnd = hwnd;
    SetWindowLongPtr(hwnd, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(_procStaticN));
    SetWindowLongPtr(hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(pHandler));
    return pHandler->proc(hwnd, msg, wParam, lParam);
  }

  return DefWindowProcW(hwnd, msg, wParam, lParam);
}

// ----------------------------------------------------------------------------
// Window Proc handler with initialized user data
LRESULT CALLBACK WinOpenGLFactory::_procStaticN(
  HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  WinOpenGL* pWin = reinterpret_cast<WinOpenGL*>(GetWindowLongPtr(hwnd, GWLP_USERDATA));
  return pWin->proc(hwnd, msg, wParam, lParam);
}

// ----------------------------------------------------------------------------
WinOpenGL::WinOpenGL()
{
}

// ----------------------------------------------------------------------------
WinOpenGL::~WinOpenGL()
{
}

// ----------------------------------------------------------------------------
LRESULT WinOpenGL::proc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  switch (msg) {
  case WM_CREATE:
    return onCreate();

  case WM_DESTROY:
    return onDestroy();

  case WM_KEYDOWN:
    return onKeyDown((unsigned)wParam);

  case WM_SIZE:
    return onSize(LOWORD(lParam), HIWORD(lParam), (unsigned)wParam);
  }

  return DefWindowProcW(hwnd, msg, wParam, lParam);
}

// ----------------------------------------------------------------------------
LRESULT WinOpenGL::onCreate()
{
  _hdc = GetDC(_hwnd);

  PIXELFORMATDESCRIPTOR pfd = { 0 };
  pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
  pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
  pfd.iPixelType = PFD_TYPE_RGBA;
  pfd.cColorBits = 32;
  pfd.cDepthBits = 32;
  pfd.iLayerType = PFD_MAIN_PLANE;

  int pixfmt = ChoosePixelFormat(_hdc, &pfd);
  if (pixfmt != 0) {
    SetPixelFormat(_hdc, pixfmt, &pfd);
  }

  _hglrc = wglCreateContext(_hdc);
  wglMakeCurrent(_hdc, _hglrc);

  initOpenGL();
  _renderer = fantasia::rendererOpenGL();
  return 0;
}

// ----------------------------------------------------------------------------
LRESULT WinOpenGL::onDestroy()
{
  wglMakeCurrent(nullptr, nullptr);
  wglDeleteContext(_hglrc);

  PostQuitMessage(1);
  return 0;
}

// --------------------------------------------------------------------------
LRESULT WinOpenGL::onKeyDown(unsigned vk)
{
  if (vk == VK_ESCAPE) {
    DestroyWindow(_hwnd);
  }
  return 0;
}

// --------------------------------------------------------------------------
LRESULT WinOpenGL::onSize(int cx, int cy, unsigned flags)
{
  RECT rc;
  GetClientRect(hwnd(), &rc);
  _renderer->resize(rc.right, rc.bottom);

  return 0;
}

// --------------------------------------------------------------------------
void WinOpenGL::render()
{
  _renderer->runGBuffer();
  _renderer->runShading();

  SwapBuffers(_hdc);
}
