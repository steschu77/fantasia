#pragma once

#include <Windows.h>
#undef min
#undef max

#include "fantasia.h"
#include "opengl.h"

// ----------------------------------------------------------------------------
class WinOpenGL
{
  friend class WinOpenGLFactory;

public:
  WinOpenGL();
  ~WinOpenGL();

  HWND hwnd() const;

  void render();

protected:
  LRESULT proc(HWND hwnd, UINT cmd, WPARAM wParam, LPARAM lParam);

  HDC _hdc = nullptr;
  HGLRC _hglrc = nullptr;
  HWND _hwnd = nullptr;

  fantasia::IRendererPtr _renderer;

private:
  LRESULT onCreate();
  LRESULT onDestroy();
  LRESULT onKeyDown(unsigned);
  LRESULT onSize(int, int, unsigned);
};

// ----------------------------------------------------------------------------
class WinOpenGLFactory
{
public:
  WinOpenGLFactory(HINSTANCE hInstance);

  HINSTANCE hinstance() const;

  WinOpenGL* createWindow(int cmdShow);

protected:
  void _registerClass(const wchar_t* className, HBRUSH hbrBackground);

private:
  HINSTANCE _hinstance = nullptr;

  static LRESULT CALLBACK _procStatic1(
    HWND hwnd, UINT cmd, WPARAM wParam, LPARAM lParam);
  static LRESULT CALLBACK _procStaticN(
    HWND hwnd, UINT cmd, WPARAM wParam, LPARAM lParam);
};

// ----------------------------------------------------------------------------
inline HWND WinOpenGL::hwnd() const
{
  return _hwnd;
}

// ----------------------------------------------------------------------------
inline HINSTANCE WinOpenGLFactory::hinstance() const
{
  return _hinstance;
}
