#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

#include "ccunit.h"

void test_gameloop_fast();
void test_gameloop_slow();
void test_gameloop_superslow();

// ----------------------------------------------------------------------------
const ccunit::Tests tests()
{
  ccunit::Tests tests;

  tests.push_back(std::make_unique<ccunit::Test>("Gameloop(Fast)", test_gameloop_fast));
  tests.push_back(std::make_unique<ccunit::Test>("Gameloop(Slow)", test_gameloop_slow));
  tests.push_back(std::make_unique<ccunit::Test>("Gameloop(SuperSlow)", test_gameloop_superslow));

  return tests;
}

// ----------------------------------------------------------------------------
int main(int argc, char* argv[])
{
  return ccunit::runTests(tests(), ccunit::VerboseMode::printEverything);
}
