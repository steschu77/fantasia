#include "ccunit.h"
#include "fantasia.h"

#include <tuple>
#include <vector>

// ----------------------------------------------------------------------------
using Times = std::vector<uint64_t>;

// ----------------------------------------------------------------------------
class MockClock : public fantasia::IClock
{
public:
  uint64_t now() const override;
  void sleep(uint64_t dt) override;

  void process(uint64_t dt);
  const Times& sleeps() const;

private:
  Times _sleeps;
  uint64_t _t = 0;
};

// ----------------------------------------------------------------------------
using Loop = std::tuple<uint64_t, unsigned>;
using Loops = std::vector<Loop>;

// ----------------------------------------------------------------------------
class MockGame : public fantasia::IGame
{
public:
  MockGame(MockClock& clock, uint64_t tUpdate, uint64_t tProcess,
    uint64_t tRender, unsigned maxLoops);

  uint64_t getUpdateTime() const;
  int preprocess(uint64_t tNow);
  int update();
  int render();

  const Loops& loops();

private:
  const uint64_t _tUpdate;
  const uint64_t _tProcess;
  const uint64_t _tRender;
  const unsigned _maxLoops;
  uint64_t _t = 0;
  unsigned _updates = 0;
  MockClock& _clock;
  Loops _loops;
};

// ----------------------------------------------------------------------------
uint64_t MockClock::now() const
{
  return _t;
}

// ----------------------------------------------------------------------------
void MockClock::sleep(uint64_t dt)
{
  _sleeps.emplace_back(dt);
  _t += dt;
}

// ----------------------------------------------------------------------------
void MockClock::process(uint64_t dt)
{
  _t += dt;
}

// ----------------------------------------------------------------------------
const Times& MockClock::sleeps() const
{
  return _sleeps;
}

// ----------------------------------------------------------------------------
MockGame::MockGame(MockClock& clock, uint64_t tUpdate, uint64_t tProcess,
  uint64_t tRender, unsigned maxLoops)
: _tUpdate(tUpdate)
, _tProcess(tProcess)
, _tRender(tRender)
, _maxLoops(maxLoops)
, _clock(clock)
{
}

// ----------------------------------------------------------------------------
uint64_t MockGame::getUpdateTime() const
{
  return _tUpdate;
}

// ----------------------------------------------------------------------------
int MockGame::preprocess(uint64_t tNow)
{
  _t = tNow;
  _updates = 0;
  return 0;
}

// ----------------------------------------------------------------------------
int MockGame::update()
{
  ++_updates;
  _clock.process(_tProcess);
  return 0;
}

// ----------------------------------------------------------------------------
int MockGame::render()
{
  _clock.process(_tRender);
  _loops.push_back(Loop { _t, _updates });
  return _loops.size() >= _maxLoops;
}

// ----------------------------------------------------------------------------
const Loops& MockGame::loops()
{
  return _loops;
}

// ----------------------------------------------------------------------------
void test_gameloop_fast()
{
  constexpr uint64_t tUpdate = 20;
  constexpr uint64_t tProcess = 0;
  constexpr uint64_t tRender = 0;

  MockClock clock;
  MockGame game { clock, tUpdate, tProcess, tRender, 4 };
  fantasia::gameloop(game, clock);

  // since processing time was 0 ms, every sleep call should be tUpdate
  const Times& sleeps = clock.sleeps();
  for (const auto& t : sleeps) {
    EXPECT_EQ(t, tUpdate);
  }

  // since processing time was 0 ms, every loop should only contain one update
  const Loops& loops = game.loops();
  for (const auto& l : loops) {
    EXPECT_EQ(std::get<1>(l), 1u);
  }

  // since processing time was 0 ms, every update call should be tUpdate ms
  // after the previous one
  for (size_t i = 1; i < loops.size(); ++i) {
    EXPECT_EQ(std::get<0>(loops[i]) - std::get<0>(loops[i - 1]), tUpdate);
  }
}

// ----------------------------------------------------------------------------
void test_gameloop_slow()
{
  constexpr uint64_t tUpdate = 20;
  constexpr uint64_t tProcess = 10;
  constexpr uint64_t tRender = 20;

  MockClock clock;
  MockGame game { clock, tUpdate, tProcess, tRender, 6 };
  fantasia::gameloop(game, clock);

  // since updating time and rendering time are larger than loop time,
  // there should be no sleep
  const Times& sleeps = clock.sleeps();
  for (const auto& t : sleeps) {
    EXPECT_EQ(t, 0);
  }

  // with 10 ms updating time and 20 ms rendering time, we expect 2 updates per loop
  // give 2 loops to account for adoption time
  const Loops& loops = game.loops();
  for (size_t i = 2; i < loops.size(); ++i) {
    EXPECT_EQ(std::get<1>(loops[i]), 2u);
  }

  // with 2 updates per loop, we expect 20 ms rendering + 2 * 10 ms update time
  // give 2 loops to account for adoption time
  for (size_t i = 3; i < loops.size(); ++i) {
    EXPECT_EQ(std::get<0>(loops[i]) - std::get<0>(loops[i - 1]), tRender + 2 * tProcess);
  }
}

// ----------------------------------------------------------------------------
void test_gameloop_superslow()
{
  constexpr uint64_t tUpdate = 20;
  constexpr uint64_t tProcess = 20;
  constexpr uint64_t tRender = 20;

  MockClock clock;
  MockGame game { clock, tUpdate, tProcess, tRender, 8 };
  fantasia::gameloop(game, clock);

  // since updating time and rendering time are larger than loop time,
  // there should be no sleep
  const Times& sleeps = clock.sleeps();
  for (const auto& t : sleeps) {
    EXPECT_EQ(t, 0);
  }

  // with 20 ms updating time and 20 ms rendering time, we expect the maximum
  // of 4 updates per loop
  // give 3 loops to account for adoption time
  const Loops& loops = game.loops();
  for (size_t i = 3; i < loops.size(); ++i) {
    EXPECT_EQ(std::get<1>(loops[i]), 4u);
  }

  // with 4 updates per loop, we expect 20 ms rendering + 4 * 20 ms update time
  // give 3 loops to account for adoption time
  for (size_t i = 4; i < loops.size(); ++i) {
    EXPECT_EQ(std::get<0>(loops[i]) - std::get<0>(loops[i - 1]), tRender + 4 * tProcess);
  }
}
